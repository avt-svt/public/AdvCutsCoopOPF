optimal value of nominal OPF is: 2.14122e+06

Congested lines in nominal operation are: 
7 to 8 at t15.
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


optimal DR to nominal grid planning prices of bus 5 has cost of: 39365.7
optimal value of integrated optimization is: 1.81625e+06
Congested lines in integrated operation are: 
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


71 MW flexible load is cooperating and the initial threshold tau is 142 MW
maxIters: 500, primary/secondary strategy: 2/0, acceptable gap: 0.0001
it 50done, CWsubProb on bus 5 has objective 0.07193, and distance 133.82, L1subProb on bus 5 has objective 0.49823, and distance 35.374, master obj (lb of Decomp) was: 1.8162e+06
iteration 50, culm. feasible Gap is 1.33819e+02 MW, which is below 1.42000e+02 MW (current tau)
detect. Gap: 6.96455e-04, intsol-masterLB: 40.1094, real Gap: 0.000674386, ub computation returns status2.
iteration 60, culm. feasible Gap is 5.76883e+01 MW, which is below 8.44900e+01 MW (current tau)
detect. Gap: 2.94533e-04, intsol-masterLB: 15.7109, real Gap: 0.000285885, ub computation returns status2.
iteration 64, culm. feasible Gap is 4.71586e+01 MW, which is below 5.02715e+01 MW (current tau)
detect. Gap: 2.41200e-04, intsol-masterLB: 9.59313, real Gap: 0.000235919, ub computation returns status2.
iteration 66, culm. feasible Gap is 2.66375e+01 MW, which is below 2.99116e+01 MW (current tau)
detect. Gap: 1.35973e-04, intsol-masterLB: 8.32285, real Gap: 0.000131391, ub computation returns status2.
iteration 72, culm. feasible Gap is 2.41965e+01 MW, which is below 2.61778e+01 MW (current tau)
detect. Gap: 1.22927e-04, intsol-masterLB: 3.08385, real Gap: 0.000121229, ub computation returns status2.
iteration 74, culm. feasible Gap is 1.13067e+01 MW, which is below 2.53415e+01 MW (current tau)
detect. Gap: 5.69988e-05, intsol-masterLB: 2.12517, real Gap: 5.58287e-05, ub computation returns status2.
breaking the loop due to sufficently accurate feasible point in it74!
done, masterPplant and accepted solution are: 
bus 5: 
60.816; 60.877
66.094; 66.124
65.941; 65.972
56.313; 56.402
67.735; 67.755
76.575; 76.541
66.747; 66.773
69.463; 69.472
67.629; 67.649
50.734; 50.857
50.735; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
50.734; 50.857
52.174; 52.288
66.891; 66.916
79.006; 78.957
79.65; 79.598
88.984; 88.875
96.712; 96.556
97.574; 97.414
97.793; 97.63
97.348; 97.189
97.413; 97.253
97.303; 97.143
97.375; 97.216
97.407; 97.247
97.356; 97.196
97.42; 97.26
97.249; 97.09
97.251; 97.092
97.426; 97.266
97.356; 97.197
97.265; 97.106
97.263; 97.104
97.274; 97.115
97.379; 97.22
97.233; 97.074
96.986; 96.829
97.09; 96.932
97.135; 96.977
97.023; 96.866
96.983; 96.826
96.973; 96.816
96.949; 96.792
96.871; 96.714
96.834; 96.678
84.341; 84.26
61.294; 61.353
50.735; 50.857
50.765; 50.887
50.764; 50.887
53.593; 53.698
62.935; 62.984
88.903; 88.794
105.43; 105.22
99.242; 99.071
94.282; 94.141
83.383; 83.308
68.262; 68.279
63.65; 63.694
60.293; 60.358
53.245; 53.352
50.734; 50.857
50.734; 50.857
50.734; 50.857
81.153; 81.092

computed cost is: 1.8163e+06. It improved the nominal case cost by 15.172% of the nominal cost. This lifts 99.969% of the cooperation potential
Spent 3.5456 sec in Master, 0.41292 sec trying to compute acceptably good feas Points, and 5.0151 sec in subProblem stage.
Average CPU time per iteration is: 0.12127 sec.
Congested lines in disaggregated operation are: 
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


