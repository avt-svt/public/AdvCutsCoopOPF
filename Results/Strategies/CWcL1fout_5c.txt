optimal value of nominal OPF is: 2.14122e+06

Congested lines in nominal operation are: 
7 to 8 at t15.
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


optimal DR to nominal grid planning prices of bus 5 has cost of: 39365.7
optimal value of integrated optimization is: 1.81625e+06
Congested lines in integrated operation are: 
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


71 MW flexible load is cooperating and the initial threshold tau is 142 MW
maxIters: 500, primary/secondary strategy: 4/0, acceptable gap: 0.0001
iteration 45, culm. feasible Gap is 8.66715e+01 MW, which is below 1.42000e+02 MW (current tau)
detect. Gap: 1.18548e-04, intsol-masterLB: 51.1072, real Gap: 9.04115e-05, ub computation returns status2.
iteration 49, culm. feasible Gap is 8.61021e+01 MW, which is below 1.42000e+02 MW (current tau)
detect. Gap: 1.21521e-04, intsol-masterLB: 50.747, real Gap: 9.35827e-05, ub computation returns status2.
it 50done, CWsubProb on bus 5 has objective 0.031289, and distance 59.104, master obj (lb of Decomp) was: 1.8162e+06
iteration 50, culm. feasible Gap is 5.91035e+01 MW, which is below 1.39054e+02 MW (current tau)
detect. Gap: 7.05541e-05, intsol-masterLB: 50.5785, real Gap: 4.27075e-05, ub computation returns status2.
breaking the loop due to sufficently accurate feasible point in it50!
done, masterPplant and accepted solution are: 
bus 5: 
60.816; 60.817
59.094; 59.094
69.424; 69.424
68.928; 68.928
75.259; 75.259
67.286; 67.287
52.415; 52.415
69.463; 69.463
67.629; 67.629
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 56.407
56.351; 56.351
69.806; 70.516
82.502; 82.502
83.146; 83.146
95.217; 95.217
97.868; 97.868
93.844; 96.965
99.841; 97.341
97.238; 97.238
97.73; 97.284
97.787; 97.378
97.856; 97.32
97.551; 97.308
97.485; 97.293
97.619; 97.277
97.572; 97.259
97.329; 97.238
97.416; 97.216
97.381; 97.192
97.356; 97.167
97.596; 97.139
97.178; 97.11
96.997; 96.997
97.374; 97.003
97.493; 97.048
97.33; 96.975
97.294; 96.937
97.261; 96.897
97.226; 96.855
97.185; 96.812
97.129; 96.768
97.09; 96.721
97.055; 96.673
84.532; 84.532
61.394; 61.58
50.734; 50.735
50.734; 50.735
50.735; 50.735
51.079; 51.196
61.327; 61.327
87.295; 87.295
103.82; 103.82
99.863; 99.863
92.674; 92.674
81.776; 81.776
66.655; 66.655
61.852; 61.852
57.33; 57.437
51.447; 51.447
51.44; 51.44
50.734; 50.735
50.734; 50.735
81.155; 81.155

computed cost is: 1.8163e+06. It improved the nominal case cost by 15.173% of the nominal cost. This lifts 99.976% of the cooperation potential
Spent 1.7985 sec in Master, 0.6498 sec trying to compute acceptably good feas Points, and 2.958 sec in subProblem stage.
Average CPU time per iteration is: 0.10813 sec.
Congested lines in disaggregated operation are: 
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


