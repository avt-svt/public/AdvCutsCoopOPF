optimal value of nominal OPF is: 2.20474e+06

Congested lines in nominal operation are: 
7 to 8 at t14.
7 to 8 at t15.
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.
7 to 8 at t32.


optimal DR to nominal grid planning prices of bus 5 has cost of: 20420.4
optimal DR to nominal grid planning prices of bus 10 has cost of: 56084.2
optimal DR to nominal grid planning prices of bus 9 has cost of: 50331.9
optimal value of integrated optimization is: 1.81802e+06
Congested lines in integrated operation are: 
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.


220.5 MW flexible load is cooperating and the initial threshold tau is 220.5 MW
maxIters: 2500, primary/secondary strategy: 1/0, acceptable gap: 0.0001
it 50done, L1subProb on bus 5 has objective 6.969, and distance 247.4, L1subProb on bus 10 has objective 2.6891, and distance 262.18, L1subProb on bus 9 has objective 4.5813, and distance 400.86, master obj (lb of Decomp) was: 1.8161e+06
it 100done, L1subProb on bus 5 has objective 3.5462, and distance 125.89, L1subProb on bus 10 has objective 1.9644, and distance 191.53, L1subProb on bus 9 has objective 2.2494, and distance 196.82, master obj (lb of Decomp) was: 1.8166e+06
it 150done, L1subProb on bus 5 has objective 3.5329, and distance 125.42, L1subProb on bus 10 has objective 1.4742, and distance 143.74, L1subProb on bus 9 has objective 1.2671, and distance 110.87, master obj (lb of Decomp) was: 1.8168e+06
it 200done, L1subProb on bus 5 has objective 2.1683, and distance 76.976, L1subProb on bus 10 has objective 0.89459, and distance 87.222, L1subProb on bus 9 has objective 1.0972, and distance 96.008, master obj (lb of Decomp) was: 1.817e+06
iteration 202, culm. feasible Gap is 2.19575e+02 MW, which is below 2.20500e+02 MW (current tau)
detect. Gap: 9.95537e-04, intsol-masterLB: 969.324, real Gap: 0.000462607, ub computation returns status2.
it 250done, L1subProb on bus 5 has objective 1.6673, and distance 59.189, L1subProb on bus 10 has objective 0.27804, and distance 27.109, L1subProb on bus 9 has objective 0.74134, and distance 64.867, master obj (lb of Decomp) was: 1.8172e+06
iteration 259, culm. feasible Gap is 1.27275e+02 MW, which is below 1.31197e+02 MW (current tau)
detect. Gap: 7.60045e-04, intsol-masterLB: 831.154, real Gap: 0.000303008, ub computation returns status2.
it 300done, L1subProb on bus 5 has objective 1.5089, and distance 53.565, L1subProb on bus 10 has objective 0.40531, and distance 39.517, L1subProb on bus 9 has objective 0.43238, and distance 37.833, master obj (lb of Decomp) was: 1.8173e+06
it 350done, L1subProb on bus 5 has objective 1.2126, and distance 43.049, L1subProb on bus 10 has objective 0.22097, and distance 21.545, L1subProb on bus 9 has objective 0.50223, and distance 43.945, master obj (lb of Decomp) was: 1.8173e+06
iteration 385, culm. feasible Gap is 7.51380e+01 MW, which is below 7.80625e+01 MW (current tau)
detect. Gap: 6.25223e-04, intsol-masterLB: 629.744, real Gap: 0.000278929, ub computation returns status2.
it 400done, L1subProb on bus 5 has objective 0.67735, and distance 24.046, L1subProb on bus 10 has objective 0.17677, and distance 17.235, L1subProb on bus 9 has objective 0.37738, and distance 33.021, master obj (lb of Decomp) was: 1.8174e+06
it 450done, L1subProb on bus 5 has objective 0.84945, and distance 30.156, L1subProb on bus 10 has objective 0.15923, and distance 15.525, L1subProb on bus 9 has objective 0.49126, and distance 42.985, master obj (lb of Decomp) was: 1.8175e+06
it 500done, L1subProb on bus 5 has objective 0.67376, and distance 23.919, L1subProb on bus 10 has objective 0.17565, and distance 17.126, L1subProb on bus 9 has objective 0.36708, and distance 32.119, master obj (lb of Decomp) was: 1.8175e+06
it 550done, L1subProb on bus 5 has objective 0.53106, and distance 18.852, L1subProb on bus 10 has objective 0.11112, and distance 10.834, L1subProb on bus 9 has objective 0.35498, and distance 31.06, master obj (lb of Decomp) was: 1.8176e+06
it 600done, L1subProb on bus 5 has objective 0.63115, and distance 22.406, L1subProb on bus 10 has objective 0.17713, and distance 17.27, L1subProb on bus 9 has objective 0.32825, and distance 28.722, master obj (lb of Decomp) was: 1.8176e+06
it 650done, L1subProb on bus 5 has objective 0.43686, and distance 15.508, L1subProb on bus 10 has objective 0.17629, and distance 17.188, L1subProb on bus 9 has objective 0.37953, and distance 33.209, master obj (lb of Decomp) was: 1.8177e+06
it 700done, L1subProb on bus 5 has objective 0.34722, and distance 12.326, L1subProb on bus 10 has objective 0.35105, and distance 34.227, L1subProb on bus 9 has objective 0.089337, and distance 7.817, master obj (lb of Decomp) was: 1.8177e+06
it 750done, L1subProb on bus 5 has objective 0.47591, and distance 16.895, L1subProb on bus 10 has objective 0.24623, and distance 24.008, L1subProb on bus 9 has objective 0.30889, and distance 27.028, master obj (lb of Decomp) was: 1.8178e+06
iteration 789, culm. feasible Gap is 4.60840e+01 MW, which is below 4.64472e+01 MW (current tau)
detect. Gap: 5.34151e-04, intsol-masterLB: 214.094, real Gap: 0.000416438, ub computation returns status2.
it 800done, L1subProb on bus 5 has objective 0.39531, and distance 14.034, L1subProb on bus 10 has objective 0.013676, and distance 1.3334, L1subProb on bus 9 has objective 0.37026, and distance 32.398, master obj (lb of Decomp) was: 1.8178e+06
it 850done, L1subProb on bus 5 has objective 0.32079, and distance 11.388, L1subProb on bus 10 has objective 0.027773, and distance 2.7078, L1subProb on bus 9 has objective 0.36095, and distance 31.583, master obj (lb of Decomp) was: 1.8178e+06
it 900done, L1subProb on bus 5 has objective 0.37476, and distance 13.304, L1subProb on bus 10 has objective 0.02091, and distance 2.0387, L1subProb on bus 9 has objective 0.30296, and distance 26.509, master obj (lb of Decomp) was: 1.8178e+06
it 950done, L1subProb on bus 5 has objective 0.50786, and distance 18.029, L1subProb on bus 10 has objective 0.0068979, and distance 0.67255, L1subProb on bus 9 has objective 0.36277, and distance 31.742, master obj (lb of Decomp) was: 1.8179e+06
it 1000done, L1subProb on bus 5 has objective 0.36654, and distance 13.012, L1subProb on bus 10 has objective 0.018023, and distance 1.7572, L1subProb on bus 9 has objective 0.34046, and distance 29.79, master obj (lb of Decomp) was: 1.8179e+06
it 1050done, L1subProb on bus 5 has objective 0.40169, and distance 14.26, L1subProb on bus 10 has objective 0.11604, and distance 11.314, L1subProb on bus 9 has objective 0.232, and distance 20.3, master obj (lb of Decomp) was: 1.8179e+06
iteration 1075, culm. feasible Gap is 2.73981e+01 MW, which is below 2.76361e+01 MW (current tau)
detect. Gap: 2.78816e-04, intsol-masterLB: 101.541, real Gap: 0.000222976, ub computation returns status2.
iteration 1097, culm. feasible Gap is 1.51749e+01 MW, which is below 1.64435e+01 MW (current tau)
detect. Gap: 1.69413e-04, intsol-masterLB: 93.1094, real Gap: 0.000118204, ub computation returns status2.
it 1100done, L1subProb on bus 5 has objective 0.31295, and distance 11.11, L1subProb on bus 10 has objective 0.017151, and distance 1.6722, L1subProb on bus 9 has objective 0.008951, and distance 0.78321, master obj (lb of Decomp) was: 1.8179e+06
it 1150done, L1subProb on bus 5 has objective 0.4584, and distance 16.273, L1subProb on bus 10 has objective 0.00073028, and distance 0.071203, L1subProb on bus 9 has objective 0.027799, and distance 2.4324, master obj (lb of Decomp) was: 1.8179e+06
it 1200done, L1subProb on bus 5 has objective 0.34136, and distance 12.118, L1subProb on bus 10 has objective 0.0013665, and distance 0.13323, L1subProb on bus 9 has objective 0.0083986, and distance 0.73488, master obj (lb of Decomp) was: 1.8179e+06
it 1250done, L1subProb on bus 5 has objective 0.35891, and distance 12.741, L1subProb on bus 10 has objective 0.0019351, and distance 0.18867, L1subProb on bus 9 has objective 0.011001, and distance 0.96258, master obj (lb of Decomp) was: 1.818e+06
iteration 1271, culm. feasible Gap is 1.10423e+01 MW, which is below 1.15503e+01 MW (current tau)
detect. Gap: 1.56711e-04, intsol-masterLB: 63.1201, real Gap: 0.000121996, ub computation returns status2.
it 1300done, L1subProb on bus 5 has objective 0.4797, and distance 17.029, L1subProb on bus 10 has objective 0.00046946, and distance 0.045773, L1subProb on bus 9 has objective 0.054122, and distance 4.7357, master obj (lb of Decomp) was: 1.818e+06
it 1350done, L1subProb on bus 5 has objective 0.46437, and distance 16.485, L1subProb on bus 10 has objective 0.00053224, and distance 0.051894, L1subProb on bus 9 has objective 0.02714, and distance 2.3748, master obj (lb of Decomp) was: 1.818e+06
it 1400done, L1subProb on bus 5 has objective 0.41101, and distance 14.591, L1subProb on bus 10 has objective 0.00020217, and distance 0.019712, L1subProb on bus 9 has objective 0.023343, and distance 2.0425, master obj (lb of Decomp) was: 1.818e+06
it 1450done, L1subProb on bus 5 has objective 0.31965, and distance 11.348, L1subProb on bus 10 has objective 0.0011058, and distance 0.10781, L1subProb on bus 9 has objective 0.036296, and distance 3.1759, master obj (lb of Decomp) was: 1.818e+06
it 1500done, L1subProb on bus 5 has objective 0.41024, and distance 14.563, L1subProb on bus 10 has objective 0.019098, and distance 1.862, L1subProb on bus 9 has objective 0.0068366, and distance 0.5982, master obj (lb of Decomp) was: 1.818e+06
it 1550done, L1subProb on bus 5 has objective 0.32626, and distance 11.582, L1subProb on bus 10 has objective 0.0001126, and distance 0.010978, L1subProb on bus 9 has objective 0.005781, and distance 0.50584, master obj (lb of Decomp) was: 1.818e+06
iteration 1591, culm. feasible Gap is 8.29570e+00 MW, which is below 8.77086e+00 MW (current tau)
detect. Gap: 8.93582e-05, intsol-masterLB: 11.4581, real Gap: 8.30562e-05, ub computation returns status2.
breaking the loop due to sufficently accurate feasible point in it1591!
done, masterPplant and accepted solution are: 
bus 5: 
30.564; 30.564
26.711; 26.711
27.873; 27.873
27.453; 27.453
26.3; 26.3
25.367; 25.535
26.281; 26.281
27.884; 27.884
27.285; 27.285
25.367; 25.686
25.367; 25.367
25.367; 25.367
25.632; 25.632
25.515; 25.63
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
26.478; 26.478
26.544; 26.544
25.977; 25.977
25.601; 25.895
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.952; 25.952
26.565; 26.565
26.023; 26.023
25.727; 25.727
25.367; 25.681
25.367; 25.367
26.001; 26.001
25.911; 25.995
25.367; 25.367
25.435; 25.435
25.367; 25.434
25.367; 25.367
25.662; 25.662
25.367; 25.659
25.367; 25.482
26.151; 26.151
36.68; 36.68
44.433; 44.433
35.031; 35.031
35.163; 35.163
42.306; 42.306
41.561; 41.561
45.869; 45.869
49.714; 49.714
48.7; 48.7
48.37; 48.448
48.565; 48.565
48.314; 48.314
47.89; 47.89
48.591; 48.591
48.615; 48.615
48.285; 48.521
49.021; 48.783
48.64; 48.64
48.602; 48.602
48.654; 48.613
48.63; 48.629
48.281; 48.281
47.54; 47.72
48.386; 48.349
48.872; 48.872
49.009; 48.535
48.277; 48.296
48.32; 48.32
48.297; 48.297
48.322; 48.322
48.4; 48.4
48.023; 48.023
48.278; 48.278
48.808; 48.526
48.159; 48.159
39.49; 39.49
30.339; 30.339
27.789; 27.789
28.106; 28.106
41.437; 41.437
39.691; 39.691
26.003; 26.143
38.165; 38.165
39.545; 39.545
40.519; 40.519
42.322; 42.322
43.914; 43.914
41.312; 41.312
26.887; 26.887
38.267; 38.267
38.66; 38.66
29.12; 29.12
27.765; 28.012
25.367; 28.792

bus 10: 
83.516; 83.516
69.672; 69.672
69.676; 69.676
69.678; 69.678
69.674; 69.674
69.672; 69.672
69.671; 69.671
69.671; 69.671
69.672; 69.672
69.672; 69.672
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.672; 69.672
69.672; 69.672
69.671; 69.671
69.671; 69.671
69.672; 69.672
69.671; 69.672
69.671; 69.671
69.671; 69.671
69.672; 69.672
69.671; 69.672
69.671; 69.671
69.671; 69.671
69.672; 69.672
69.682; 69.682
103.57; 103.57
103.22; 103.22
96.348; 96.348
115.25; 115.25
107.63; 107.63
112.43; 112.43
125.54; 125.54
133.67; 133.67
133.72; 133.72
133.75; 133.75
133.76; 133.76
133.75; 133.75
133.75; 133.75
133.73; 133.73
133.72; 133.72
133.7; 133.7
133.68; 133.68
133.66; 133.66
133.63; 133.63
133.61; 133.61
133.58; 133.58
133.54; 133.54
133.51; 133.51
133.47; 133.47
133.43; 133.43
133.39; 133.39
133.34; 133.34
133.29; 133.29
133.24; 133.24
133.19; 133.19
133.14; 133.14
133.08; 133.08
133.02; 133.02
132.96; 132.96
132.9; 132.9
132.82; 132.82
106.82; 106.83
117.28; 117.16
111.16; 111.16
106.65; 106.65
133.16; 133.16
131.75; 131.75
124.41; 124.41
89.115; 89.234
69.905; 69.905
69.903; 69.903
111.32; 111.32
110.95; 110.95
77.959; 77.959
79.788; 79.788
71.618; 71.618
70.791; 70.791
70.776; 70.776
94.251; 94.251

bus 9: 
74.949; 74.95
62.525; 62.525
62.558; 62.558
62.61; 62.61
62.604; 62.604
62.571; 62.571
62.569; 62.569
62.581; 62.581
62.526; 62.557
62.525; 62.525
62.541; 62.541
62.528; 62.541
62.525; 62.525
62.525; 62.525
62.525; 62.525
62.525; 62.525
62.525; 62.525
62.525; 62.525
62.525; 62.525
62.525; 62.525
62.525; 62.525
62.525; 62.525
62.525; 62.525
62.525; 62.525
62.525; 62.525
62.525; 62.525
62.525; 62.525
62.596; 62.596
62.593; 62.595
62.525; 62.525
62.573; 62.573
62.55; 62.572
62.525; 62.525
62.545; 62.545
62.549; 62.549
62.525; 62.529
62.525; 62.525
62.525; 62.525
62.539; 62.539
62.525; 62.538
62.552; 62.552
64.496; 64.496
64.478; 64.478
68.591; 68.611
85.518; 85.518
83.632; 83.632
91.738; 91.738
104.83; 104.83
108.67; 108.67
119.73; 119.73
119.76; 119.76
119.78; 119.78
119.78; 119.78
119.79; 119.79
119.78; 119.78
119.77; 119.77
119.77; 119.77
119.72; 119.72
119.72; 119.72
119.74; 119.74
119.7; 119.7
119.69; 119.69
119.67; 119.67
119.63; 119.63
119.61; 119.61
119.57; 119.57
119.51; 119.51
119.5; 119.5
119.48; 119.48
119.41; 119.41
119.36; 119.36
119.34; 119.34
119.3; 119.3
119.24; 119.24
119.19; 119.19
119.16; 119.16
119.1; 119.1
118.94; 118.94
118.87; 118.87
94.022; 94.022
103.37; 103.37
99.617; 99.617
99.979; 100.04
129.41; 129.41
119.76; 119.88
118.5; 118.27
118.16; 118.16
90.874; 90.874
66.877; 66.877
65.658; 65.997
62.635; 62.635
64.023; 64.023
63.979; 63.979
69.064; 69.064
68.956; 68.956
90.405; 90.405

computed cost is: 1.8182e+06. It improved the nominal case cost by 17.534% of the nominal cost. This lifts 99.961% of the cooperation potential
Spent 228.27 sec in Master, 0.38707 sec trying to compute acceptably good feas Points, and 108.34 sec in subProblem stage.
Average CPU time per iteration is: 0.21181 sec.
Congested lines in disaggregated operation are: 
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.


