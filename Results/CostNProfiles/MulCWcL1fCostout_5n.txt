optimal value of nominal OPF is: 1.8137e+06

Congested lines in nominal operation are: 


optimal DR to nominal grid planning prices of bus 5 has cost of: 15830
71 MW flexible load is cooperating and the initial threshold tau is 142 MW
maxIters: 500, primary/secondary strategy: 4/1, acceptable gap: 0.0001
iteration 36, culm. feasible Gap is 4.56186e+01 MW, which is below 1.42000e+02 MW (current tau)
detect. Gap: 4.05193e-05, ub computation returns status2.
breaking the loop due to sufficently accurate feasible point in it36!
done, masterPplant and accepted solution are: 
bus 5: 
60.816; 60.817
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
67.602; 67.602
75.032; 75.032
58.257; 58.257
50.735; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.906
50.734; 50.905
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
64.841; 72.264
92.239; 92.239
87.322; 87.322
90.385; 90.385
103.79; 96.947
96.422; 96.538
96.561; 96.561
96.603; 96.706
96.708; 96.708
96.69; 96.799
96.793; 96.793
96.949; 96.971
97.047; 96.957
96.932; 96.932
96.704; 96.913
96.924; 96.902
96.978; 96.879
96.891; 96.855
96.864; 96.829
96.878; 96.801
96.833; 96.772
96.778; 96.74
96.756; 96.707
96.705; 96.672
96.673; 96.636
96.636; 96.597
96.596; 96.557
96.553; 96.516
96.511; 96.472
96.482; 96.427
96.539; 96.381
96.439; 96.332
96.497; 96.282
96.563; 96.231
95.652; 95.652
77.001; 77.001
74.597; 74.597
70.148; 70.249
50.734; 50.735
50.734; 50.735
79.483; 79.483
102.87; 102.87
97.474; 97.474
84.444; 84.444
67.08; 67.08
56.489; 56.489
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.735; 50.735
81.155; 81.155

computed cost is: 1.8044e+06. It improved the nominal case cost by 0.51062% of the nominal cost.
Spent 1.6984 sec in Master, 0.50811 sec trying to compute acceptably good feas Points, and 2.61 sec in subProblem stage.
Average CPU time per iteration is: 0.13379 sec.
Congested lines in disaggregated operation are: 


