optimal value of nominal OPF is: 1.8137e+06

Congested lines in nominal operation are: 


optimal DR to nominal grid planning prices of bus 5 has cost of: 15830
71 MW flexible load is cooperating and the initial threshold tau is 142 MW
maxIters: 500, primary/secondary strategy: 4/0, acceptable gap: 0.0001
it 50done, CWsubProb on bus 5 has objective 0.1777, and distance 346.19, master obj (lb of Decomp) was: 1.8044e+06
iteration 52, culm. feasible Gap is 1.15347e+02 MW, which is below 1.42000e+02 MW (current tau)
detect. Gap: 5.95871e-05, ub computation returns status2.
breaking the loop due to sufficently accurate feasible point in it52!
done, masterPplant and accepted solution are: 
bus 5: 
60.816; 60.817
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
51.265; 51.265
70.727; 70.727
77.605; 77.605
58.257; 58.257
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 57.652
62.62; 62.62
77.392; 77.392
95.897; 95.897
97.383; 97.383
93.682; 96.884
99.536; 96.912
96.187; 96.607
96.752; 96.752
97.315; 97.049
96.507; 96.507
96.478; 96.501
97.493; 96.878
96.426; 96.426
96.065; 96.472
96.889; 96.889
97.037; 96.812
97.121; 96.789
97.385; 96.765
96.738; 96.738
95.294; 95.489
96.025; 96.025
97.193; 97.193
96.821; 96.609
96.309; 96.523
96.947; 96.51
97.152; 96.521
97.04; 96.457
96.932; 96.415
97.78; 96.372
97.481; 96.326
96.805; 96.279
96.864; 96.23
95.376; 95.376
96.494; 95.703
96.557; 96.44
77.034; 77.034
74.63; 74.63
70.407; 70.707
50.734; 50.735
50.734; 50.735
79.517; 79.517
103.13; 103.13
97.507; 97.507
84.477; 84.477
67.113; 67.113
56.304; 56.304
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.735; 50.735
81.155; 81.155

computed cost is: 1.8045e+06. It improved the nominal case cost by 0.50859% of the nominal cost.
Spent 2.1074 sec in Master, 0.48199 sec trying to compute acceptably good feas Points, and 3.5191 sec in subProblem stage.
Average CPU time per iteration is: 0.11747 sec.
Congested lines in disaggregated operation are: 


