optimal value of nominal OPF is: 2.14122e+06

Congested lines in nominal operation are: 
7 to 8 at t15.
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


optimal DR to nominal grid planning prices of bus 5 has cost of: 39365.7
71 MW flexible load is cooperating and the initial threshold tau is 142 MW
maxIters: 500, primary/secondary strategy: 1/0, acceptable gap: 0.0001
iteration 47, culm. feasible Gap is 1.36069e+02 MW, which is below 1.42000e+02 MW (current tau)
detect. Gap: 5.72069e-04, ub computation returns status2.
it 50done, L1subProb on bus 5 has objective 2.113, and distance 150.02, master obj (lb of Decomp) was: 1.8159e+06
it 100done, L1subProb on bus 5 has objective 0.72723, and distance 51.633, master obj (lb of Decomp) was: 1.8161e+06
iteration 126, culm. feasible Gap is 2.59424e+01 MW, which is below 2.95384e+01 MW (current tau)
detect. Gap: 1.38544e-04, ub computation returns status2.
iteration 137, culm. feasible Gap is 2.26447e+01 MW, which is below 2.53715e+01 MW (current tau)
detect. Gap: 1.00815e-04, ub computation returns status2.
iteration 138, culm. feasible Gap is 2.47517e+01 MW, which is below 2.53715e+01 MW (current tau)
detect. Gap: 1.66566e-04, ub computation returns status2.
iteration 142, culm. feasible Gap is 1.72100e+01 MW, which is below 1.81262e+01 MW (current tau)
detect. Gap: 1.13846e-04, ub computation returns status2.
iteration 143, culm. feasible Gap is 1.29521e+01 MW, which is below 1.81262e+01 MW (current tau)
detect. Gap: 8.88738e-05, ub computation returns status2.
breaking the loop due to sufficently accurate feasible point in it143!
done, masterPplant and accepted solution are: 
bus 5: 
60.816; 60.817
66.094; 66.094
68.615; 68.615
61.717; 61.717
61.921; 61.921
71.144; 71.144
69.915; 69.915
69.463; 69.463
67.629; 67.629
50.734; 50.901
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 51.089
64.918; 64.918
77.278; 77.278
77.922; 77.922
86.651; 86.651
95.502; 95.502
97.357; 97.357
98.009; 97.442
96.893; 96.893
97.022; 97.022
97.696; 97.571
96.708; 96.708
96.662; 96.662
96.41; 96.41
96.83; 96.83
97.948; 97.812
97.48; 97.422
97.919; 97.367
98.003; 97.346
97.668; 97.323
96.64; 96.64
96.918; 96.924
98.007; 97.541
97.076; 97.208
97.212; 97.176
96.231; 96.231
96.319; 96.319
97.916; 97.323
97.558; 97.165
97.368; 96.984
97.289; 96.941
97.126; 96.897
96.679; 96.679
84.877; 84.877
65.2; 65.2
53.639; 53.9
52.864; 52.864
52.669; 53.565
53.753; 53.753
61.925; 61.925
87.893; 87.893
104.42; 104.42
99.524; 99.524
93.272; 93.272
82.374; 82.374
67.253; 67.253
62.104; 62.104
60.258; 60.258
54.976; 54.976
54.768; 54.768
56.474; 56.474
54.922; 54.922
72.305; 78.543

computed cost is: 1.8164e+06. It improved the nominal case cost by 15.172% of the nominal cost.
Spent 5.854 sec in Master, 0.48254 sec trying to compute acceptably good feas Points, and 9.8025 sec in subProblem stage.
Within subProblem stage, spent 8.6245 sec solving and 1.177 sec generating cuts.
Average CPU time per iteration is: 0.11286 sec.
Congested lines in disaggregated operation are: 
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


