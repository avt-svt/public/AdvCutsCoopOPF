optimal value of nominal OPF is: 1.8137e+06

Congested lines in nominal operation are: 


optimal DR to nominal grid planning prices of bus 5 has cost of: 15830
optimal value of integrated optimization is: 1.80438e+06
Congested lines in integrated operation are: 


71 MW flexible load is cooperating and the initial threshold tau is 142 MW
maxIters: 500, primary/secondary strategy: 4/1, acceptable gap: 0.0001
iteration 40, culm. feasible Gap is 5.17990e+01 MW, which is below 1.42000e+02 MW (current tau)
detect. Gap: 3.40414e-05, intsol-masterLB: 12.8304, real Gap: 2.69309e-05, ub computation returns status2.
breaking the loop due to sufficently accurate feasible point in it40!
done, masterPplant and accepted solution are: 
bus 5: 
60.816; 60.817
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
65.978; 65.978
73.424; 73.424
58.257; 58.257
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 52.649
50.734; 52.63
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 51.308
66.22; 66.22
86.183; 86.183
95.057; 95.057
99.777; 97.506
98.312; 96.835
96.446; 96.612
96.62; 96.62
96.819; 96.856
96.901; 96.85
96.876; 96.841
96.83; 96.829
96.815; 96.815
96.805; 96.799
96.788; 96.781
96.768; 96.762
96.746; 96.74
96.722; 96.716
96.697; 96.691
96.67; 96.664
96.642; 96.635
96.611; 96.605
96.578; 96.572
96.544; 96.538
96.509; 96.502
96.192; 96.192
96.206; 96.206
96.476; 96.469
96.383; 96.375
96.302; 96.296
96.186; 96.186
95.618; 95.618
95.904; 95.891
96.369; 96.352
96.664; 96.047
96.3; 95.993
77.028; 77.028
74.623; 74.623
70.381; 70.669
50.734; 50.735
50.734; 50.735
79.51; 79.51
102.41; 102.41
97.501; 97.501
84.471; 84.471
67.107; 67.107
56.983; 56.983
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.735; 50.735
81.155; 81.155

computed cost is: 1.8044e+06. It improved the nominal case cost by 0.51104% of the nominal cost. This lifts 99.478% of the cooperation potential
Spent 1.4937 sec in Master, 0.66925 sec trying to compute acceptably good feas Points, and 6.0431 sec in subProblem stage.
Within subProblem stage, spent 5.3761 sec solving and 0.66664 sec generating cuts.
Average CPU time per iteration is: 0.20515 sec.
Congested lines in disaggregated operation are: 


