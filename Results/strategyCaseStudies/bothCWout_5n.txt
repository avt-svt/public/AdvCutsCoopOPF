optimal value of nominal OPF is: 1.8137e+06

Congested lines in nominal operation are: 


optimal DR to nominal grid planning prices of bus 5 has cost of: 15830
optimal value of integrated optimization is: 1.80438e+06
Congested lines in integrated operation are: 


71 MW flexible load is cooperating and the initial threshold tau is 142 MW
maxIters: 500, primary/secondary strategy: 2/0, acceptable gap: 0.0001
it 50done, CWsubProb on bus 5 has objective 0.10099, and distance 190.46, L1subProb on bus 5 has objective 0.60604, and distance 43.029, master obj (lb of Decomp) was: 1.8044e+06
iteration 53, culm. feasible Gap is 1.13976e+02 MW, which is below 1.42000e+02 MW (current tau)
detect. Gap: 2.90799e-04, intsol-masterLB: 19.3534, real Gap: 0.000280076, ub computation returns status2.
iteration 68, culm. feasible Gap is 4.79479e+01 MW, which is below 5.81088e+01 MW (current tau)
detect. Gap: 1.22122e-04, intsol-masterLB: 2.56557, real Gap: 0.0001207, ub computation returns status2.
iteration 69, culm. feasible Gap is 4.91590e+01 MW, which is below 5.66234e+01 MW (current tau)
detect. Gap: 1.26607e-04, intsol-masterLB: 2.31695, real Gap: 0.000125323, ub computation returns status2.
iteration 71, culm. feasible Gap is 4.30854e+01 MW, which is below 5.32213e+01 MW (current tau)
detect. Gap: 1.11070e-04, intsol-masterLB: 1.61185, real Gap: 0.000110176, ub computation returns status2.
iteration 72, culm. feasible Gap is 3.30054e+01 MW, which is below 5.32213e+01 MW (current tau)
detect. Gap: 8.50008e-05, intsol-masterLB: 1.32695, real Gap: 8.42655e-05, ub computation returns status2.
breaking the loop due to sufficently accurate feasible point in it72!
done, masterPplant and accepted solution are: 
bus 5: 
60.816; 60.991
50.734; 51.084
50.735; 51.084
50.734; 51.084
63.66; 63.787
66.111; 66.196
54.729; 55.009
62.129; 62.282
68.202; 68.25
58.257; 58.476
50.735; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
64.057; 64.177
83.975; 83.751
92.934; 92.556
96.663; 96.221
96.963; 96.516
96.683; 96.24
97.204; 96.753
96.936; 96.489
96.789; 96.345
96.853; 96.408
96.98; 96.532
96.818; 96.373
96.899; 96.452
96.916; 96.47
96.836; 96.391
96.861; 96.415
97.34; 96.886
97.088; 96.639
96.952; 96.504
96.909; 96.463
96.62; 96.178
96.733; 96.29
96.988; 96.54
97.083; 96.633
95.889; 95.46
96.064; 95.632
96.781; 96.337
96.524; 96.084
96.484; 96.045
96.237; 95.802
96.491; 96.051
96.489; 96.05
95.773; 95.346
96.238; 95.803
96.358; 95.921
76.208; 76.118
73.803; 73.755
70.552; 70.56
50.734; 51.084
50.734; 51.084
78.69; 78.557
92.408; 92.039
92.261; 91.895
83.65; 83.432
66.287; 66.368
60.719; 60.896
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.734; 51.084
50.735; 51.084
80.319; 80.159

computed cost is: 1.8045e+06. It improved the nominal case cost by 0.50534% of the nominal cost. This lifts 98.368% of the cooperation potential
Spent 3.5774 sec in Master, 0.39379 sec trying to compute acceptably good feas Points, and 8.9675 sec in subProblem stage.
Within subProblem stage, spent 7.8096 sec solving and 1.1574 sec generating cuts.
Average CPU time per iteration is: 0.1797 sec.
Congested lines in disaggregated operation are: 


