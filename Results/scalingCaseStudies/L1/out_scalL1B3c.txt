optimal value of nominal OPF is: 2.19297e+06

Congested lines in nominal operation are: 
7 to 8 at t14.
7 to 8 at t15.
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


optimal DR to nominal grid planning prices of bus 7 has cost of: 20470
optimal DR to nominal grid planning prices of bus 6 has cost of: 38673.3
optimal DR to nominal grid planning prices of bus 10 has cost of: 55450.7
optimal value of integrated optimization is: 1.81942e+06
Congested lines in integrated operation are: 
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.


228 MW flexible load is cooperating and the initial threshold tau is 228 MW
maxIters: 2500, primary/secondary strategy: 1/0, acceptable gap: 0.0001
it 50done, L1subProb on bus 7 has objective 4.8052, and distance 300.33, L1subProb on bus 6 has objective 3.9736, and distance 270.21, L1subProb on bus 10 has objective 3.5353, and distance 344.69, master obj (lb of Decomp) was: 1.8174e+06
it 100done, L1subProb on bus 7 has objective 3.3306, and distance 208.16, L1subProb on bus 6 has objective 3.2892, and distance 223.67, L1subProb on bus 10 has objective 1.6694, and distance 162.77, master obj (lb of Decomp) was: 1.8179e+06
it 150done, L1subProb on bus 7 has objective 1.7762, and distance 111.01, L1subProb on bus 6 has objective 1.8876, and distance 128.35, L1subProb on bus 10 has objective 1.079, and distance 105.2, master obj (lb of Decomp) was: 1.8182e+06
it 200done, L1subProb on bus 7 has objective 1.4411, and distance 90.07, L1subProb on bus 6 has objective 1.4071, and distance 95.681, L1subProb on bus 10 has objective 0.54873, and distance 53.501, master obj (lb of Decomp) was: 1.8184e+06
iteration 210, culm. feasible Gap is 2.24477e+02 MW, which is below 2.28000e+02 MW (current tau)
detect. Gap: 1.10143e-03, intsol-masterLB: 1007.09, real Gap: 0.000548214, ub computation returns status2.
it 250done, L1subProb on bus 7 has objective 0.92775, and distance 57.984, L1subProb on bus 6 has objective 0.8746, and distance 59.473, L1subProb on bus 10 has objective 0.37633, and distance 36.692, master obj (lb of Decomp) was: 1.8185e+06
it 300done, L1subProb on bus 7 has objective 0.74404, and distance 46.503, L1subProb on bus 6 has objective 0.86764, and distance 58.999, L1subProb on bus 10 has objective 0.36719, and distance 35.801, master obj (lb of Decomp) was: 1.8186e+06
it 350done, L1subProb on bus 7 has objective 0.61076, and distance 38.173, L1subProb on bus 6 has objective 0.68435, and distance 46.536, L1subProb on bus 10 has objective 0.35633, and distance 34.743, master obj (lb of Decomp) was: 1.8187e+06
it 400done, L1subProb on bus 7 has objective 0.49997, and distance 31.248, L1subProb on bus 6 has objective 0.51823, and distance 35.24, L1subProb on bus 10 has objective 0.14949, and distance 14.575, master obj (lb of Decomp) was: 1.8188e+06
it 450done, L1subProb on bus 7 has objective 0.44888, and distance 28.055, L1subProb on bus 6 has objective 0.55546, and distance 37.771, L1subProb on bus 10 has objective 0.10516, and distance 10.253, master obj (lb of Decomp) was: 1.8188e+06
it 500done, L1subProb on bus 7 has objective 0.37215, and distance 23.259, L1subProb on bus 6 has objective 0.47036, and distance 31.984, L1subProb on bus 10 has objective 0.10592, and distance 10.328, master obj (lb of Decomp) was: 1.8189e+06
it 550done, L1subProb on bus 7 has objective 0.50092, and distance 31.308, L1subProb on bus 6 has objective 0.41851, and distance 28.458, L1subProb on bus 10 has objective 0.079841, and distance 7.7845, master obj (lb of Decomp) was: 1.8189e+06
it 600done, L1subProb on bus 7 has objective 0.54378, and distance 33.986, L1subProb on bus 6 has objective 0.50904, and distance 34.615, L1subProb on bus 10 has objective 0.076725, and distance 7.4807, master obj (lb of Decomp) was: 1.819e+06
it 650done, L1subProb on bus 7 has objective 0.52562, and distance 32.851, L1subProb on bus 6 has objective 0.45371, and distance 30.852, L1subProb on bus 10 has objective 0.099084, and distance 9.6607, master obj (lb of Decomp) was: 1.819e+06
it 700done, L1subProb on bus 7 has objective 0.34924, and distance 21.828, L1subProb on bus 6 has objective 0.20995, and distance 14.277, L1subProb on bus 10 has objective 0.21253, and distance 20.722, master obj (lb of Decomp) was: 1.8191e+06
it 750done, L1subProb on bus 7 has objective 0.44099, and distance 27.562, L1subProb on bus 6 has objective 0.35475, and distance 24.123, L1subProb on bus 10 has objective 0.11607, and distance 11.317, master obj (lb of Decomp) was: 1.8191e+06
it 800done, L1subProb on bus 7 has objective 0.44221, and distance 27.638, L1subProb on bus 6 has objective 0.45102, and distance 30.669, L1subProb on bus 10 has objective 0.037294, and distance 3.6361, master obj (lb of Decomp) was: 1.8191e+06
it 850done, L1subProb on bus 7 has objective 0.44788, and distance 27.993, L1subProb on bus 6 has objective 0.45657, and distance 31.047, L1subProb on bus 10 has objective 0.012667, and distance 1.235, master obj (lb of Decomp) was: 1.8192e+06
it 900done, L1subProb on bus 7 has objective 0.3559, and distance 22.244, L1subProb on bus 6 has objective 0.29839, and distance 20.291, L1subProb on bus 10 has objective 0.0003002, and distance 0.029269, master obj (lb of Decomp) was: 1.8192e+06
it 950done, L1subProb on bus 7 has objective 0.32179, and distance 20.112, L1subProb on bus 6 has objective 0.27724, and distance 18.853, L1subProb on bus 10 has objective 0.00014759, and distance 0.01439, master obj (lb of Decomp) was: 1.8192e+06
it 1000done, L1subProb on bus 7 has objective 0.44302, and distance 27.689, L1subProb on bus 6 has objective 0.30704, and distance 20.878, L1subProb on bus 10 has objective 0.00075597, and distance 0.073707, master obj (lb of Decomp) was: 1.8192e+06
it 1050done, L1subProb on bus 7 has objective 0.34096, and distance 21.31, L1subProb on bus 6 has objective 0.37171, and distance 25.276, L1subProb on bus 10 has objective 0.020533, and distance 2.002, master obj (lb of Decomp) was: 1.8193e+06
it 1100done, L1subProb on bus 7 has objective 0.41292, and distance 25.808, L1subProb on bus 6 has objective 0.13627, and distance 9.2661, L1subProb on bus 10 has objective 0.026924, and distance 2.6251, master obj (lb of Decomp) was: 1.8193e+06
it 1150done, L1subProb on bus 7 has objective 0.54424, and distance 34.015, L1subProb on bus 6 has objective 0.063635, and distance 4.3272, L1subProb on bus 10 has objective 0.0067626, and distance 0.65935, master obj (lb of Decomp) was: 1.8193e+06
iteration 1188, culm. feasible Gap is 2.34501e+01 MW, which is below 2.46334e+01 MW (current tau)
detect. Gap: 3.06189e-04, intsol-masterLB: 76.9153, real Gap: 0.000263925, ub computation returns status2.
it 1200done, L1subProb on bus 7 has objective 0.34829, and distance 21.768, L1subProb on bus 6 has objective 0.072177, and distance 4.9081, L1subProb on bus 10 has objective 0.0058994, and distance 0.57519, master obj (lb of Decomp) was: 1.8193e+06
it 1250done, L1subProb on bus 7 has objective 0.39576, and distance 24.735, L1subProb on bus 6 has objective 0.041662, and distance 2.833, L1subProb on bus 10 has objective 0.00072606, and distance 0.070791, master obj (lb of Decomp) was: 1.8194e+06
it 1300done, L1subProb on bus 7 has objective 0.26796, and distance 16.747, L1subProb on bus 6 has objective 0.059231, and distance 4.0277, L1subProb on bus 10 has objective 0.0015282, and distance 0.149, master obj (lb of Decomp) was: 1.8194e+06
iteration 1325, culm. feasible Gap is 9.35692e+00 MW, which is below 9.57374e+00 MW (current tau)
detect. Gap: 9.22673e-05, intsol-masterLB: 25.8442, real Gap: 7.80638e-05, ub computation returns status2.
breaking the loop due to sufficently accurate feasible point in it1325!
done, masterPplant and accepted solution are: 
bus 7: 
53.535; 53.536
44.661; 44.661
45.042; 45.042
45.335; 45.335
48.724; 48.724
48.06; 48.393
46.279; 46.279
48.144; 48.144
46.349; 46.523
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.827; 44.827
44.661; 44.825
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.969; 44.969
44.944; 44.966
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.664; 44.664
44.661; 44.664
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
45.688; 46.315
46.298; 46.298
44.661; 44.661
67.686; 67.686
71.875; 71.875
58.255; 58.861
55.775; 55.775
63.402; 63.402
73.119; 73.119
79.77; 79.77
88.948; 88.948
85.304; 85.304
85.204; 85.204
84.896; 85.205
85.429; 85.429
85.723; 85.723
84.676; 84.676
84.771; 85.109
86.281; 86.02
85.573; 85.573
84.767; 85.095
85.088; 85.088
85.201; 85.277
85.263; 85.263
84.804; 84.804
84.202; 84.202
84.923; 85.013
85.431; 85.431
85.326; 85.326
85.626; 85.444
84.514; 84.514
83.656; 84.507
85.479; 85.479
85.341; 85.341
84.335; 84.335
84.767; 84.767
85.685; 85.515
85.1; 85.09
84.974; 84.974
79.738; 79.738
60.766; 60.766
48.035; 48.035
49.661; 49.661
66.932; 66.932
65.464; 65.464
48.385; 48.385
50.227; 50.227
67.679; 67.679
71.509; 71.509
58.814; 58.814
54.884; 54.884
74.704; 74.704
81.12; 81.12
57.519; 57.519
62.513; 62.513
58.883; 59.508
63.163; 65.305

bus 6: 
58.246; 58.247
50.732; 50.732
50.732; 50.732
48.925; 48.925
48.944; 48.944
48.848; 48.848
48.797; 48.803
48.638; 48.638
48.591; 48.637
48.591; 48.591
48.591; 48.591
48.591; 48.591
48.591; 48.591
48.591; 48.591
48.591; 48.591
48.598; 48.598
48.591; 48.598
48.672; 48.672
48.591; 48.672
48.591; 48.591
48.591; 48.591
48.591; 48.591
48.591; 48.591
48.591; 48.591
48.591; 48.591
48.591; 48.591
48.591; 48.591
48.591; 48.591
48.591; 48.591
48.612; 48.612
48.591; 48.612
48.702; 48.702
48.793; 48.793
48.91; 48.91
48.591; 48.816
48.591; 48.591
48.591; 48.591
48.591; 48.591
48.591; 48.591
48.591; 48.591
48.742; 48.742
59.798; 59.798
73.991; 73.991
63.267; 63.489
52.073; 52.073
72.322; 72.322
82.124; 82.124
82.972; 82.972
92.331; 92.331
93.083; 93.083
93.081; 93.081
92.958; 92.958
92.964; 92.964
92.743; 92.881
92.995; 92.995
93.177; 93.177
92.902; 92.902
92.904; 92.904
93.147; 93.147
93.139; 93.139
93.037; 93.037
92.88; 92.88
92.898; 92.898
92.959; 92.959
92.838; 92.838
92.814; 92.814
92.727; 92.727
92.666; 92.666
92.551; 92.551
92.648; 92.648
92.891; 92.891
92.822; 92.809
92.776; 92.731
92.68; 92.691
92.708; 92.649
92.48; 92.491
92.549; 92.501
76.751; 76.751
56.711; 56.711
54.181; 54.181
70.047; 70.047
91.487; 91.487
80.118; 80.612
84.677; 84.677
95.781; 95.781
89.41; 89.41
80.829; 80.829
69.378; 69.378
60.412; 60.412
59.488; 59.488
59.052; 59.052
49.1; 49.1
49.122; 49.122
77.309; 77.309
77.032; 77.032
48.591; 49.113

bus 10: 
83.516; 83.516
69.673; 69.673
69.672; 69.672
69.673; 69.673
69.671; 69.673
69.677; 69.677
69.691; 69.691
69.696; 69.696
69.685; 69.685
69.671; 69.673
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.674; 69.674
69.683; 69.683
69.671; 69.68
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.674; 69.674
69.677; 69.677
69.673; 69.674
69.671; 69.671
69.674; 69.674
69.671; 69.673
69.675; 69.675
69.676; 69.676
69.671; 69.672
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.672
70.197; 70.197
70.19; 70.19
89.163; 89.163
114.53; 114.53
113.75; 113.75
107.16; 107.16
109.29; 109.29
122.45; 122.45
133.57; 133.57
133.63; 133.63
133.66; 133.66
133.67; 133.67
133.67; 133.67
133.67; 133.67
133.66; 133.66
133.64; 133.64
133.63; 133.63
133.61; 133.61
133.59; 133.59
133.57; 133.57
133.54; 133.54
133.51; 133.51
133.48; 133.48
133.44; 133.44
133.41; 133.41
133.37; 133.37
133.33; 133.33
133.28; 133.28
133.24; 133.24
133.19; 133.19
133.14; 133.14
133.09; 133.09
133.03; 133.03
132.98; 132.97
132.92; 132.91
132.85; 132.85
113.21; 113.21
121.53; 121.53
140.8; 140.8
132.5; 132.5
132.38; 132.38
132.4; 132.4
132.29; 132.29
132.23; 132.21
114.18; 114.18
83.576; 83.576
70.312; 70.312
69.682; 69.696
70.378; 70.378
71.082; 71.082
71.148; 71.148
70.442; 70.442
72.146; 72.146
72.121; 72.121
105.52; 105.52

computed cost is: 1.8196e+06. It improved the nominal case cost by 17.028% of the nominal cost. This lifts 99.962% of the cooperation potential
Spent 200.37 sec in Master, 0.43054 sec trying to compute acceptably good feas Points, and 206.57 sec in subProblem stage.
Within subProblem stage, spent 175.04 sec solving and 31.52 sec generating cuts.
Average CPU time per iteration is: 0.30745 sec.
Congested lines in disaggregated operation are: 
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.


