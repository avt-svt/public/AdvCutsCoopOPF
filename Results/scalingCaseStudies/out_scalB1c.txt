optimal value of nominal OPF is: 2.12795e+06

Congested lines in nominal operation are: 
7 to 8 at t15.
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


optimal DR to nominal grid planning prices of bus 7 has cost of: 19826.8
optimal value of integrated optimization is: 2.11497e+06
Congested lines in integrated operation are: 
7 to 8 at t32.
7 to 8 at t15.
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


62.5 MW flexible load is cooperating and the initial threshold tau is 125 MW
maxIters: 500, primary/secondary strategy: 4/0, acceptable gap: 0.0001
it 50done, CWsubProb on bus 7 has objective 0.20097, and distance 354.8, master obj (lb of Decomp) was: 2.1148e+06
iteration 58, culm. feasible Gap is 9.20472e+01 MW, which is below 1.25000e+02 MW (current tau)
detect. Gap: 9.00501e-05, intsol-masterLB: 72.2526, real Gap: 5.58895e-05, ub computation returns status2.
breaking the loop due to sufficently accurate feasible point in it58!
done, masterPplant and accepted solution are: 
bus 7: 
53.535; 53.536
52.194; 56.071
60.058; 60.058
52.52; 52.52
71.439; 71.439
71.172; 71.172
48.818; 53.672
65.94; 65.94
64.024; 64.024
47.008; 48.14
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 50.287
55.864; 55.864
63.223; 63.223
75.668; 75.668
80.488; 80.488
82.921; 82.921
90.383; 85.621
85.277; 85.277
85.998; 85.456
86.086; 85.805
85.61; 85.61
86.14; 85.613
86.304; 85.628
86.131; 85.603
85.906; 85.589
85.904; 85.573
86.328; 85.555
85.893; 85.536
85.9; 85.515
85.405; 85.405
85.993; 85.422
86.148; 85.481
85.767; 85.414
85.392; 85.385
85.49; 85.355
86.313; 85.323
85.434; 85.29
85.573; 85.255
86.136; 85.218
85.675; 85.18
86.146; 85.141
85.549; 85.1
84.956; 84.956
74.716; 74.716
54.285; 54.63
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
50.979; 50.979
76.959; 76.959
93.472; 93.113
87.468; 87.318
82.229; 82.229
71.281; 71.281
56.096; 56.096
52.832; 52.832
50.313; 50.509
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
71.439; 71.439

computed cost is: 2.1151e+06. It improved the nominal case cost by 0.60425% of the nominal cost. This lifts 99.089% of the cooperation potential
Spent 1.9145 sec in Master, 0.69088 sec trying to compute acceptably good feas Points, and 3.6922 sec in subProblem stage.
Within subProblem stage, spent 3.214 sec solving and 0.47771 sec generating cuts.
Average CPU time per iteration is: 0.10858 sec.
Congested lines in disaggregated operation are: 
7 to 8 at t32.
7 to 8 at t15.
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


