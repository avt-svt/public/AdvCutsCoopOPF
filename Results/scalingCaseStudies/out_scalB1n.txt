optimal value of nominal OPF is: 1.81258e+06

Congested lines in nominal operation are: 


optimal DR to nominal grid planning prices of bus 7 has cost of: 13917.2
optimal value of integrated optimization is: 1.80431e+06
Congested lines in integrated operation are: 


62.5 MW flexible load is cooperating and the initial threshold tau is 125 MW
maxIters: 500, primary/secondary strategy: 4/0, acceptable gap: 0.0001
iteration 47, culm. feasible Gap is 5.13090e+01 MW, which is below 1.25000e+02 MW (current tau)
detect. Gap: 3.03245e-05, intsol-masterLB: 16.6707, real Gap: 2.10854e-05, ub computation returns status2.
breaking the loop due to sufficently accurate feasible point in it47!
done, masterPplant and accepted solution are: 
bus 7: 
53.535; 53.536
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 46.619
46.6; 46.6
50.76; 50.76
69.166; 69.166
72.499; 72.499
52.258; 54.121
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
55.939; 59.537
81.468; 81.468
78.001; 78.001
79.315; 79.315
91.135; 88.132
84.817; 85.201
85.209; 85.209
85.12; 85.12
85.358; 85.201
85.79; 85.372
85.877; 85.277
85.379; 85.264
85.25; 85.25
85.494; 85.233
85.418; 85.216
85.395; 85.196
85.371; 85.175
85.35; 85.152
85.329; 85.128
85.303; 85.102
85.277; 85.074
85.249; 85.045
85.219; 85.015
85.036; 84.982
84.92; 84.92
84.952; 84.899
84.913; 84.89
84.873; 84.839
84.832; 84.799
85.323; 84.759
85.222; 84.716
84.76; 84.672
84.616; 84.616
84.815; 84.575
84.64; 84.537
71.162; 71.162
62.199; 62.199
55.23; 55.479
44.661; 44.661
44.661; 44.661
70.524; 70.524
84.254; 84.254
82.066; 82.066
75.435; 75.435
52.039; 52.039
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
44.661; 44.661
71.439; 71.439

computed cost is: 1.8043e+06. It improved the nominal case cost by 0.45432% of the nominal cost. This lifts 99.54% of the cooperation potential
Spent 1.5167 sec in Master, 0.6043 sec trying to compute acceptably good feas Points, and 3 sec in subProblem stage.
Within subProblem stage, spent 2.6176 sec solving and 0.38199 sec generating cuts.
Average CPU time per iteration is: 0.10896 sec.
Congested lines in disaggregated operation are: 


