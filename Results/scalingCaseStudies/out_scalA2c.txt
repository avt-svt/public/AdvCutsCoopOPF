optimal value of nominal OPF is: 2.16741e+06

Congested lines in nominal operation are: 
7 to 8 at t15.
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.
7 to 8 at t32.


optimal DR to nominal grid planning prices of bus 5 has cost of: 19744.4
optimal DR to nominal grid planning prices of bus 10 has cost of: 54227.5
optimal value of integrated optimization is: 1.81511e+06
Congested lines in integrated operation are: 
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.


133 MW flexible load is cooperating and the initial threshold tau is 266 MW
maxIters: 500, primary/secondary strategy: 4/0, acceptable gap: 0.0001
it 50done, CWsubProb on bus 5 has objective 0.33189, and distance 375.29, CWsubProb on bus 10 has objective 0.47287, and distance 1202.8, master obj (lb of Decomp) was: 1.8144e+06
iteration 72, culm. feasible Gap is 2.58551e+02 MW, which is below 2.66000e+02 MW (current tau)
detect. Gap: 1.80237e-04, intsol-masterLB: 72.8552, real Gap: 0.000140105, ub computation returns status2.
iteration 76, culm. feasible Gap is 1.58193e+02 MW, which is below 1.75624e+02 MW (current tau)
detect. Gap: 1.55186e-04, intsol-masterLB: 68.4795, real Gap: 0.000117463, ub computation returns status2.
iteration 78, culm. feasible Gap is 7.58417e+01 MW, which is below 1.34672e+02 MW (current tau)
detect. Gap: 1.18652e-04, intsol-masterLB: 67.7345, real Gap: 8.1338e-05, ub computation returns status2.
iteration 79, culm. feasible Gap is 8.75848e+01 MW, which is below 1.34672e+02 MW (current tau)
detect. Gap: 8.26846e-05, intsol-masterLB: 59.6127, real Gap: 4.98438e-05, ub computation returns status2.
breaking the loop due to sufficently accurate feasible point in it79!
done, masterPplant and accepted solution are: 
bus 5: 
30.408; 30.408
25.367; 25.367
25.368; 25.368
25.367; 25.368
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 27.368
25.367; 27.348
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
40.577; 40.577
40.426; 40.426
35.209; 37.774
50.321; 50.321
51.939; 49.503
48.279; 48.279
48.264; 48.264
48.33; 48.425
48.449; 48.448
48.439; 48.446
48.939; 48.443
48.68; 48.438
48.258; 48.432
48.657; 48.426
48.645; 48.418
48.598; 48.409
48.544; 48.4
48.727; 48.389
48.996; 48.378
48.603; 48.366
48.336; 48.352
48.807; 48.338
48.587; 48.323
48.279; 48.307
48.695; 48.291
48.781; 48.273
48.149; 48.149
48.175; 48.18
48.447; 48.263
48.334; 48.194
47.763; 47.763
36.156; 36.303
40.577; 40.577
52.462; 52.462
43.212; 43.212
31.236; 31.236
35.853; 35.92
49.845; 49.845
50.765; 50.765
49.045; 48.013
37.594; 37.594
40.577; 41.475
51.84; 51.84
36.667; 36.841
25.367; 25.367
32.12; 32.12
32.052; 32.052
25.367; 25.367
25.367; 25.367
40.577; 40.577

bus 10: 
83.515; 83.516
78.529; 78.529
78.442; 78.442
69.671; 69.672
71.734; 71.734
85.791; 85.791
84.779; 84.779
77.643; 77.643
76.415; 76.416
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
73.221; 74.866
79.523; 79.523
99.657; 99.657
110.17; 110.17
95.922; 95.922
102.92; 102.92
118.27; 118.27
130.99; 130.99
138.28; 136.62
133.88; 133.69
133.7; 133.7
133.78; 133.71
133.89; 133.7
133.76; 133.69
133.7; 133.68
133.94; 133.66
133.69; 133.65
133.34; 133.37
133.35; 133.35
133.01; 133.06
133.22; 133.22
133.71; 133.71
133.66; 133.49
133.6; 133.45
133.53; 133.41
133.27; 133.27
133.15; 133.15
133.16; 133.16
133.46; 133.23
133.44; 133.23
132.97; 132.97
133.07; 132.99
133.25; 133.1
132.89; 132.92
132.88; 132.88
128.89; 128.89
97.005; 97.155
69.671; 69.671
69.671; 69.835
99.22; 99.22
119.89; 119.89
131.2; 131.2
141.63; 141.39
132.45; 132.35
128.48; 128.48
97.018; 97.038
111.45; 111.45
116.01; 116.01
74.604; 74.604
69.671; 69.671
69.671; 69.671
69.671; 69.671
69.671; 69.671
98.094; 99.045

computed cost is: 1.8152e+06. It improved the nominal case cost by 16.25% of the nominal cost. This lifts 99.974% of the cooperation potential
Spent 3.2583 sec in Master, 1.2738 sec trying to compute acceptably good feas Points, and 9.3054 sec in subProblem stage.
Within subProblem stage, spent 8.0473 sec solving and 1.2574 sec generating cuts.
Average CPU time per iteration is: 0.17516 sec.
Congested lines in disaggregated operation are: 
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.


