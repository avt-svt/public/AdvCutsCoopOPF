optimal value of nominal OPF is: 2.12772e+06

Congested lines in nominal operation are: 
7 to 8 at t15.
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


optimal DR to nominal grid planning prices of bus 5 has cost of: 20008
optimal value of integrated optimization is: 1.81912e+06
Congested lines in integrated operation are: 
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


35.5 MW flexible load is cooperating and the initial threshold tau is 71 MW
maxIters: 500, primary/secondary strategy: 4/0, acceptable gap: 0.0001
it 50done, CWsubProb on bus 5 has objective 0.23605, and distance 244.02, master obj (lb of Decomp) was: 1.819e+06
iteration 63, culm. feasible Gap is 7.02798e+01 MW, which is below 7.10000e+01 MW (current tau)
detect. Gap: 1.84115e-04, intsol-masterLB: 46.0069, real Gap: 0.000158828, ub computation returns status2.
iteration 70, culm. feasible Gap is 4.13238e+01 MW, which is below 4.58898e+01 MW (current tau)
detect. Gap: 5.88661e-05, intsol-masterLB: 34.307, real Gap: 4.00077e-05, ub computation returns status2.
breaking the loop due to sufficently accurate feasible point in it70!
done, masterPplant and accepted solution are: 
bus 5: 
30.408; 30.408
27.353; 28.816
39.283; 39.283
42.222; 42.222
35.97; 38.265
34.73; 34.73
37.602; 37.602
42.819; 42.819
34.331; 34.331
25.367; 26.201
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.963
25.367; 25.957
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
30.488; 31.905
45.647; 45.647
50.544; 50.544
48.407; 48.407
49.757; 48.547
48.669; 48.549
48.723; 48.549
48.276; 48.546
48.732; 48.541
48.905; 48.534
48.836; 48.527
48.969; 48.518
48.838; 48.509
48.699; 48.499
48.544; 48.487
48.748; 48.475
48.78; 48.462
48.65; 48.448
48.513; 48.433
48.584; 48.418
48.793; 48.401
48.367; 48.383
48.386; 48.365
48.67; 48.346
48.583; 48.326
48.836; 48.305
48.656; 48.283
48.434; 48.261
48.572; 48.237
48.591; 48.213
48.376; 48.188
48.13; 48.13
45.257; 45.257
33.843; 33.87
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
40.577; 40.577
53.285; 53.003
49.273; 49.235
36.866; 36.866
25.798; 25.798
29.234; 29.234
29.196; 29.196
25.367; 25.596
25.367; 25.367
25.367; 25.367
25.367; 25.367
25.367; 25.367
40.577; 40.577

computed cost is: 1.8192e+06. It improved the nominal case cost by 14.5% of the nominal cost. This lifts 99.976% of the cooperation potential
Spent 2.151 sec in Master, 0.73614 sec trying to compute acceptably good feas Points, and 4.2959 sec in subProblem stage.
Within subProblem stage, spent 3.7045 sec solving and 0.59089 sec generating cuts.
Average CPU time per iteration is: 0.10261 sec.
Congested lines in disaggregated operation are: 
7 to 8 at t16.
7 to 8 at t17.
7 to 8 at t18.
7 to 8 at t19.
7 to 8 at t20.
7 to 8 at t21.
7 to 8 at t22.
7 to 8 at t23.
7 to 8 at t24.
7 to 8 at t25.
7 to 8 at t26.
7 to 8 at t27.
7 to 8 at t28.
7 to 8 at t29.
7 to 8 at t30.
7 to 8 at t31.


