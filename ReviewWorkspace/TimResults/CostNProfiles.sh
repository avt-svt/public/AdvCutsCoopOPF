#!/bin/sh
echo "congInd; nominalCost; IntCost; IntRelCostSav; DisCost; DisRelCostSav; 1stBus; nomECost; DR_Cost; DR_RelECostSav; disECost; disaggRelECostSav" > ./../BetterCutsWorkspace/Cost_5n.csv
echo "congInd; nominalCost; IntCost; IntRelCostSav; DisCost; DisRelCostSav; 1stBus; nomECost; DR_Cost; DR_RelECostSav; disECost; disaggRelECostSav" > ./../BetterCutsWorkspace/Cost_5c.csv
echo "congInd; nominalCost; IntCost; IntRelCostSav; DisCost; DisRelCostSav; 1stBus; nomECost; DR_Cost; DR_RelECostSav; disECost; disaggRelECostSav; 2ndBus; nomECost2; DR_Cost2; DR_RelECostSav2; disECost2; disaggRelECostSav2; 3rdBus; nomECost3; DR_Cost3; DR_RelECostSav3; disECost3; disaggRelECostSav3" > ./../BetterCutsWorkspace/Cost_578n.csv
echo "congInd; nominalCost; IntCost; IntRelCostSav; DisCost; DisRelCostSav; 1stBus; nomECost; DR_Cost; DR_RelECostSav; disECost; disaggRelECostSav; 2ndBus; nomECost2; DR_Cost2; DR_RelECostSav2; disECost2; disaggRelECostSav2; 3rdBus; nomECost3; DR_Cost3; DR_RelECostSav3; disECost3; disaggRelECostSav3" > ./../BetterCutsWorkspace/Cost_578c.csv


#L1_strategy#
echo "LP_NOMINAL_OPERATION loadflow_5n.lp \n LP_PRICE_CALCULATION loadflow_price_5n.lp \n LP_INTEGRATED loadflow_integrated_5n.lp \n LP_SUB_DR sub_DR.lp \n NR_TIMESTEPS 96 \n MULTI_PURPOSE_STRING 5 \n PLANTBUSSES 5 1 \n USE_INTEGRATED 0 \n CONG_INDICATOR n \n OUTPUT_FILE L1Costout_5n.txt \n CSV_FILE L1Costres_5n \n CASE_STUDY_CSV Cost_5n \n CASE_STUDY_INFO 2" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_main.txt
echo "LP_MASTER_FIXED loadflow_5n.lp \n MAXIMUM_ITERATIONS 500 \n PRIMARY_SUB_TYPE 1 \n SECONDARY_SUB_TYPE 0 \n MU 0 \n TAU_FACTOR 2 \n ACCEPTABLE_GAP 1E-4 \n SAFETY_FACTOR 1.19 \n SUB_FEAS_VALUE 1e-7" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_algorithm.txt
./CoopBendersDec /Users/tim/repos/cooperatopf/BetterCutsWorkspace/

echo "LP_NOMINAL_OPERATION loadflow_5c.lp \n LP_PRICE_CALCULATION loadflow_price_5c.lp \n LP_INTEGRATED loadflow_integrated_5c.lp \n LP_SUB_DR sub_DR.lp \n NR_TIMESTEPS 96 \n MULTI_PURPOSE_STRING 5 \n PLANTBUSSES 5 1 \n USE_INTEGRATED 0 \n CONG_INDICATOR c \n OUTPUT_FILE L1Costout_5c.txt \n CSV_FILE L1Costres_5c \n CASE_STUDY_CSV Cost_5c \n CASE_STUDY_INFO 2" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_main.txt
echo "LP_MASTER_FIXED loadflow_5c.lp \n MAXIMUM_ITERATIONS 500 \n PRIMARY_SUB_TYPE 1 \n SECONDARY_SUB_TYPE 0 \n MU 0 \n TAU_FACTOR 2 \n ACCEPTABLE_GAP 1E-4 \n SAFETY_FACTOR 1.19 \n SUB_FEAS_VALUE 1e-7" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_algorithm.txt
./CoopBendersDec /Users/tim/repos/cooperatopf/BetterCutsWorkspace/

echo "LP_NOMINAL_OPERATION loadflow_578n.lp \n LP_PRICE_CALCULATION loadflow_price_578n.lp \n LP_INTEGRATED loadflow_integrated_578n.lp \n LP_SUB_DR sub_DR.lp \n NR_TIMESTEPS 96 \n MULTI_PURPOSE_STRING 5 \n PLANTBUSSES 5 0.462 7 0.2536 8 0.038 \n USE_INTEGRATED 0 \n CONG_INDICATOR n \n OUTPUT_FILE L1Costout_578n.txt \n CSV_FILE L1Costres_578n \n CASE_STUDY_CSV Cost_578n \n CASE_STUDY_INFO 2" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_main.txt
echo "LP_MASTER_FIXED loadflow_578n.lp \n MAXIMUM_ITERATIONS 500 \n PRIMARY_SUB_TYPE 1 \n SECONDARY_SUB_TYPE 0 \n MU 0 \n TAU_FACTOR 2 \n ACCEPTABLE_GAP 1E-4 \n SAFETY_FACTOR 1.19 \n SUB_FEAS_VALUE 1e-7" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_algorithm.txt
./CoopBendersDec /Users/tim/repos/cooperatopf/BetterCutsWorkspace/

echo "LP_NOMINAL_OPERATION loadflow_578c.lp \n LP_PRICE_CALCULATION loadflow_price_578c.lp \n LP_INTEGRATED loadflow_integrated_578c.lp \n LP_SUB_DR sub_DR.lp \n NR_TIMESTEPS 96 \n MULTI_PURPOSE_STRING 5 \n PLANTBUSSES 5 0.462 7 0.2536 8 0.038 \n USE_INTEGRATED 0 \n CONG_INDICATOR c \n OUTPUT_FILE L1Costout_578c.txt \n CSV_FILE L1Costres_578c \n CASE_STUDY_CSV Cost_578c \n CASE_STUDY_INFO 2" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_main.txt
echo "LP_MASTER_FIXED loadflow_578c.lp \n MAXIMUM_ITERATIONS 500 \n PRIMARY_SUB_TYPE 1 \n SECONDARY_SUB_TYPE 0 \n MU 0 \n TAU_FACTOR 2 \n ACCEPTABLE_GAP 1E-4 \n SAFETY_FACTOR 1.19 \n SUB_FEAS_VALUE 1e-7" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_algorithm.txt
./CoopBendersDec /Users/tim/repos/cooperatopf/BetterCutsWorkspace/


#CWcL1f_strategy#
echo "LP_NOMINAL_OPERATION loadflow_5n.lp \n LP_PRICE_CALCULATION loadflow_price_5n.lp \n LP_INTEGRATED loadflow_integrated_5n.lp \n LP_SUB_DR sub_DR.lp \n NR_TIMESTEPS 96 \n MULTI_PURPOSE_STRING 5 \n PLANTBUSSES 5 1 \n USE_INTEGRATED 0 \n CONG_INDICATOR n \n OUTPUT_FILE CWcL1fCostout_5n.txt \n CSV_FILE CWcL1fCostres_5n \n CASE_STUDY_CSV Cost_5n \n CASE_STUDY_INFO 2" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_main.txt
echo "LP_MASTER_FIXED loadflow_5n.lp \n MAXIMUM_ITERATIONS 500 \n PRIMARY_SUB_TYPE 4 \n SECONDARY_SUB_TYPE 0 \n MU 0 \n TAU_FACTOR 2 \n ACCEPTABLE_GAP 1E-4 \n SAFETY_FACTOR 1.19 \n SUB_FEAS_VALUE 1e-7" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_algorithm.txt
./CoopBendersDec /Users/tim/repos/cooperatopf/BetterCutsWorkspace/

echo "LP_NOMINAL_OPERATION loadflow_5c.lp \n LP_PRICE_CALCULATION loadflow_price_5c.lp \n LP_INTEGRATED loadflow_integrated_5c.lp \n LP_SUB_DR sub_DR.lp \n NR_TIMESTEPS 96 \n MULTI_PURPOSE_STRING 5 \n PLANTBUSSES 5 1 \n USE_INTEGRATED 0 \n CONG_INDICATOR c \n OUTPUT_FILE CWcL1fCostout_5c.txt \n CSV_FILE CWcL1fCostres_5c \n CASE_STUDY_CSV Cost_5c \n CASE_STUDY_INFO 2" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_main.txt
echo "LP_MASTER_FIXED loadflow_5c.lp \n MAXIMUM_ITERATIONS 500 \n PRIMARY_SUB_TYPE 4 \n SECONDARY_SUB_TYPE 0 \n MU 0 \n TAU_FACTOR 2 \n ACCEPTABLE_GAP 1E-4 \n SAFETY_FACTOR 1.19 \n SUB_FEAS_VALUE 1e-7" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_algorithm.txt
./CoopBendersDec /Users/tim/repos/cooperatopf/BetterCutsWorkspace/

echo "LP_NOMINAL_OPERATION loadflow_578n.lp \n LP_PRICE_CALCULATION loadflow_price_578n.lp \n LP_INTEGRATED loadflow_integrated_578n.lp \n LP_SUB_DR sub_DR.lp \n NR_TIMESTEPS 96 \n MULTI_PURPOSE_STRING 5 \n PLANTBUSSES 5 0.462 7 0.2536 8 0.038 \n USE_INTEGRATED 0 \n CONG_INDICATOR n \n OUTPUT_FILE CWcL1fCostout_578n.txt \n CSV_FILE CWcL1fCostres_578n \n CASE_STUDY_CSV Cost_578n \n CASE_STUDY_INFO 2" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_main.txt
echo "LP_MASTER_FIXED loadflow_578n.lp \n MAXIMUM_ITERATIONS 500 \n PRIMARY_SUB_TYPE 4 \n SECONDARY_SUB_TYPE 0 \n MU 0 \n TAU_FACTOR 2 \n ACCEPTABLE_GAP 1E-4 \n SAFETY_FACTOR 1.19 \n SUB_FEAS_VALUE 1e-7" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_algorithm.txt
./CoopBendersDec /Users/tim/repos/cooperatopf/BetterCutsWorkspace/

echo "LP_NOMINAL_OPERATION loadflow_578c.lp \n LP_PRICE_CALCULATION loadflow_price_578c.lp \n LP_INTEGRATED loadflow_integrated_578c.lp \n LP_SUB_DR sub_DR.lp \n NR_TIMESTEPS 96 \n MULTI_PURPOSE_STRING 5 \n PLANTBUSSES 5 0.462 7 0.2536 8 0.038 \n USE_INTEGRATED 0 \n CONG_INDICATOR c \n OUTPUT_FILE CWcL1fCostout_578c.txt \n CSV_FILE CWcL1fCostres_578c \n CASE_STUDY_CSV Cost_578c \n CASE_STUDY_INFO 2" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_main.txt
echo "LP_MASTER_FIXED loadflow_578c.lp \n MAXIMUM_ITERATIONS 500 \n PRIMARY_SUB_TYPE 4 \n SECONDARY_SUB_TYPE 0 \n MU 0 \n TAU_FACTOR 2 \n ACCEPTABLE_GAP 1E-4 \n SAFETY_FACTOR 1.19 \n SUB_FEAS_VALUE 1e-7" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_algorithm.txt
./CoopBendersDec /Users/tim/repos/cooperatopf/BetterCutsWorkspace/


#multiCWcL1f_strategy#
echo "LP_NOMINAL_OPERATION loadflow_5n.lp \n LP_PRICE_CALCULATION loadflow_price_5n.lp \n LP_INTEGRATED loadflow_integrated_5n.lp \n LP_SUB_DR sub_DR.lp \n NR_TIMESTEPS 96 \n MULTI_PURPOSE_STRING 6 \n PLANTBUSSES 5 1 \n USE_INTEGRATED 0 \n CONG_INDICATOR n \n OUTPUT_FILE MulCWcL1fCostout_5n.txt \n CSV_FILE MulCWcL1fCostres_5n \n CASE_STUDY_CSV Cost_5n \n CASE_STUDY_INFO 2" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_main.txt
echo "LP_MASTER_FIXED loadflow_5n.lp \n MAXIMUM_ITERATIONS 500 \n PRIMARY_SUB_TYPE 4 \n SECONDARY_SUB_TYPE 1 \n MU 0.97 \n TAU_FACTOR 2 \n ACCEPTABLE_GAP 1E-4 \n SAFETY_FACTOR 1.19 \n SUB_FEAS_VALUE 1e-7" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_algorithm.txt
./CoopBendersDec /Users/tim/repos/cooperatopf/BetterCutsWorkspace/

echo "LP_NOMINAL_OPERATION loadflow_5c.lp \n LP_PRICE_CALCULATION loadflow_price_5c.lp \n LP_INTEGRATED loadflow_integrated_5c.lp \n LP_SUB_DR sub_DR.lp \n NR_TIMESTEPS 96 \n MULTI_PURPOSE_STRING 6 \n PLANTBUSSES 5 1 \n USE_INTEGRATED 0 \n CONG_INDICATOR c \n OUTPUT_FILE MulCWcL1fCostout_5c.txt \n CSV_FILE MulCWcL1fCostres_5c \n CASE_STUDY_CSV Cost_5c \n CASE_STUDY_INFO 2" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_main.txt
echo "LP_MASTER_FIXED loadflow_5c.lp \n MAXIMUM_ITERATIONS 500 \n PRIMARY_SUB_TYPE 4 \n SECONDARY_SUB_TYPE 1 \n MU 0.97 \n TAU_FACTOR 2 \n ACCEPTABLE_GAP 1E-4 \n SAFETY_FACTOR 1.19 \n SUB_FEAS_VALUE 1e-7" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_algorithm.txt
./CoopBendersDec /Users/tim/repos/cooperatopf/BetterCutsWorkspace/
DisCost; 
echo "LP_NOMINAL_OPERATION loadflow_578n.lp \n LP_PRICE_CALCULATION loadflow_price_578n.lp \n LP_INTEGRATED loadflow_integrated_578n.lp \n LP_SUB_DR sub_DR.lp \n NR_TIMESTEPS 96 \n MULTI_PURPOSE_STRING 6 \n PLANTBUSSES 5 0.462 7 0.2536 8 0.038 \n USE_INTEGRATED 0 \n CONG_INDICATOR n \n OUTPUT_FILE MulCWcL1fCostout_578n.txt \n CSV_FILE MulCWcL1fCostres_578n \n CASE_STUDY_CSV Cost_578n \n CASE_STUDY_INFO 2" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_main.txt
echo "LP_MASTER_FIXED loadflow_578n.lp \n MAXIMUM_ITERATIONS 500 \n PRIMARY_SUB_TYPE 4 \n SECONDARY_SUB_TYPE 1 \n MU 0.97 \n TAU_FACTOR 2 \n ACCEPTABLE_GAP 1E-4 \n SAFETY_FACTOR 1.19 \n SUB_FEAS_VALUE 1e-7" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_algorithm.txt
./CoopBendersDec /Users/tim/repos/cooperatopf/BetterCutsWorkspace/

echo "LP_NOMINAL_OPERATION loadflow_578c.lp \n LP_PRICE_CALCULATION loadflow_price_578c.lp \n LP_INTEGRATED loadflow_integrated_578c.lp \n LP_SUB_DR sub_DR.lp \n NR_TIMESTEPS 96 \n MULTI_PURPOSE_STRING 6 \n PLANTBUSSES 5 0.462 7 0.2536 8 0.038 \n USE_INTEGRATED 0 \n CONG_INDICATOR c \n OUTPUT_FILE MulCWcL1fCostout_578c.txt \n CSV_FILE MulCWcL1fCostres_578c \n CASE_STUDY_CSV Cost_578c \n CASE_STUDY_INFO 2" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_main.txt
echo "LP_MASTER_FIXED loadflow_578c.lp \n MAXIMUM_ITERATIONS 500 \n PRIMARY_SUB_TYPE 4 \n SECONDARY_SUB_TYPE 1 \n MU 0.97 \n TAU_FACTOR 2 \n ACCEPTABLE_GAP 1E-4 \n SAFETY_FACTOR 1.19 \n SUB_FEAS_VALUE 1e-7" > /Users/tim/repos/cooperatopf/BetterCutsWorkspace/options_algorithm.txt
./CoopBendersDec /Users/tim/repos/cooperatopf/BetterCutsWorkspace/



