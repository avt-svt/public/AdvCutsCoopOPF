optimal value of nominal OPF is: 1.8137e+06

Congested lines in nominal operation are: 


optimal DR to nominal grid planning prices of bus 5 has cost of: 15830
71 MW flexible load is cooperating and the initial threshold tau is 142 MW
maxIters: 500, primary/secondary strategy: 1/0, acceptable gap: 0.0001
it 50done, L1subProb on bus 5 has objective 2.3066, and distance 163.77, master obj (lb of Decomp) was: 1.8042e+06
iteration 55, culm. feasible Gap is 1.41539e+02 MW, which is below 1.42000e+02 MW (current tau)
detect. Gap: 2.84317e-04, ub computation returns status2.
iteration 79, culm. feasible Gap is 8.05014e+01 MW, which is below 8.44900e+01 MW (current tau)
detect. Gap: 1.94833e-04, ub computation returns status2.
it 100done, L1subProb on bus 5 has objective 0.69509, and distance 49.351, master obj (lb of Decomp) was: 1.8043e+06
iteration 100, culm. feasible Gap is 4.93511e+01 MW, which is below 5.16047e+01 MW (current tau)
detect. Gap: 1.44744e-04, ub computation returns status2.
iteration 121, culm. feasible Gap is 3.86573e+01 MW, which is below 4.24263e+01 MW (current tau)
detect. Gap: 1.17423e-04, ub computation returns status2.
iteration 122, culm. feasible Gap is 3.41409e+01 MW, which is below 4.24263e+01 MW (current tau)
detect. Gap: 1.00257e-04, ub computation returns status2.
iteration 124, culm. feasible Gap is 3.11441e+01 MW, which is below 4.24263e+01 MW (current tau)
detect. Gap: 1.18327e-04, ub computation returns status2.
iteration 125, culm. feasible Gap is 3.36365e+01 MW, which is below 4.24263e+01 MW (current tau)
detect. Gap: 1.18717e-04, ub computation returns status2.
iteration 127, culm. feasible Gap is 3.81158e+01 MW, which is below 4.24263e+01 MW (current tau)
detect. Gap: 1.06443e-04, ub computation returns status2.
iteration 129, culm. feasible Gap is 3.77353e+01 MW, which is below 4.24263e+01 MW (current tau)
detect. Gap: 1.45114e-04, ub computation returns status2.
iteration 134, culm. feasible Gap is 2.82583e+01 MW, which is below 3.47915e+01 MW (current tau)
detect. Gap: 1.24139e-04, ub computation returns status2.
iteration 143, culm. feasible Gap is 2.87183e+01 MW, which is below 3.33513e+01 MW (current tau)
detect. Gap: 8.43794e-05, ub computation returns status2.
breaking the loop due to sufficently accurate feasible point in it143!
done, masterPplant and accepted solution are: 
bus 5: 
60.816; 60.817
50.734; 50.735
57.826; 57.826
58.588; 58.588
57.355; 57.355
62.086; 62.086
68.294; 68.294
79.54; 79.54
70.459; 70.459
52.145; 53.675
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 50.735
50.734; 51.468
63.753; 63.753
83.836; 83.836
92.488; 92.488
94.829; 94.829
97.967; 97.057
97.031; 97.074
97.32; 97.085
95.611; 96.128
96.137; 96.137
95.584; 96.148
96.154; 96.154
95.742; 96.831
96.823; 96.823
95.884; 97.047
97.536; 97.029
96.592; 96.592
96.974; 96.766
98.008; 97.15
97.689; 96.932
98.592; 96.903
95.422; 95.422
95.097; 96.074
97.729; 97.467
96.536; 96.536
97.201; 96.607
96.833; 96.791
96.692; 96.646
97.225; 96.604
96.519; 96.519
95.613; 95.613
97.327; 96.021
98.627; 96.818
96.328; 96.363
96.996; 96.311
95.207; 95.207
81.804; 81.804
66.137; 66.137
56.813; 56.813
53.555; 53.555
50.734; 53.413
78.173; 78.173
91.891; 91.891
85.154; 85.154
83.134; 83.134
65.77; 65.77
56.685; 56.685
52.93; 53.569
50.734; 50.735
50.734; 50.735
50.734; 50.735
52.418; 52.418
54.763; 54.763
50.734; 53.073
67.553; 73.422

computed cost is: 1.8045e+06. It improved the nominal case cost by 0.50834% of the nominal cost.
Spent 6.4365 sec in Master, 0.47024 sec trying to compute acceptably good feas Points, and 9.5033 sec in subProblem stage.
Average CPU time per iteration is: 0.11476 sec.
Congested lines in disaggregated operation are: 


