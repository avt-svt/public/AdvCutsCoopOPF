/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

/**
* \file coopBendersAlgorithm.cpp
* \brief Implementation of %CoopBenderAlgorithm (except for solve and cut generation methods) and %PriceProblem.
*/

#include "coopBendersAlgorithm.hpp"
#include <sstream>
#include <iomanip>
#include <thread>

namespace decoop {

	/**
	* \details Sets some parameters on master_model_ and fixed_model_, these are initialized before in constructor initializer list.
	*		   Fills vectors #master_p_plant_var_ and #fx_p_plant_var_.
	*/
	void CoopBendersAlgorithm::SetUpMasterAndFixed() {
		master_model_.set(GRB_IntParam_LogToConsole, 0);
		fixed_model_.set(GRB_IntParam_LogToConsole, 0);
		master_model_.set(GRB_IntParam_Method, 1);
		fixed_model_.set(GRB_IntParam_Method, 1);
		//Fill vectors for var_p_plant and p_plant_var
		for (auto pb_rec : plantbusses_) {
			master_p_plant_var_.emplace_back();
			fx_p_plant_var_.emplace_back();
			std::vector<GRBVar>& master = master_p_plant_var_.back();
			std::vector<GRBVar>& fixed = fx_p_plant_var_.back();
			for (unsigned t = 1; t <= num_timesteps_; ++t) {
				std::string name = "Pplant(" + std::to_string(pb_rec.first) + ",t" + std::to_string(t) + ")";
				master.push_back(master_model_.getVarByName(name));
				fixed.push_back(fixed_model_.getVarByName(name));
			}
		}
	}

	/**
	* \details Calls CreateSubproblemObjects() to set up vectors #sub_prob_ and #pb_info_.
	*		   Sets all the iterators to various points in #sub_prob_ vector.
	*		   Computes total flexible load of all cooperating busses.
	*/
	double CoopBendersAlgorithm::SetUpSub(const std::string& workspace_path, const std::unordered_map<int, double>& bus_data) {
		for (GRBEnv& env : environments_) {
			env.set(GRB_IntParam_LogToConsole, 0);
		}

		double flex_pow{};

		if (options_.primary_sp_type == 0) { //only CW
			sub_prob_.reserve(plantbusses_.size() * (options_.secondary_sp_type + 1));
			flex_pow = CreateSubproblemObjects(workspace_path, bus_data, SubproblemType::kCWSubproblem, false);
			cw_begin_ = sub_prob_.begin();
			cw_end_ = sub_prob_.end();
			l1_begin_ = sub_prob_.end();
			l1_end_ = sub_prob_.end();
			feas_begin_ = sub_prob_.begin();
			feas_end_ = sub_prob_.end();
		}
		else if (options_.primary_sp_type == 1) { //only L1
			sub_prob_.reserve(plantbusses_.size() * (options_.secondary_sp_type + 1));
			flex_pow = CreateSubproblemObjects(workspace_path, bus_data, SubproblemType::kL1Subproblem, false);
			cw_begin_ = sub_prob_.begin();
			cw_end_ = sub_prob_.begin();
			l1_begin_ = sub_prob_.begin();
			l1_end_ = sub_prob_.end();
			feas_begin_ = sub_prob_.begin();
			feas_end_ = sub_prob_.end();
		}
		else { //multiple subproblem types
			sub_prob_.reserve(plantbusses_.size() * (options_.secondary_sp_type + 2));
			flex_pow = CreateSubproblemObjects(workspace_path, bus_data, SubproblemType::kCWSubproblem, false);
			CreateSubproblemObjects(workspace_path, bus_data, SubproblemType::kL1Subproblem, true);
			cw_begin_ = sub_prob_.begin();
			cw_end_ = sub_prob_.begin() + plantbusses_.size();
			l1_begin_ = sub_prob_.begin() + plantbusses_.size();
			l1_end_ = sub_prob_.end();
			if (options_.primary_sp_type == 2) { // both cuts and CW feas pts
				feas_begin_ = cw_begin_;
				feas_end_ = cw_end_;
			}
			else if (options_.primary_sp_type == 3) { // both cuts and L1 feas pts
				feas_begin_ = l1_begin_;
				feas_end_ = l1_end_;
			}
			else { // tailored stratey: CW cuts and L1 feas pts
				feas_begin_ = l1_begin_;
				feas_end_ = l1_end_;
				l1_end_ = l1_begin_;
			}
		}
        if (options_.secondary_sp_type == 1) {// multi-cut strategy
			unsigned long last_size = sub_prob_.size();
			CreateSubproblemObjects(workspace_path, bus_data, SubproblemType::kCWSubproblem, true);

			// iterators to the end of sub_prob_ get invalidated through the call to CreateSubproblemObjects,
			// so they are re-assigned here
			switch (options_.primary_sp_type) {
			case 0:
				cw_end_ = cw_begin_ + plantbusses_.size();
				feas_end_ = cw_end_;
				break;
			case 2:
				l1_end_ = l1_begin_ + plantbusses_.size();
				feas_end_ = cw_end_;
				break;
			case 3:
				l1_end_ = l1_begin_ + plantbusses_.size();
				feas_end_ = l1_end_;
				break;
			case 4:
				feas_end_ = l1_begin_ + plantbusses_.size();
				break;
			}

			secondary_begin_ = sub_prob_.begin() + last_size;
			secondary_end_ = sub_prob_.end();
		}
		else {
			secondary_begin_ = sub_prob_.end();
			secondary_end_ = sub_prob_.end();
		}

		tau_ = flex_pow * options_.initial_tau_factor;
		out_file_ << flex_pow << " MW flexible load is cooperating and the initial threshold tau is " << tau_ << " MW" << std::endl;
		return flex_pow;
	}

	double CoopBendersAlgorithm::CreateSubproblemObjects(const std::string& workspace_path, const std::unordered_map<int, double>& bus_data, const SubproblemType sp_type, const bool add_environments) {
		double flex_pow{};
		int b{};

		if (sp_type == SubproblemType::kCWSubproblem) {
			if (add_environments) {
				//adding environments for CW problems means we are adding secondary problems (primary CW is the first problem type added if it exists),
				//so we don't need more entries in last_primary_feas_sol_ or pb_info_
				for (auto pb_rec : plantbusses_) {
					environments_.emplace_back();
					environments_.back().set(GRB_IntParam_LogToConsole, 0);
					sub_prob_.emplace_back(std::in_place_type<PlantbusSubproblemCW>, environments_.back(), workspace_path, bus_data.at(pb_rec.first) * pb_rec.second, num_timesteps_);
					flex_pow += std::get<PlantbusSubproblemCW>(sub_prob_.back()).get_plant_load();
				}
			}
			else {
				for (auto pb_rec : plantbusses_) {
					sub_prob_.emplace_back(std::in_place_type<PlantbusSubproblemCW>, environments_[b], workspace_path, bus_data.at(pb_rec.first) * pb_rec.second, num_timesteps_);
					pb_info_.emplace_back();
					last_primary_feas_sol_.emplace_back(num_timesteps_);
					flex_pow += std::get<PlantbusSubproblemCW>(sub_prob_[b]).get_plant_load();
					++b;
				}
			}
		}
		else {
			if (add_environments) {
				// adding environments for L1 problems means a multi-cut strategy, so no need for more entries in pb_info_
				for (auto pb_rec : plantbusses_) {
					environments_.emplace_back();
					environments_.back().set(GRB_IntParam_LogToConsole, 0);
					sub_prob_.emplace_back(std::in_place_type<PlantbusSubproblemL1>, environments_.back(), workspace_path, bus_data.at(pb_rec.first) * pb_rec.second, num_timesteps_);
					flex_pow += std::get<PlantbusSubproblemL1>(sub_prob_.back()).get_plant_load();
				}
			}
			else {
				for (auto pb_rec : plantbusses_) {
					sub_prob_.emplace_back(std::in_place_type<PlantbusSubproblemL1>, environments_[b], workspace_path, bus_data.at(pb_rec.first) * pb_rec.second, num_timesteps_);
					pb_info_.emplace_back();
					flex_pow += std::get<PlantbusSubproblemL1>(sub_prob_[b]).get_plant_load();
					++b;
				}
			}
		}
		
		return flex_pow;
	}

	void CoopBendersAlgorithm::SubStage(const int iteration) {
		if (options_.secondary_sp_type == 1) {
			std::vector<double> new_corepoint(num_timesteps_, 1.0);
			double load{};
			int b{};
			for (auto iter = secondary_begin_; iter != secondary_end_; ++iter) {
				new_corepoint = last_primary_feas_sol_[b];
				load = std::visit([&](auto&& subproblem) {return subproblem.get_plant_load(); }, *iter);
				for (double& val : new_corepoint) {
					val /= load;
				}
				std::transform(new_corepoint.begin(), new_corepoint.end(), new_corepoint.begin(),
					[this](double value) { return (1 - options_.mu) + options_.mu * value; });
				std::get<PlantbusSubproblemCW>(*iter).UpdateCorepoint(new_corepoint);
				++b;
			}
		}

		std::vector<std::pair<SubIterator, SubIterator>> iterators;
		iterators.emplace(iterators.cbegin(), cw_begin_, cw_end_);
		iterators.emplace(iterators.cbegin(), l1_begin_, l1_end_);
		iterators.emplace_back(secondary_begin_, secondary_end_);
        	SolveAndGenerateCutParallel(iteration, iterators);
		int b{};
		auto SaveFeasSol = [this, &b](SubVariant& sp) {
			last_primary_feas_sol_[b] = std::get<PlantbusSubproblemCW>(sp).ComputeFeasSol();
			++b;
		};
		std::for_each(cw_begin_, cw_end_, SaveFeasSol);
	}

	void CoopBendersAlgorithm::FeasPtStage(const int iteration) {
		cumul_feas_dist_ = 0;
		if (options_.primary_sp_type == 0 || options_.primary_sp_type == 2 || options_.primary_sp_type == 4) {
			// accumulate CW distance
			cumul_feas_dist_ = std::accumulate(cw_begin_, cw_end_, double{},
				[](double acc, SubVariant& elem) -> double {
				return acc + std::visit([&](auto&& subproblem) { return subproblem.ComputeFeasDistance(); }, elem); });
		}
		else { //accumulate L1 distance
			cumul_feas_dist_ = std::accumulate(l1_begin_, l1_end_, double{},
				[](double acc, SubVariant& elem) -> double {
				return acc + std::visit([&](auto&& subproblem) { return subproblem.ComputeFeasDistance(); }, elem); });
		}

		if (cumul_feas_dist_ < tau_) {
			if (options_.primary_sp_type == 4) {
				// Solve L1 problems to get L1 feas pts
				std::vector<std::pair<SubIterator, SubIterator>> iterators;
				iterators.emplace_back(feas_begin_, feas_end_);
				SolveParallel(iterators);
			}
			auto fx_p_plant_var_plantbus_iter = fx_p_plant_var_.begin(); //iterates over plantbusses of fixed problem
			auto SetFxPPlant = [&](SubVariant& sp) {
				const std::vector<double> feas_pt{ std::visit([&](auto&& subproblem) {return subproblem.ComputeFeasSol(); }, sp) };
				unsigned i{};
				auto SetBounds = [&feas_pt, &i](GRBVar& fx_var) {
					fx_var.set(GRB_DoubleAttr_LB, feas_pt[i]);
					fx_var.set(GRB_DoubleAttr_UB, feas_pt[i]);
					++i;
				};
				std::for_each((*fx_p_plant_var_plantbus_iter).begin(), (*fx_p_plant_var_plantbus_iter).end(), SetBounds);
				++fx_p_plant_var_plantbus_iter;
			};
			std::for_each(feas_begin_, feas_end_, SetFxPPlant);

			fixed_model_.optimize();
			// gaps in (absolute $ gap)/(cost of feas solution) (ub-lb)/ub = 1- lb/ub
			detectable_opt_gap_ = 1.0 - master_model_.get(GRB_DoubleAttr_ObjVal) / fixed_model_.get(GRB_DoubleAttr_ObjVal);
			std::setprecision(20);
			// numerial errors can occur when fixed and master model have equal objective values, so we introduce a small threshold, negative gaps with magnitude less than the threshold are tolerated
			if (detectable_opt_gap_ < -1e-15) { throw NegativeOptimalityGapException("iteration " + std::to_string(iteration) + ": detectable gap less than 0. Is " + std::to_string(detectable_opt_gap_)); }
			std::stringstream real_info;
			if (coop_sol_available_) {
				double real_feas_gap = 1.0 - coop_sol_cost_ / fixed_model_.get(GRB_DoubleAttr_ObjVal);
				if (real_feas_gap < -1e-15) { throw NegativeOptimalityGapException("real gap less than 0. Is " + std::to_string(real_feas_gap)); }
				if (real_feas_gap > detectable_opt_gap_ + 1e-15) { throw WrongOptimalityGapException("in iteration " + std::to_string(iteration) + ": real gap is bigger than detectable gap, should be smaller. real - detectable is " + std::to_string(real_feas_gap - detectable_opt_gap_)); }
				real_info << ", intsol-masterLB: " << coop_sol_cost_ - master_model_.get(GRB_DoubleAttr_ObjVal);
				real_info << ", real Gap: " << real_feas_gap;
			}
			//report progress
			out_file_ << "iteration " << iteration << ", culm. feasible Gap is " << cumul_feas_dist_ << " MW, which is below " << tau_ << " MW (current tau)" << std::endl;
			out_file_ << "detect. Gap: " << detectable_opt_gap_;
			out_file_ << real_info.str();
			out_file_ << ", ub computation returns status" << fixed_model_.get(GRB_IntAttr_Status) << "." << std::endl;
			
			if (detectable_opt_gap_ / options_.accept_gap > options_.rho) { //options_.rho safety margin: when in vicinity, do not decrease threshold
				const double reduction_factor = std::clamp(options_.accept_gap / detectable_opt_gap_, 0.5, 1.0); //maximal reduction by factor 2 (not including safety factor)
				tau_ *= options_.rho*reduction_factor;
			}

			if (detectable_opt_gap_ < options_.accept_gap) {
				out_file_ << "breaking the loop due to sufficently accurate feasible point in it" << iteration << "!" << std::endl;
				converged_ = true;
				final_iters_ = iteration;
				accepted_cost_ = fixed_model_.get(GRB_DoubleAttr_ObjVal);
			}
		}
	}

	void CoopBendersAlgorithm::UpdatePowSugg() {
		auto pb_info = pb_info_.begin();
		auto master_p_plant_var_plantbus_iter = master_p_plant_var_.begin(); //iterates over plantbusses of master problem
		auto UpdatePowSugg = [this, &pb_info, &master_p_plant_var_plantbus_iter](SubVariant& sp) {
			(*pb_info).set_no_update(true);
			unsigned i{};
			double new_val{};
			for (auto& master_p_plant_var : *master_p_plant_var_plantbus_iter) {
				new_val = master_p_plant_var.get(GRB_DoubleAttr_X) / std::visit([&](auto&& subproblem) { return subproblem.get_plant_load(); }, sp);
				// Power suggestion is not updated yet in subproblems, so this compares previous suggestion with new suggestion, which should be equal when irrelevant cut was added due to feasibility
				if (std::abs(std::visit([&](auto&& subproblem) { return subproblem.get_pow_sugg(i); }, sp) - new_val) > 1e-14) {
					(*pb_info).set_no_update(false);
				}
				std::visit([&](auto&& subproblem) {subproblem.set_pow_sugg(i, new_val); }, sp);
				++i;
			}
			++master_p_plant_var_plantbus_iter;
			if (master_p_plant_var_plantbus_iter == master_p_plant_var_.end()) { master_p_plant_var_plantbus_iter = master_p_plant_var_.begin(); }
			++pb_info;
			if (pb_info == pb_info_.end()) { pb_info = pb_info_.begin(); }
		};

		std::for_each(sub_prob_.begin(), sub_prob_.end(), UpdatePowSugg);
	}

	void CoopBendersAlgorithm::MasterStage(const int iteration) {
		master_model_.optimize();
		UpdatePowSugg();
		bool all_no_update = true;
		auto pb_rec = plantbusses_.begin();
		for (auto& pb_infos : pb_info_) {
			if (pb_infos.get_no_update()) { out_file_ << "detected no new master suggestion for bus " << (*pb_rec).first << " in iteration " << iteration << ". summed up plantobjectives are" << cumul_feas_dist_ << std::endl; }
			else { all_no_update = false; }
			++pb_rec;
			//multiple cuts require iterating over plantbusses twice:
			if (pb_rec == plantbusses_.end()) { pb_rec = plantbusses_.begin(); }

		}
		//Since FeasPtStage sets the value of converged_ which would break the loop if an accepted solution is found,
		//any call to MasterStage means the last solution was not acceptable and thus we expect a different power profile on at least one bus.
		//If no bus gets an updated power profile here, then throw an exception since something went wrong.
		if (all_no_update) {
			out_file_ << "detected no new master suggestion on any bus in iteration " << iteration << " despite not converging. summed up plantobjectives are " << cumul_feas_dist_ << ". aborting." << std::endl;
			throw std::runtime_error("detected no new master suggestion on any bus despite not converging");
		}
	}

	void CoopBendersAlgorithm::PrintSubproblemObjectivesAndDistances(const int iteration) {
		out_file_ << "it " << iteration << "done, ";
		auto pb_rec = plantbusses_.begin();
		auto PrintObjectiveAndDistance = [this, &pb_rec](SubVariant& sp) {
			double distance = std::visit([](auto&& subproblem) { return subproblem.ComputeFeasDistance(); }, sp);
			double objective = std::visit([](auto&& subproblem) { return subproblem.GetSubproblemObjective(); }, sp);
			if (std::holds_alternative<PlantbusSubproblemCW>(sp)) {
				out_file_ << "CWsubProb ";
			}
			else {
				out_file_ << "L1subProb ";
			}
			out_file_ << "on bus " << (*pb_rec).first << " has objective " << objective << ", and distance " << distance << ", ";
			++pb_rec;
		};
		std::for_each(cw_begin_, cw_end_, PrintObjectiveAndDistance);
		pb_rec = plantbusses_.begin();
		std::for_each(l1_begin_, l1_end_, PrintObjectiveAndDistance);
		out_file_ << "master obj (lb of Decomp) was: " << master_model_.get(GRB_DoubleAttr_ObjVal) << std::endl;
	}

	CoopBendersAlgorithm::CoopBendersAlgorithm(
		std::ofstream& out, const std::string& workspace_path, unsigned timesteps,
		const std::vector<std::pair<int, double>>& plantbusses, const std::unordered_map<int, double>& bus_data,
		const bool coop_sol_available, const double coop_sol_cost) :
		environments_(plantbusses.size()), options_{ workspace_path },
		fixed_model_{ environments_[0], workspace_path + options_.master_lp_file },
		master_model_{ environments_[0], workspace_path + options_.master_lp_file }, plantbusses_(plantbusses),
		fx_p_plant_var_{}, master_p_plant_var_{}, sub_prob_{}, pb_info_{}, last_primary_feas_sol_{},
		cw_begin_{}, cw_end_{}, l1_begin_{}, l1_end_{}, feas_begin_{}, feas_end_{}, secondary_begin_{}, secondary_end_{},
		out_file_{ out }, num_timesteps_{ timesteps }, final_iters_{}, tot_flex_power_{ SetUpSub(workspace_path, bus_data) },
		detectable_opt_gap_{ 1.0 }, accepted_cost_{}, cumul_feas_dist_{ 500000 }, coop_sol_cost_{ coop_sol_cost }, time_master_stage_{},
		time_feas_pt_stage_{}, time_sub_stage_{}, mas_t_{}, fpt_t_{}, sub_t_{}, converged_{ false }, coop_sol_available_{ coop_sol_available }
	{
		SetUpMasterAndFixed();
		out_file_ << "maxIters: " << options_.max_iters << ", primary/secondary strategy: " << options_.primary_sp_type << "/" << options_.secondary_sp_type << ", acceptable gap: " << options_.accept_gap << std::endl;
	}

	void CoopBendersAlgorithm::Iterate() {
		{// initial cut
			std::vector<std::thread> threads;
			std::for_each(cw_begin_, cw_end_, [this, &threads](SubVariant& sp) {
				threads.emplace_back(&PlantbusSubproblemCW::Solve, &std::get<PlantbusSubproblemCW>(sp));
			});
			std::for_each(l1_begin_, l1_end_, [this, &threads](SubVariant& sp) {
				threads.emplace_back(&PlantbusSubproblemL1::Solve, &std::get<PlantbusSubproblemL1>(sp));
			});
			for (auto& t : threads) {
				t.join();
			}
			std::vector<std::pair<SubIterator, SubIterator>> iterators;
			iterators.emplace_back(cw_begin_, cw_end_);
			iterators.emplace_back(l1_begin_, l1_end_);
			GenerateCutParallel(0, iterators);

			int b{};
			auto SaveFeasSol = [this, &b](SubVariant& sp) {
				last_primary_feas_sol_[b] = std::get<PlantbusSubproblemCW>(sp).ComputeFeasSol();
				++b;
			};
			std::for_each(cw_begin_, cw_end_, SaveFeasSol);
		}

		for (unsigned i = 1; i <= options_.max_iters; ++i) {
			mas_t_ = std::chrono::steady_clock::now();
			MasterStage(i);
			time_master_stage_ += std::chrono::steady_clock::now() - mas_t_;
			sub_t_ = std::chrono::steady_clock::now();
			SubStage(i);
			time_sub_stage_ += std::chrono::steady_clock::now() - sub_t_;
			//report some progress to outputfile
			out_file_ << std::defaultfloat << std::setprecision(5);
			if (i % 50 == 0) {
				PrintSubproblemObjectivesAndDistances(i);
			}
			out_file_ << std::scientific;
			fpt_t_ = std::chrono::steady_clock::now();
			FeasPtStage(i);
			time_feas_pt_stage_ += std::chrono::steady_clock::now() - fpt_t_;
			if (converged_) { break; }
		}
		if (!converged_) { final_iters_ = options_.max_iters; }
	}

	std::vector<std::vector<GRBVar>> CoopBendersAlgorithm::get_master_p_plant_var() {
		return master_p_plant_var_;
	}

	std::vector<std::pair<int, double>> CoopBendersAlgorithm::GetPlantbus() const {
		return plantbusses_;
	}

	unsigned CoopBendersAlgorithm::get_num_timesteps() const {
		return num_timesteps_;
	}

	GRBModel& CoopBendersAlgorithm::get_fixed_model() {
		return fixed_model_;
	}

	std::vector<CoopBendersAlgorithm::SubVariant>& CoopBendersAlgorithm::ProvideSubproblemVec() {
		return sub_prob_;
	}

	std::vector<PlantbusInfo> CoopBendersAlgorithm::get_pb_info() {
		return pb_info_;
	}

	std::vector<std::vector<double>> CoopBendersAlgorithm::GetFixedPPlant() const {
		std::vector<std::vector<double>> fx_p_plant_values(plantbusses_.size(), std::vector<double>(num_timesteps_, 0.0));
		if (fixed_model_.get(GRB_IntAttr_Status) == GRB_OPTIMAL) {
			auto fx_p_plant_values_plantbus_iter = fx_p_plant_values.begin();
			for (auto& fx_p_plant_variables_plantbus : fx_p_plant_var_) {
				auto fx_p_plant_value_iter = (*fx_p_plant_values_plantbus_iter).begin();
				for (auto& var : fx_p_plant_variables_plantbus) {
					*fx_p_plant_value_iter = var.get(GRB_DoubleAttr_X);
					++fx_p_plant_value_iter;
				}
				++fx_p_plant_values_plantbus_iter;
			}
		}
		else {
			std::cout << "The fixed model has not been solved to optimality. This is most likely due to no feasible candidate point having been found yet. GetFixedPPlant will return a vector of zeroes." << std::endl;
		}
		return fx_p_plant_values;
	}

	bool CoopBendersAlgorithm::get_converged() const {
		return converged_;
	}

	std::chrono::duration<double> CoopBendersAlgorithm::get_time_master_stage() const {
		return time_master_stage_;
	}

	std::chrono::duration<double> CoopBendersAlgorithm::get_time_sub_stage() const {
		return time_sub_stage_;
	}

	std::chrono::duration<double> CoopBendersAlgorithm::get_time_feas_pt_stage() const {
		return time_feas_pt_stage_;
	}

	double CoopBendersAlgorithm::get_tot_flex_power() const {
		return tot_flex_power_;
	}

	double CoopBendersAlgorithm::get_tau() const {
		return tau_;
	}

	int CoopBendersAlgorithm::get_final_iters() const {
		return final_iters_;
	}

	double CoopBendersAlgorithm::get_detectable_opt_gap() const {
		return detectable_opt_gap_;
	}

	double CoopBendersAlgorithm::get_accepted_cost() const {
		//final_iters_ is only 0 if we did not iterate, in that case, the objective variable may not be initialized in the master
		return (final_iters_ == 0 || converged_) ? accepted_cost_ : master_model_.get(GRB_DoubleAttr_ObjVal);
	}

	double CoopBendersAlgorithm::GetInitialTau() const {
		return options_.initial_tau_factor;
	}

	double CoopBendersAlgorithm::GetAcceptableGap() const {
		return options_.accept_gap;
	}

	void CoopBendersAlgorithm::PrintActiveCuts() {
		DynArrayRAIIWrapper<GRBConstr> constraints(master_model_.getConstrs(), static_cast<size_t>(master_model_.get(GRB_IntAttr_NumConstrs)));
		std::string name;
		double dual{};
		for (GRBConstr& c : constraints) {
			name = c.get(GRB_StringAttr_ConstrName);
			if (name.find("cut(") != std::string::npos) {
				dual = c.get(GRB_DoubleAttr_Pi);
				if (dual > 1e-8) {
					out_file_ << "Constraint " << name << " is active and its dual value is: " << dual << std::endl;
				}
			}
		}
	}

	PriceProblem::PriceProblem(const std::string& lp_file, const std::vector<std::pair<int, double>>& plantbusses, const unsigned timesteps, const double sbase) :
		loadflow_price_{ price_env_, lp_file }, price_vars_(loadflow_price_.getVars(),
			static_cast<size_t>(loadflow_price_.get(GRB_IntAttr_NumVars))), plantbusses_{ plantbusses },
		timesteps_{ timesteps }, sbase_{ sbase }
	{
		loadflow_price_.set(GRB_IntParam_LogToConsole, 0);
	}

	void PriceProblem::ComputePrices(const std::unordered_map<int, double>& bus_data, const std::unordered_map<std::string, double>& wd, GRBModel& loadflow_in) {
		//subtract shedded load form balance
		for (auto it = bus_data.begin(); it != bus_data.end(); ++it) {
			int bus = (*it).first;
			for (unsigned t = 1; t <= timesteps_; ++t) {
				GRBConstr const2mod = loadflow_price_.getConstrByName("const2mod(" + std::to_string(bus) + ",t" + std::to_string(t) + ")");
				double lsh_var_value = loadflow_in.getVarByName("lsh(" + std::to_string(bus) + ",t" + std::to_string(t) + ")").get(GRB_DoubleAttr_X);
				double pb_fraction = 0;
				auto plantbus = std::find_if(plantbusses_.begin(), plantbusses_.end(), [bus](const std::pair<int, double>& x) {return x.first == bus; });
				if (plantbus != plantbusses_.end()) {
					pb_fraction = (*plantbus).second;
				}
				double new_rhs = wd.find("t" + std::to_string(t))->second * bus_data.find(bus)->second * (1.0 - pb_fraction) / sbase_ - lsh_var_value;
				const2mod.set(GRB_DoubleAttr_RHS, new_rhs);
			}
		}

		//fix power profile
		for (auto& pb_rec : plantbusses_) {
			for (unsigned t = 1; t <= timesteps_; ++t) {
				std::string pplant_name = "Pplant(" + std::to_string(pb_rec.first) + ",t" + std::to_string(t) + ")";
				loadflow_price_.getVarByName(pplant_name).set(GRB_DoubleAttr_LB, loadflow_in.getVarByName(pplant_name).get(GRB_DoubleAttr_X));
				loadflow_price_.getVarByName(pplant_name).set(GRB_DoubleAttr_UB, loadflow_in.getVarByName(pplant_name).get(GRB_DoubleAttr_X));
			}
		}

		loadflow_price_.optimize();
		if (loadflow_price_.get(GRB_IntAttr_Status) != GRB_OPTIMAL)
		{
			throw std::runtime_error("Price model not solved to optimality");
		}
	}

	std::vector<std::vector<double>> PriceProblem::GetPrices() {
		std::vector<std::vector<double>> prices;
		int b = 0; //variable to iterate through dynamic containers with #plantbus entries
		for (auto& pb_rec : plantbusses_) {
			prices.emplace_back(timesteps_);
			for (unsigned t = 1; t <= timesteps_; ++t) {
				prices[b][t - 1] =
					loadflow_price_.getConstrByName(
						"const2mod(" + std::to_string(pb_rec.first) + ",t" + std::to_string(t) + ")"
					).get(GRB_DoubleAttr_Pi) / sbase_;
			}
			++b;
		}
		return prices;
	}

	double PriceProblem::ComputeAndPrintCongestionCost(std::ofstream& out_file, std::string&& setup) {
		double cong_cost{};
		out_file << "Congested lines in " << setup << " operation are: " << std::endl;
		for (auto& var : price_vars_) {
			std::string name = var.get(GRB_StringAttr_VarName);
			if (name.find("Pij") != std::string::npos) {
				size_t open_bracket, first_comma, second_comma, closed_bracket;
				open_bracket = name.find('(');
				first_comma = name.find(',');
				second_comma = name.find(',', first_comma + 1);
				closed_bracket = name.find(')');
				std::string bus = name.substr(open_bracket + 1, first_comma - open_bracket - 1);
				std::string node = name.substr(first_comma + 1, second_comma - first_comma - 1);
				std::string time = name.substr(second_comma + 1, closed_bracket - second_comma - 1);
				GRBConstr const2mod_bus = loadflow_price_.getConstrByName("const2mod(" + bus + "," + time + ")");
				GRBConstr const2mod_node = loadflow_price_.getConstrByName("const2mod(" + node + "," + time + ")");
				cong_cost += var.get(GRB_DoubleAttr_X) * 0.5 * (-const2mod_bus.get(GRB_DoubleAttr_Pi) + const2mod_node.get(GRB_DoubleAttr_Pi));

				//write congested lines to out_file
				if (std::abs(var.get(GRB_DoubleAttr_RC)) > 1E-4) {
					std::string fromBus{ name.substr(open_bracket + 1) };
					fromBus.erase(fromBus.find(','));
					std::string toBus{ name.substr(first_comma + 1) };
					std::string timeSlot{ toBus.substr(toBus.find(',') + 1) };
					toBus.erase(toBus.find(','));
					timeSlot.erase(timeSlot.find(')'));
					if (var.get(GRB_DoubleAttr_X) > 0) {
						out_file << fromBus << " to " << toBus << " at " << timeSlot << "." << std::endl;
					}
					else {
						out_file << toBus << " to " << fromBus << " at " << timeSlot << "." << std::endl;
					}
				}
			}
		}
		out_file << '\n' << '\n';
		return cong_cost;
	}
} //namespace decoop
