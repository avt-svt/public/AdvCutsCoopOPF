/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

/**
* \file coopBendersSolveNCutgen.cpp
* \brief Implementation of solve and cut generation methods for %CoopBendersAlgorithm.
*/

#include "coopBendersAlgorithm.hpp"
#include <thread>

namespace decoop {

	void CoopBendersAlgorithm::SolveParallel(const std::vector<std::pair<SubIterator, SubIterator>>& iterators) {
		std::vector<std::thread> threads;
		int b{};
		auto CreateThread = [this, &threads, &b](SubVariant& sp) {
			if (!pb_info_[b].get_no_update()) {
				if (std::holds_alternative<PlantbusSubproblemCW>(sp)) {
					threads.emplace_back(&PlantbusSubproblemCW::Solve, &std::get<PlantbusSubproblemCW>(sp));
				}
				else {
					threads.emplace_back(&PlantbusSubproblemL1::Solve, &std::get<PlantbusSubproblemL1>(sp));
				}
			}
			++b;
		};

		for (auto& iter_pair : iterators) {
			std::for_each(iter_pair.first, iter_pair.second, CreateThread);
			if (b >= pb_info_.size()) { b = 0; }
		}

		for (std::thread& t : threads) {
			t.join();
		}
	}

	void CoopBendersAlgorithm::SolveSerial(const std::vector<std::pair<SubIterator, SubIterator>>& iterators) {
		int b{};
		auto Solve = [this, &b](SubVariant& sp) {
			if (!pb_info_[b].get_no_update()) {
				std::visit([&](auto&& subproblem) { subproblem.Solve(); }, sp);
			}
			++b;
			if (b >= plantbusses_.size()) { b = 0; }
		};
		for (auto& it_pairs : iterators) {
			std::for_each(it_pairs.first, it_pairs.second, Solve);
		}
	}

	void CoopBendersAlgorithm::GenerateCut(const int iteration, const std::vector<std::pair<SubIterator, SubIterator>>& iterators) {
		auto pb_rec = plantbusses_.begin();
		auto master_p_plant_var_plantbus_iter = master_p_plant_var_.begin();
		int cut_nr{};
		double rhs{};
		for (auto& it_pairs : iterators) {
			for (auto p = it_pairs.first; p != it_pairs.second; ++p) {
				if (std::visit([&](auto&& subproblem) { return subproblem.GetModelStatus(); }, *p) != GRB_OPTIMAL) {
					out_file_ << "aborting because sub status is " << std::visit([&](auto&& arg) { return arg.GetModelStatus(); }, *p) << " in iteration " << iteration << " on bus " << (*pb_rec).first << std::endl;
					out_file_ << "pplant is" << std::endl;
					for (auto& var : *master_p_plant_var_plantbus_iter) {
						out_file_ << var.get(GRB_DoubleAttr_X) << std::endl;
					}
					out_file_ << std::endl;
					throw std::runtime_error("feasible subproblem reported not optimal!");
				}
				else if (std::visit([&](auto&& subproblem) { return subproblem.GetSubproblemObjective(); }, *p) <= options_.eps) {
					out_file_ << "problem on bus " << (*pb_rec).first << " converged in it" << iteration << std::endl;
				}
				//prepare cuts for next iteration
				//the subproblem always adds the last cut of the iteration, so the cut-index will be the total number of cuts added per iteration
				try {
					rhs = std::visit([&](auto&& subproblem) { return subproblem.GenerateAndVerifyCut(); }, *p); //If cut verification fails, this throws an error and the cut is not added
					//If no exception is thrown, cut has been verified and we want to add it.
					//If we got an exception, we might want to continue iteration anyway (if the SP is feasible)
					//so we can't move this behind the catch block since then we would be adding irrelevant cuts
					std::vector<double> coeffs = std::visit([&](auto&& subproblem) { return subproblem.GetNewCoeffs(); }, *p);
					GRBLinExpr lhs;
					lhs.addTerms(&coeffs[0], &(*master_p_plant_var_plantbus_iter)[0], num_timesteps_);
					master_model_.addConstr(lhs >= rhs, "cut(" + std::to_string((*pb_rec).first) + ",it" + std::to_string(iteration) + "," + std::to_string(cut_nr) + ")");
				}
				catch (BadCutException& e) {
					//If we generated a bad (wrong) cut, rethrow -> terminate
					throw;
				}
				catch (IrrelevantCutException& e) {
					if (std::visit([&](auto&& subproblem) { return subproblem.GetSubproblemObjective(); }, *p) <= options_.eps) {
						//If we have an irrelevant cut but are feasible, continue iteration and notify
						out_file_ << "encountered acceptable irrelevant cut on bus " << (*pb_rec).first << " in iteration " << iteration << std::endl;
					}
					else {
						//If we have an irrelevant cut but are infeasible, terminate
						out_file_ << "aborting, unacceptable irrelevant cut when subproblem objective is: " << std::visit([&](auto&& arg) { return arg.GetSubproblemObjective(); }, *p) << std::endl << "Pplant is: " << std::endl;
						for (auto& var : *master_p_plant_var_plantbus_iter) {
							out_file_ << var.get(GRB_DoubleAttr_X) << std::endl;
						}
						out_file_ << std::endl;
						throw std::runtime_error("irrelevant cut generated by subproblem problem on bus " + std::to_string((*pb_rec).first) + " in iteration " + std::to_string(iteration));
					}
				}
				++pb_rec;
				if (pb_rec == plantbusses_.end()) { pb_rec = plantbusses_.begin(); }
				++master_p_plant_var_plantbus_iter;
				if (master_p_plant_var_plantbus_iter == master_p_plant_var_.end()) { master_p_plant_var_plantbus_iter = master_p_plant_var_.begin(); }
			}
			++cut_nr;
		}
	}

	void CoopBendersAlgorithm::GenerateCutParallel(const int iteration, const std::vector<std::pair<SubIterator, SubIterator>>& iterators) {
		//parallel: generate the cutcoeffs and rhs
		std::vector<GRBLinExpr> lhs(sub_prob_.size());
		std::vector<double> rhs(sub_prob_.size());
		std::vector<std::thread> threads;
		std::vector<bool> valid_cut(sub_prob_.size(), false);
		auto master_p_plant_var_plantbus_iter = master_p_plant_var_.begin();
		auto pb_rec = plantbusses_.begin();

		int index{};

		auto BuildLhs = [this, &lhs](SubVariant& sp, std::vector<GRBVar>& vars, int index) {
			std::vector<double> coeffs = std::visit([&](auto&& subproblem) { return subproblem.GetNewCoeffs(); }, sp);
			lhs[index].addTerms(&coeffs[0], &vars[0], num_timesteps_);
		};
		auto GetRhs = [this, &rhs, &valid_cut, iteration](SubVariant& sp, std::vector<GRBVar>& vars, int pb, int index) {
			try {
				rhs[index] = std::visit([&](auto&& subproblem) { return subproblem.GenerateAndVerifyCut(); }, sp);
				valid_cut[index] = true;
			}
			catch (BadCutException& e) {
				throw;
			}
			catch (IrrelevantCutException& e) {
				if (std::visit([&](auto&& subproblem) { return subproblem.GetSubproblemObjective(); }, sp) <= options_.eps) {
					//If we have an irrelevant cut but are feasible, continue iteration and notify
					out_file_ << "encountered acceptable irrelevant cut on bus " << pb << " in iteration " << iteration << std::endl;
				}
				else {
					//If we have an irrelevant cut but are infeasible, terminate
					out_file_ << "aborting, unacceptable irrelevant cut when subproblem objective is: " << std::visit([&](auto&& arg) { return arg.GetSubproblemObjective(); }, sp) << std::endl << "Pplant is: " << std::endl;
					for (auto& var : vars) {
						out_file_ << var.get(GRB_DoubleAttr_X) << std::endl;
					}
					out_file_ << std::endl;
					throw std::runtime_error("irrelevant cut generated by subproblem problem on bus " + std::to_string(pb) + " in iteration " + std::to_string(iteration));
				}
			}

		};
		//1 Thread for LHS, 1 Thread for RHS
		//Parallel overhead may not be worth it here
		auto CreateThreads = [this, &threads, &index, &BuildLhs, &GetRhs, &master_p_plant_var_plantbus_iter, &pb_rec](SubVariant& sp) {
			threads.emplace_back(BuildLhs, std::ref(sp), std::ref(*master_p_plant_var_plantbus_iter), index);
			threads.emplace_back(GetRhs, std::ref(sp), std::ref(*master_p_plant_var_plantbus_iter), (*pb_rec).first, index);
			++index;
			++master_p_plant_var_plantbus_iter;
			if (master_p_plant_var_plantbus_iter == master_p_plant_var_.end()) { master_p_plant_var_plantbus_iter = master_p_plant_var_.begin(); }
			++pb_rec;
			if (pb_rec == plantbusses_.end()) { pb_rec = plantbusses_.begin(); }

		};

		auto BuildCut = [this, &lhs, &rhs, &valid_cut, iteration](SubVariant& sp, std::vector<GRBVar>& vars, int pb, int index) {
			try {
				rhs[index] = std::visit([&](auto&& subproblem) { return subproblem.GenerateAndVerifyCut(); }, sp);
				std::vector<double> coeffs = std::visit([&](auto&& subproblem) { return subproblem.GetNewCoeffs(); }, sp);
				lhs[index].addTerms(&coeffs[0], &vars[0], num_timesteps_);
				valid_cut[index] = true;
			}
			catch (BadCutException& e) {
				throw;
			}
			catch (IrrelevantCutException& e) {
				if (std::visit([&](auto&& subproblem) { return subproblem.GetSubproblemObjective(); }, sp) <= options_.eps) {
					//If we have an irrelevant cut but are feasible, continue iteration and notify
					out_file_ << "encountered acceptable irrelevant cut on bus " << pb << " in iteration " << iteration << std::endl;
				}
				else {
					//If we have an irrelevant cut but are infeasible, terminate
					out_file_ << "aborting, unacceptable irrelevant cut when subproblem objective is: " << std::visit([&](auto&& arg) { return arg.GetSubproblemObjective(); }, sp) << std::endl << "Pplant is: " << std::endl;
					for (auto& var : vars) {
						out_file_ << var.get(GRB_DoubleAttr_X) << std::endl;
					}
					out_file_ << std::endl;
					throw std::runtime_error("irrelevant cut generated by subproblem problem on bus " + std::to_string(pb) + " in iteration " + std::to_string(iteration));
				}
			}
		};
		//1 Thread that does LHS and RHS sequentially
		auto CreateThread = [this, &threads, &index, &BuildCut, &master_p_plant_var_plantbus_iter, &pb_rec](SubVariant& sp) {
			threads.emplace_back(BuildCut, std::ref(sp), std::ref(*master_p_plant_var_plantbus_iter), (*pb_rec).first, index);
			++index;
			++master_p_plant_var_plantbus_iter;
			if (master_p_plant_var_plantbus_iter == master_p_plant_var_.end()) { master_p_plant_var_plantbus_iter = master_p_plant_var_.begin(); }
			++pb_rec;
			if (pb_rec == plantbusses_.end()) { pb_rec = plantbusses_.begin(); }
		};

		for (auto& it_pair : iterators) {
			std::for_each(it_pair.first, it_pair.second, CreateThread);
		}

		for (std::thread& t : threads) {
			t.join();
		}

		//sequential: add constraints, Gurobi does not allow parallel calls to this
		if (lhs.size() != rhs.size()) {
			throw std::runtime_error("Number of cut LHS unequal to number of cut RHS!");
		}
		pb_rec = plantbusses_.begin();
		int cut_nr{};
		for (unsigned i = 0; i < lhs.size(); ++i) {
			if (valid_cut[i]) {
				master_model_.addConstr(lhs[i] >= rhs[i], "cut(" + std::to_string((*pb_rec).first) + ",it " + std::to_string(iteration) + "," + std::to_string(cut_nr) + ")");
			}
			++pb_rec;
			if (pb_rec == plantbusses_.end()) { pb_rec = plantbusses_.begin(); ++cut_nr; }
		}
	}

	void CoopBendersAlgorithm::SolveAndGenerateCutParallel(const int iteration, const std::vector<std::pair<SubIterator, SubIterator>>& iterators) {
		std::vector<GRBLinExpr> lhs(sub_prob_.size());
		std::vector<double> rhs(sub_prob_.size());
		std::vector<std::thread> threads;
		std::vector<bool> valid_cut(sub_prob_.size(), false);
		auto master_p_plant_var_plantbus_iter = master_p_plant_var_.begin();
		auto pb_rec = plantbusses_.begin();
		int index{};
		int b{};

		auto SolveAndGenerateCut = [this, &rhs, &lhs, &valid_cut, iteration, b](SubVariant& sp, std::vector<GRBVar>& vars, int pb, int index) {
			//Solve
			if (!pb_info_[b].get_no_update()) {
				std::visit([](auto&& subproblem) { subproblem.Solve(); }, sp);
			}
			//Build cut
			try {
				rhs[index] = std::visit([&](auto&& subproblem) { return subproblem.GenerateAndVerifyCut(); }, sp);
				std::vector<double> coeffs = std::visit([&](auto&& subproblem) { return subproblem.GetNewCoeffs(); }, sp);
				lhs[index].addTerms(&coeffs[0], &vars[0], num_timesteps_);
				valid_cut[index] = true;
			}
			catch (BadCutException& e) {
				throw;
			}
			catch (IrrelevantCutException& e) {
				if (std::visit([&](auto&& subproblem) { return subproblem.GetSubproblemObjective(); }, sp) <= options_.eps) {
					//If we have an irrelevant cut but are feasible, continue iteration and notify
					out_file_ << "encountered acceptable irrelevant cut on bus " << pb << " in iteration " << iteration << std::endl;
				}
				else {
					//If we have an irrelevant cut but are infeasible, terminate
					out_file_ << "aborting, unacceptable irrelevant cut when subproblem objective is: " << std::visit([&](auto&& arg) { return arg.GetSubproblemObjective(); }, sp) << std::endl << "Pplant is: " << std::endl;
					for (auto& var : vars) {
						out_file_ << var.get(GRB_DoubleAttr_X) << std::endl;
					}
					out_file_ << std::endl;
					throw std::runtime_error("irrelevant cut generated by subproblem problem on bus " + std::to_string(pb) + " in iteration " + std::to_string(iteration));
				}
			}
		};

		auto CreateThread = [this, &threads, &SolveAndGenerateCut, &master_p_plant_var_plantbus_iter, &pb_rec, &index](SubVariant& sp) {
			threads.emplace_back(SolveAndGenerateCut, std::ref(sp), std::ref(*master_p_plant_var_plantbus_iter), (*pb_rec).first, index);
			++index;
			++master_p_plant_var_plantbus_iter;
			if (master_p_plant_var_plantbus_iter == master_p_plant_var_.end()) { master_p_plant_var_plantbus_iter = master_p_plant_var_.begin(); }
			++pb_rec;
			if (pb_rec == plantbusses_.end()) { pb_rec = plantbusses_.begin(); }
		};

		for (const auto& iter_pair : iterators) {
			std::for_each(iter_pair.first, iter_pair.second, CreateThread);
			b = 0;
		}

		for (std::thread& t : threads) {
			t.join();
		}

		//sequential: add constraints, Gurobi does not allow parallel calls to this
		if (lhs.size() != rhs.size()) {
			throw std::runtime_error("Number of cut LHS unequal to number of cut RHS!");
		}
		pb_rec = plantbusses_.begin();
		int cut_nr{};
		for (unsigned i = 0; i < lhs.size(); ++i) {
			if (valid_cut[i]) {
				master_model_.addConstr(lhs[i] >= rhs[i], "cut(" + std::to_string((*pb_rec).first) + ",it " + std::to_string(iteration) + "," + std::to_string(cut_nr) + ")");
			}
			++pb_rec;
			if (pb_rec == plantbusses_.end()) { pb_rec = plantbusses_.begin(); ++cut_nr; }
		}
	}

}
