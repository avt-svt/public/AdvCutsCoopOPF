/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

#include "gtest/gtest.h"
#include "coopBendersHeader.hpp"
#include "gurobi_c++.h"
#include <unordered_map>
#include <filesystem>

using namespace decoop;

void UpdatePathInSubOptionFiles(const std::string& test_dir) {
	std::ifstream file;
	std::ofstream out_file;
	std::vector<std::string> strings;
	file.open(test_dir + "Workspace/options_subproblem.txt");
	while (!file.eof()) {
		strings.emplace_back();
		std::getline(file, strings.back());
	}
	file.close();
	out_file.open(test_dir + "Workspace/options_subproblem.txt");
	for (auto& s : strings) {
		if (s.find("LP_FILE_PATH") == std::string::npos) {
			out_file << s << std::endl;
		}
		else {
			out_file << "LP_FILE_PATH " << test_dir << "lp_files/" << std::endl;
		}
	}
	out_file.close();
	out_file.open(test_dir + "regular/options_subproblem.txt");
	for (auto& s : strings) {
		if (s.find("LP_FILE_PATH") == std::string::npos) {
			out_file << s << std::endl;
		}
		else {
			out_file << "LP_FILE_PATH " << test_dir << "lp_files/" << std::endl;
		}
	}
	out_file.close();
	out_file.open(test_dir + "high_tau/options_subproblem.txt");
	for (auto& s : strings) {
		if (s.find("LP_FILE_PATH") == std::string::npos) {
			out_file << s << std::endl;
		}
		else {
			out_file << "LP_FILE_PATH " << test_dir << "lp_files/" << std::endl;
		}
	}
	out_file.close();
	out_file.open(test_dir + "no_conv/options_subproblem.txt");
	for (auto& s : strings) {
		if (s.find("LP_FILE_PATH") == std::string::npos) {
			out_file << s << std::endl;
		}
		else {
			out_file << "LP_FILE_PATH " << test_dir << "lp_files/" << std::endl;
		}
	}
	out_file.close();
}

class InputTest : public ::testing::Test {
public:
	static std::string test_dir;

protected:
	std::fstream in;

	MainOptions ReadInMain(const std::string& filename) {
		return MainOptions{ filename, true };
	}

	AlgorithmOptions ReadInAlgorithm(const std::string& filename) {
		return AlgorithmOptions{ filename, true };
	}

	SubproblemOptions ReadInSubproblem(const std::string& filename) {
		return SubproblemOptions{ filename, true };
	}

};
std::string InputTest::test_dir = "";

class BendersTest : public ::testing::Test {
public:
	static std::string test_dir;
protected:
	std::string work_dir_;
	MainOptions options_;
	std::ofstream out_file_;
	std::unordered_map<int, double> bus_data_;
	CoopBendersAlgorithm decomposition_only_cw_;
	CoopBendersAlgorithm decomposition_only_cw_no_iter_;
	CoopBendersAlgorithm decomposition_only_cw_no_conv_;
	CoopBendersAlgorithm decomposition_only_l1_;
	CoopBendersAlgorithm decomposition_both_cw_;
	CoopBendersAlgorithm decomposition_both_l1_;
	CoopBendersAlgorithm decomposition_tailored_strategy_;
	CoopBendersAlgorithm decomposition_tailored_strategy_multicut_;

	BendersTest() : work_dir_{ test_dir + "Workspace/" }, options_ { work_dir_ }, out_file_{ work_dir_ + options_.output_name },
		bus_data_{ {2, 100} }, decomposition_only_cw_{out_file_, test_dir + "regular/only_cw/", options_.timesteps, options_.plantbusses, bus_data_, true, 2070},
		decomposition_only_cw_no_iter_{ out_file_, test_dir + "regular/only_cw/", options_.timesteps, options_.plantbusses, bus_data_, true, 2070 },
		decomposition_only_cw_no_conv_{ out_file_, test_dir + "no_conv/", options_.timesteps, options_.plantbusses, bus_data_, true, 2070 },
		decomposition_only_l1_{ out_file_, test_dir + "regular/only_l1/", options_.timesteps, options_.plantbusses, bus_data_, true, 2070 },
		decomposition_both_cw_{ out_file_, test_dir + "regular/both_cw/", options_.timesteps, options_.plantbusses, bus_data_, true, 2070 },
		decomposition_both_l1_{ out_file_, test_dir + "regular/both_l1/", options_.timesteps, options_.plantbusses, bus_data_, true, 2070 },
		decomposition_tailored_strategy_{ out_file_, test_dir + "regular/tailored_strategy/", options_.timesteps, options_.plantbusses, bus_data_, true, 2070 },
		decomposition_tailored_strategy_multicut_{ out_file_, test_dir + "regular/tailored_strategy_multicut/", options_.timesteps, options_.plantbusses, bus_data_, true, 2070 }
	{
		decomposition_only_cw_.Iterate();
		decomposition_only_cw_no_conv_.Iterate();
		decomposition_only_l1_.Iterate();
		decomposition_both_cw_.Iterate();
		decomposition_both_l1_.Iterate();
		decomposition_tailored_strategy_.Iterate();
		decomposition_tailored_strategy_multicut_.Iterate();
	}
};

std::string BendersTest::test_dir = "";

class SubproblemTest : public ::testing::Test {
protected:

	static GRBEnv env_;
	// 3 entries for each subproblem type to test following states: before solving, after solving with infeasible suggestion, after solving with feasible suggestion
	static std::vector<PlantbusSubproblemCW> sub_cw_;
	static std::vector<PlantbusSubproblemL1> sub_l1_;
public:
	static std::string test_dir;
protected:
	static void SetUpTestSuite() {

		std::string work_dir{ test_dir + "Workspace/" };
		MainOptions options_main{work_dir};
		SubproblemOptions options_subproblem{work_dir};

		env_.set(GRB_IntParam_LogToConsole, 0);

		sub_cw_.reserve(3);
		sub_l1_.reserve(3);
		for (int i = 0; i < 3; ++i) {
			sub_cw_.emplace_back(env_, work_dir, 100, 2); //load and timesteps hardcoded here
			sub_l1_.emplace_back(env_, work_dir, 100, 2); //load and timesteps hardcoded here
		}

		std::vector<double> infeasible_suggestion{ 0.2, 0.2 };
		sub_cw_[1].set_pow_sugg(infeasible_suggestion);
		sub_l1_[1].set_pow_sugg(infeasible_suggestion);
		sub_l1_[1].Solve();
		sub_cw_[1].Solve();

		std::vector<double> feasible_suggestion { 1.05, 0.95 };
		sub_cw_[2].set_pow_sugg(feasible_suggestion);
		sub_l1_[2].set_pow_sugg(feasible_suggestion);
		sub_l1_[2].Solve();
		sub_cw_[2].Solve();
	}

};


GRBEnv SubproblemTest::env_;
std::vector<PlantbusSubproblemCW> SubproblemTest::sub_cw_;
std::vector<PlantbusSubproblemL1> SubproblemTest::sub_l1_;
std::string SubproblemTest::test_dir = "";

TEST_F(InputTest, ReadMainOptions) {
	MainOptions main_options = ReadInMain(test_dir + "input_test/options_main.txt");
	EXPECT_EQ(main_options.plantbusses.at(0).first, 2);
	EXPECT_DOUBLE_EQ(main_options.plantbusses.at(0).second, 1);
	EXPECT_ANY_THROW(main_options.plantbusses.at(1));
	EXPECT_EQ(main_options.timesteps, 2);
	EXPECT_EQ(main_options.output_name, "output");
	EXPECT_EQ(main_options.csv_name, "resmanual");
	EXPECT_EQ(main_options.spec_name, "");
	EXPECT_EQ(main_options.spec_func, 0);
	EXPECT_EQ(main_options.nominal_lp, "lpnominal");
	EXPECT_EQ(main_options.price_lp, "lpprice");
	EXPECT_EQ(main_options.integrated_lp, "lpint");
	EXPECT_EQ(main_options.sub_dr_lp, "lpsubdr");
	EXPECT_EQ(main_options.cong_indicator, 'n');
	EXPECT_EQ(main_options.use_int_prob, 0);
}

TEST_F(InputTest, ReadMainOptionsWithOptionals) {
	MainOptions main_options = ReadInMain(test_dir + "input_test/options_main_optionals.txt");
	EXPECT_EQ(main_options.plantbusses.at(0).first, 2);
	EXPECT_DOUBLE_EQ(main_options.plantbusses.at(0).second, 1);
	EXPECT_ANY_THROW(main_options.plantbusses.at(1));
	EXPECT_EQ(main_options.timesteps, 2);
	EXPECT_EQ(main_options.output_name, "output");
	EXPECT_EQ(main_options.csv_name, "resmanual");
	EXPECT_EQ(main_options.spec_name, "casestudycsv");
	EXPECT_EQ(main_options.spec_func, 1);
	EXPECT_EQ(main_options.nominal_lp, "lpnominal");
	EXPECT_EQ(main_options.price_lp, "lpprice");
	EXPECT_EQ(main_options.integrated_lp, "lpint");
	EXPECT_EQ(main_options.sub_dr_lp, "lpsubdr");
	EXPECT_EQ(main_options.cong_indicator, 'n');
	EXPECT_EQ(main_options.use_int_prob, 1);
	EXPECT_EQ(main_options.univ_str, "univstring");
}

TEST_F(InputTest, ReadMainOptionsMultiplePlantbus) {
	MainOptions main_options = ReadInMain(test_dir + "input_test/options_main_multiple.txt");
	EXPECT_EQ(main_options.plantbusses.at(0).first, 2);
	EXPECT_DOUBLE_EQ(main_options.plantbusses.at(0).second, 1);
	EXPECT_EQ(main_options.plantbusses.at(1).first, 3);
	EXPECT_DOUBLE_EQ(main_options.plantbusses.at(1).second, 0.5);
	EXPECT_ANY_THROW(main_options.plantbusses.at(2));
	EXPECT_EQ(main_options.timesteps, 2);
	EXPECT_EQ(main_options.output_name, "output");
	EXPECT_EQ(main_options.csv_name, "resmanual");
	EXPECT_EQ(main_options.spec_name, "");
	EXPECT_EQ(main_options.spec_func, 0);
	EXPECT_EQ(main_options.nominal_lp, "lpnominal");
	EXPECT_EQ(main_options.price_lp, "lpprice");
	EXPECT_EQ(main_options.integrated_lp, "lpint");
}

TEST_F(InputTest, ReadAlgorithmOptions) {
	AlgorithmOptions algorithm_options = ReadInAlgorithm(test_dir + "input_test/options_algorithm.txt");
	EXPECT_EQ(algorithm_options.master_lp_file, "masterlp");
	EXPECT_EQ(algorithm_options.max_iters, 5);
	EXPECT_EQ(algorithm_options.primary_sp_type, 2);
	EXPECT_EQ(algorithm_options.secondary_sp_type, 1);
	EXPECT_DOUBLE_EQ(algorithm_options.initial_tau_factor, 1);
	EXPECT_DOUBLE_EQ(algorithm_options.accept_gap, 0.1);
	EXPECT_DOUBLE_EQ(algorithm_options.rho, 1.19);
	EXPECT_DOUBLE_EQ(algorithm_options.eps, 1e-7);
	EXPECT_DOUBLE_EQ(algorithm_options.mu, 0.97);
}

TEST_F(InputTest, ReadDefaultAlgorithmOptions) {
	AlgorithmOptions default_algorithm_options = ReadInAlgorithm(test_dir + "input_test/options_algorithm_default.txt");
	EXPECT_EQ(default_algorithm_options.master_lp_file, "masterlp");
	EXPECT_EQ(default_algorithm_options.max_iters, 500);
	EXPECT_EQ(default_algorithm_options.primary_sp_type, 4);
	EXPECT_EQ(default_algorithm_options.secondary_sp_type, 0);
	EXPECT_DOUBLE_EQ(default_algorithm_options.initial_tau_factor, 1);
	EXPECT_DOUBLE_EQ(default_algorithm_options.accept_gap, 1e-4);
	EXPECT_DOUBLE_EQ(default_algorithm_options.rho, 1.19);
	EXPECT_DOUBLE_EQ(default_algorithm_options.eps, 1e-7);
	EXPECT_DOUBLE_EQ(default_algorithm_options.mu, 0.97);

}

TEST_F(InputTest, ReadSubproblemOptions) {
	SubproblemOptions subproblem_options = ReadInSubproblem(test_dir + "input_test/options_subproblem.txt");
	EXPECT_EQ(subproblem_options.lp_file_l1, "sublpl1");
	EXPECT_EQ(subproblem_options.lp_file_cw, "sublpcw");
	EXPECT_EQ(subproblem_options.workspace_dir, "workspace");
}

TEST_F(BendersTest, Times) {
	EXPECT_GE(decomposition_only_cw_.get_time_sub_stage().count(), 0);
	EXPECT_GE(decomposition_only_cw_.get_time_master_stage().count(), 0);
	EXPECT_GE(decomposition_only_cw_.get_time_feas_pt_stage().count(), 0);
	EXPECT_GE(decomposition_only_cw_no_conv_.get_time_sub_stage().count(), 0);
	EXPECT_GE(decomposition_only_cw_no_conv_.get_time_master_stage().count(), 0);
	EXPECT_GE(decomposition_only_cw_no_conv_.get_time_feas_pt_stage().count(), 0);
	EXPECT_GE(decomposition_only_l1_.get_time_sub_stage().count(), 0);
	EXPECT_GE(decomposition_only_l1_.get_time_master_stage().count(), 0);
	EXPECT_GE(decomposition_only_l1_.get_time_feas_pt_stage().count(), 0);
	EXPECT_GE(decomposition_both_cw_.get_time_sub_stage().count(), 0);
	EXPECT_GE(decomposition_both_cw_.get_time_master_stage().count(), 0);
	EXPECT_GE(decomposition_both_cw_.get_time_feas_pt_stage().count(), 0);
	EXPECT_GE(decomposition_both_l1_.get_time_sub_stage().count(), 0);
	EXPECT_GE(decomposition_both_l1_.get_time_master_stage().count(), 0);
	EXPECT_GE(decomposition_both_l1_.get_time_feas_pt_stage().count(), 0);
	EXPECT_GE(decomposition_tailored_strategy_.get_time_sub_stage().count(), 0);
	EXPECT_GE(decomposition_tailored_strategy_.get_time_master_stage().count(), 0);
	EXPECT_GE(decomposition_tailored_strategy_.get_time_feas_pt_stage().count(), 0);
	EXPECT_GE(decomposition_tailored_strategy_multicut_.get_time_sub_stage().count(), 0);
	EXPECT_GE(decomposition_tailored_strategy_multicut_.get_time_master_stage().count(), 0);
	EXPECT_GE(decomposition_tailored_strategy_multicut_.get_time_feas_pt_stage().count(), 0);
	//Init to 0
	EXPECT_EQ(decomposition_only_cw_no_iter_.get_time_sub_stage().count(), 0);
	EXPECT_EQ(decomposition_only_cw_no_iter_.get_time_master_stage().count(), 0);
	EXPECT_EQ(decomposition_only_cw_no_iter_.get_time_feas_pt_stage().count(), 0);
}

TEST_F(BendersTest, Convergence) {
	EXPECT_TRUE(decomposition_only_cw_.get_converged());
	EXPECT_FALSE(decomposition_only_cw_no_conv_.get_converged());
	EXPECT_FALSE(decomposition_only_cw_no_iter_.get_converged());
	EXPECT_TRUE(decomposition_only_l1_.get_converged());
	EXPECT_TRUE(decomposition_both_cw_.get_converged());
	EXPECT_TRUE(decomposition_both_l1_.get_converged());
	EXPECT_TRUE(decomposition_tailored_strategy_.get_converged());
	EXPECT_TRUE(decomposition_tailored_strategy_multicut_.get_converged());
}

TEST_F(BendersTest, SubproblemVectorSize) {
	EXPECT_EQ(decomposition_only_cw_.ProvideSubproblemVec().size(), 1);
	EXPECT_EQ(decomposition_only_l1_.ProvideSubproblemVec().size(), 1);
	EXPECT_EQ(decomposition_both_cw_.ProvideSubproblemVec().size(), 2);
	EXPECT_EQ(decomposition_both_l1_.ProvideSubproblemVec().size(), 2);
	EXPECT_EQ(decomposition_tailored_strategy_.ProvideSubproblemVec().size(), 2);
	EXPECT_EQ(decomposition_tailored_strategy_multicut_.ProvideSubproblemVec().size(), 3);
	//Sub prob is set up in ctor so should already be 1 even without iterating
	EXPECT_EQ(decomposition_only_cw_no_iter_.ProvideSubproblemVec().size(), 1);
}

TEST_F(BendersTest, FinalIters) {
	EXPECT_EQ(decomposition_only_cw_.get_final_iters(), 2);
	EXPECT_EQ(decomposition_only_l1_.get_final_iters(), 1);
	EXPECT_EQ(decomposition_both_cw_.get_final_iters(), 1);
	EXPECT_EQ(decomposition_both_l1_.get_final_iters(), 1);
	//Tailored strategy performing worst here is unexpected, yet has an expanation:
	//Nominal production leads to an optimal OPF (master) cost, so the CW search direction is actually a good choice here.
	//L1 search direction allows for other search directions in the subproblem, and as such may (and here does) lead to an optimal L1-subproblem solution that leads to a suboptimal OPF solution.
	//In general, this behaviour is not expected since nominal production will normally not lead to an optimal cooperative OPF solution.
	EXPECT_EQ(decomposition_tailored_strategy_.get_final_iters(), 3);
	EXPECT_EQ(decomposition_tailored_strategy_multicut_.get_final_iters(), 3);
	EXPECT_EQ(decomposition_only_cw_no_conv_.get_final_iters(), 1);
	//Init to 0
	EXPECT_EQ(decomposition_only_cw_no_iter_.get_final_iters(), 0);
}

TEST_F(BendersTest, DetectableGap) {

	//Should be 0 (or machine precision) since the toy model has a feasible optiomal solution
	EXPECT_DOUBLE_EQ(decomposition_only_cw_no_iter_.get_detectable_opt_gap(), 1);
	EXPECT_DOUBLE_EQ(decomposition_only_cw_no_conv_.get_detectable_opt_gap(), 1);
	EXPECT_NEAR(decomposition_only_cw_.get_detectable_opt_gap(), 0.0, 1e-14);
	EXPECT_NEAR(decomposition_only_l1_.get_detectable_opt_gap(), 0.0, 1e-14);
	EXPECT_NEAR(decomposition_both_cw_.get_detectable_opt_gap(), 0.0, 1e-14);
	EXPECT_NEAR(decomposition_both_l1_.get_detectable_opt_gap(), 0.0, 1e-14);
	EXPECT_NEAR(decomposition_tailored_strategy_.get_detectable_opt_gap(), 0.0, 1e-14);
	EXPECT_NEAR(decomposition_tailored_strategy_multicut_.get_detectable_opt_gap(), 0.0, 1e-14);
}

TEST_F(BendersTest, AcceptedCost) {
	EXPECT_DOUBLE_EQ(decomposition_only_cw_.get_accepted_cost(), 2070);
	EXPECT_DOUBLE_EQ(decomposition_only_l1_.get_accepted_cost(), 2070);
	EXPECT_DOUBLE_EQ(decomposition_both_cw_.get_accepted_cost(), 2070);
	EXPECT_DOUBLE_EQ(decomposition_both_l1_.get_accepted_cost(), 2070);
	EXPECT_DOUBLE_EQ(decomposition_tailored_strategy_.get_accepted_cost(), 2070);
	EXPECT_DOUBLE_EQ(decomposition_tailored_strategy_multicut_.get_accepted_cost(), 2070);
	//If no convergence, accepted cost is just a lower bound, so should expect value lower than optimum
	EXPECT_LT(decomposition_only_cw_no_conv_.get_accepted_cost(), 2070);
	EXPECT_EQ(decomposition_only_cw_no_iter_.get_accepted_cost(), 0);
}

TEST_F(BendersTest, TotFlexPower) {
	EXPECT_DOUBLE_EQ(decomposition_only_cw_.get_tot_flex_power(), 100);
	EXPECT_DOUBLE_EQ(decomposition_only_cw_no_iter_.get_tot_flex_power(), 100);
}

TEST_F(BendersTest, Timesteps) {
	EXPECT_EQ(decomposition_only_cw_.get_num_timesteps(), 2);
}

TEST_F(BendersTest, Plantbus) {
	EXPECT_EQ(decomposition_only_cw_.GetPlantbus().size(), 1);
	EXPECT_EQ(decomposition_only_cw_.GetPlantbus().at(0).first, 2);
	EXPECT_EQ(decomposition_only_cw_.GetPlantbus().at(0).second, 1);
}

TEST_F(BendersTest, Tau) {
	EXPECT_DOUBLE_EQ(decomposition_only_cw_.get_tau(), 100);
	EXPECT_DOUBLE_EQ(decomposition_only_l1_.get_tau(), 100);
	EXPECT_DOUBLE_EQ(decomposition_both_cw_.get_tau(), 100);
	EXPECT_DOUBLE_EQ(decomposition_both_l1_.get_tau(), 100);
	EXPECT_NEAR(decomposition_tailored_strategy_.get_tau(), 59.5, 1e-3);
	EXPECT_NEAR(decomposition_tailored_strategy_multicut_.get_tau(), 59.5, 1e-3);
	EXPECT_DOUBLE_EQ(decomposition_only_cw_no_iter_.get_tau(), 100);
}

TEST_F(BendersTest, OptimalityGapExceptions) {
	std::ifstream in(test_dir + "regular/gridSpecs.txt");
	double tmp;
	std::string tmp_str;
	std::unordered_map<int, double> bus_data_reg;
	int bus_no;
	double load;
	in >> tmp;
	for (unsigned i = 1; i <= options_.timesteps; ++i) {
		in >> tmp_str >> tmp;
	}
	while (in >> bus_no >> load) {
		bus_data_reg[bus_no] = load;
	}
	in.close();

	//dummy coop solution cost to make the real gap negative
	double coop_sol_val = 4140;
	CoopBendersAlgorithm decomposition_only_cw_neg_real_gap(out_file_, test_dir + "regular/only_cw/", options_.timesteps, options_.plantbusses, bus_data_reg, true, coop_sol_val);
	//dummy cost to trigger real > detectable gap
	coop_sol_val = 0;
	CoopBendersAlgorithm decomposition_only_cw_real_bigger_than_detectable(out_file_, test_dir + "regular/only_cw/", options_.timesteps, options_.plantbusses, bus_data_reg, true, coop_sol_val);
	//don't currently see a way to purposefully trigger the detectable gap being negative
	//CoopBendersAlgorithm decomposition_only_cw_neg_detect_gap(out_file_, test_dir + "regular/only_cw/", options_.timesteps, options_.plantbusses, false);
	//Since either gap being negative throws the same type of exception, test exception message to ensure the correct gap threw it
	EXPECT_THROW({ 
		try { decomposition_only_cw_neg_real_gap.Iterate(); }
		catch (const NegativeOptimalityGapException& e) {
			std::string tmp = e.what();
			EXPECT_TRUE(tmp.find("real") != std::string::npos);
			throw;
		}
		}, NegativeOptimalityGapException);
	//EXPECT_THROW({
	//	try { decomposition_only_cw_neg_detect_gap.Iterate(); }
	//	catch (const NegativeOptimalityGapException& e) {
	//		std::string tmp = e.what();
	//		EXPECT_TRUE(tmp.find("detectable") != std::string::npos);
	//		throw;
	//	}
	//	}, NegativeOptimalityGapException);
	EXPECT_THROW(decomposition_only_cw_real_bigger_than_detectable.Iterate(), WrongOptimalityGapException);
}

TEST_F(BendersTest, FixedPPlant) {
	EXPECT_EQ(decomposition_only_cw_.GetFixedPPlant().size(), 1);
	EXPECT_DOUBLE_EQ(decomposition_only_cw_.GetFixedPPlant()[0].at(0), 110);
	EXPECT_DOUBLE_EQ(decomposition_only_cw_.GetFixedPPlant()[0].at(1), 90);
	EXPECT_DOUBLE_EQ(decomposition_only_l1_.GetFixedPPlant()[0].at(0), 110);
	EXPECT_DOUBLE_EQ(decomposition_only_l1_.GetFixedPPlant()[0].at(1), 90);
	EXPECT_NEAR(decomposition_both_cw_.GetFixedPPlant()[0].at(0), 100, 1e-10);
	EXPECT_NEAR(decomposition_both_cw_.GetFixedPPlant()[0].at(1), 100, 1e-10);
	EXPECT_NEAR(decomposition_both_l1_.GetFixedPPlant()[0].at(0), 100, 1e-10);
	EXPECT_NEAR(decomposition_both_l1_.GetFixedPPlant()[0].at(1), 100, 1e-10);
	EXPECT_DOUBLE_EQ(decomposition_tailored_strategy_.GetFixedPPlant()[0].at(0), 110);
	EXPECT_DOUBLE_EQ(decomposition_tailored_strategy_.GetFixedPPlant()[0].at(1), 90);
	EXPECT_DOUBLE_EQ(decomposition_tailored_strategy_multicut_.GetFixedPPlant()[0].at(0), 110);
	EXPECT_DOUBLE_EQ(decomposition_tailored_strategy_multicut_.GetFixedPPlant()[0].at(1), 90);
	//No feasible point after just 1 iteration, so fixed load should be at 0 which it was initialized to
	EXPECT_DOUBLE_EQ(decomposition_only_cw_no_conv_.GetFixedPPlant()[0].at(0), 0);
	EXPECT_DOUBLE_EQ(decomposition_only_cw_no_conv_.GetFixedPPlant()[0].at(1), 0);
	//Initialize to 0
	EXPECT_DOUBLE_EQ(decomposition_only_cw_no_iter_.GetFixedPPlant()[0].at(0), 0);
	EXPECT_DOUBLE_EQ(decomposition_only_cw_no_iter_.GetFixedPPlant()[0].at(1), 0);
}

TEST_F(BendersTest, MasterPPlantVar) {
	EXPECT_EQ(decomposition_only_cw_.get_master_p_plant_var().size(), 1);
	EXPECT_DOUBLE_EQ(decomposition_only_cw_.get_master_p_plant_var()[0].at(0).get(GRB_DoubleAttr_X), 135);
	EXPECT_DOUBLE_EQ(decomposition_only_cw_.get_master_p_plant_var()[0].at(1).get(GRB_DoubleAttr_X), 65);
	EXPECT_DOUBLE_EQ(decomposition_only_l1_.get_master_p_plant_var()[0].at(0).get(GRB_DoubleAttr_X), 135);
	EXPECT_DOUBLE_EQ(decomposition_only_l1_.get_master_p_plant_var()[0].at(1).get(GRB_DoubleAttr_X), 65);
	EXPECT_NEAR(decomposition_both_cw_.get_master_p_plant_var()[0].at(0).get(GRB_DoubleAttr_X), 100, 1e-10);
	EXPECT_NEAR(decomposition_both_cw_.get_master_p_plant_var()[0].at(1).get(GRB_DoubleAttr_X), 100, 1e-10);
	EXPECT_NEAR(decomposition_both_l1_.get_master_p_plant_var()[0].at(0).get(GRB_DoubleAttr_X), 100, 1e-10);
	EXPECT_NEAR(decomposition_both_l1_.get_master_p_plant_var()[0].at(1).get(GRB_DoubleAttr_X), 100, 1e-10);
	EXPECT_DOUBLE_EQ(decomposition_tailored_strategy_.get_master_p_plant_var()[0].at(0).get(GRB_DoubleAttr_X), 110);
	EXPECT_DOUBLE_EQ(decomposition_tailored_strategy_.get_master_p_plant_var()[0].at(1).get(GRB_DoubleAttr_X), 90);
	EXPECT_DOUBLE_EQ(decomposition_tailored_strategy_multicut_.get_master_p_plant_var()[0].at(0).get(GRB_DoubleAttr_X), 110);
	EXPECT_DOUBLE_EQ(decomposition_tailored_strategy_multicut_.get_master_p_plant_var()[0].at(1).get(GRB_DoubleAttr_X), 90);
	EXPECT_DOUBLE_EQ(decomposition_only_cw_no_conv_.get_master_p_plant_var()[0].at(0).get(GRB_DoubleAttr_X), 140);
	EXPECT_DOUBLE_EQ(decomposition_only_cw_no_conv_.get_master_p_plant_var()[0].at(1).get(GRB_DoubleAttr_X), 20);
	EXPECT_THROW(decomposition_only_cw_no_iter_.get_master_p_plant_var()[0].at(0).get(GRB_DoubleAttr_X), GRBException);
	EXPECT_THROW(decomposition_only_cw_no_iter_.get_master_p_plant_var()[0].at(1).get(GRB_DoubleAttr_X), GRBException);
}

TEST_F(BendersTest, ThrowsNoUpdate) {
	//This is also very constructed, let the converged algorithm iteratate two more times (starting from the last accepted solution each time).
	//The first time, there will be a new suggestion here, as the last accepted solution was induced by an infeasible suggestion.
	//The new suggestion will itself be feasible, and so the second call to Iterate will throw the error we want.
	EXPECT_THROW({
		try {
			decomposition_only_cw_.Iterate();
			decomposition_only_cw_.Iterate();
		}
		catch (const std::runtime_error& e) {
			std::string tmp = e.what();
			EXPECT_TRUE(tmp.find("detected no new master suggestion") != std::string::npos);
			throw;
		}
		}, std::runtime_error);
}

TEST_F(BendersTest, InitialTauFactor) {
	EXPECT_DOUBLE_EQ(decomposition_only_cw_.GetInitialTau(), 1);
	EXPECT_DOUBLE_EQ(decomposition_only_cw_no_iter_.GetInitialTau(), 1);
	EXPECT_DOUBLE_EQ(decomposition_only_cw_no_conv_.GetInitialTau(), 1);
}

TEST_F(BendersTest, AcceptableGap) {
	EXPECT_DOUBLE_EQ(decomposition_only_cw_.GetAcceptableGap(), 0.1);
	EXPECT_DOUBLE_EQ(decomposition_only_cw_no_iter_.GetAcceptableGap(), 0.1);
	EXPECT_DOUBLE_EQ(decomposition_only_cw_no_conv_.GetAcceptableGap(), 0.1);
}

TEST_F(SubproblemTest, Slacks) {
	EXPECT_THROW(sub_l1_[0].GetNSlackValues(), GRBException);
	EXPECT_THROW(sub_l1_[0].GetPSlackValues(), GRBException);
	EXPECT_EQ(sub_l1_[1].GetNSlackValues().size(), 2);
	EXPECT_EQ(sub_l1_[1].GetPSlackValues().size(), 2);
	EXPECT_DOUBLE_EQ(sub_l1_[1].GetNSlackValues()[0], 0);
	EXPECT_DOUBLE_EQ(sub_l1_[1].GetPSlackValues()[0], 0.9);
	EXPECT_DOUBLE_EQ(sub_l1_[1].GetNSlackValues()[1], 0);
	EXPECT_DOUBLE_EQ(sub_l1_[1].GetPSlackValues()[1], 0.7);
	EXPECT_DOUBLE_EQ(sub_l1_[2].GetNSlackValues()[0], 0);
	EXPECT_NEAR(sub_l1_[2].GetPSlackValues()[0], 0, 1e-14);
	EXPECT_DOUBLE_EQ(sub_l1_[2].GetNSlackValues()[1], 0);
	EXPECT_DOUBLE_EQ(sub_l1_[2].GetPSlackValues()[1], 0);
}

TEST_F(SubproblemTest, Modelstatus) {
	EXPECT_EQ(sub_cw_[0].GetModelStatus(), GRB_LOADED);
	EXPECT_EQ(sub_cw_[1].GetModelStatus(), GRB_OPTIMAL);
	EXPECT_EQ(sub_cw_[2].GetModelStatus(), GRB_OPTIMAL);

	EXPECT_EQ(sub_l1_[0].GetModelStatus(), GRB_LOADED);
	EXPECT_EQ(sub_l1_[1].GetModelStatus(), GRB_OPTIMAL);
	EXPECT_EQ(sub_l1_[2].GetModelStatus(), GRB_OPTIMAL);
}

TEST_F(SubproblemTest, Plantload) {
	EXPECT_DOUBLE_EQ(sub_cw_[0].get_plant_load(), 100);

	EXPECT_DOUBLE_EQ(sub_l1_[0].get_plant_load(), 100);
}

TEST_F(SubproblemTest, PowSugg) {
	EXPECT_EQ(sub_cw_[0].get_pow_sugg().size(), 2);
	EXPECT_DOUBLE_EQ(sub_cw_[0].get_pow_sugg()[0], 0);
	EXPECT_DOUBLE_EQ(sub_cw_[0].get_pow_sugg()[1], 0);
	EXPECT_DOUBLE_EQ(sub_cw_[1].get_pow_sugg()[0], 0.2);
	EXPECT_DOUBLE_EQ(sub_cw_[1].get_pow_sugg()[1], 0.2);
	EXPECT_DOUBLE_EQ(sub_cw_[2].get_pow_sugg()[0], 1.05);
	EXPECT_DOUBLE_EQ(sub_cw_[2].get_pow_sugg()[1], 0.95);
	//test different versions of set and get
	std::vector<double> tmp(2, 1.0);
	sub_cw_[0].set_pow_sugg(tmp);
	EXPECT_EQ(sub_cw_[0].get_pow_sugg(), tmp);
	EXPECT_DOUBLE_EQ(sub_cw_[0].get_pow_sugg(0), 1.0);
	EXPECT_DOUBLE_EQ(sub_cw_[0].get_pow_sugg(1), 1.0);
	sub_cw_[0].set_pow_sugg(0, 0);
	sub_cw_[0].set_pow_sugg(1, 0);
	EXPECT_DOUBLE_EQ(sub_cw_[0].get_pow_sugg(0), 0);
	EXPECT_DOUBLE_EQ(sub_cw_[0].get_pow_sugg(1), 0);

	EXPECT_EQ(sub_l1_[0].get_pow_sugg().size(), 2);
	EXPECT_DOUBLE_EQ(sub_l1_[0].get_pow_sugg()[0], 0);
	EXPECT_DOUBLE_EQ(sub_l1_[0].get_pow_sugg()[1], 0);
	EXPECT_DOUBLE_EQ(sub_l1_[1].get_pow_sugg()[0], 0.2);
	EXPECT_DOUBLE_EQ(sub_l1_[1].get_pow_sugg()[1], 0.2);
	EXPECT_DOUBLE_EQ(sub_l1_[2].get_pow_sugg()[0], 1.05);
	EXPECT_DOUBLE_EQ(sub_l1_[2].get_pow_sugg()[1], 0.95);
	//test different versions of set and get
	sub_l1_[0].set_pow_sugg(tmp);
	EXPECT_EQ(sub_l1_[0].get_pow_sugg(), tmp);
	EXPECT_DOUBLE_EQ(sub_l1_[0].get_pow_sugg(0), 1.0);
	EXPECT_DOUBLE_EQ(sub_l1_[0].get_pow_sugg(1), 1.0);
	sub_l1_[0].set_pow_sugg(0, 0);
	sub_l1_[0].set_pow_sugg(1, 0);
	EXPECT_DOUBLE_EQ(sub_l1_[0].get_pow_sugg(0), 0);
	EXPECT_DOUBLE_EQ(sub_l1_[0].get_pow_sugg(1), 0);
}

TEST_F(SubproblemTest, SubObjective) {
	//Should have no objective record before solving so this should throw an exception
	EXPECT_THROW(sub_cw_[0].GetSubproblemObjective(), GRBException);
	EXPECT_DOUBLE_EQ(sub_cw_[1].GetSubproblemObjective(), 1);
	EXPECT_NEAR(sub_cw_[2].GetSubproblemObjective(), 0.0, 1e-14);

	EXPECT_THROW(sub_l1_[0].GetSubproblemObjective(), GRBException);
	EXPECT_DOUBLE_EQ(sub_l1_[1].GetSubproblemObjective(), 1.6);
	EXPECT_NEAR(sub_l1_[2].GetSubproblemObjective(), 0.0, 1e-14);
}

TEST_F(SubproblemTest, FeasDistance) {
	EXPECT_THROW(sub_cw_[0].ComputeFeasDistance(), GRBException); //no objective available yet
	EXPECT_DOUBLE_EQ(sub_cw_[1].ComputeFeasDistance(), 160);
	EXPECT_DOUBLE_EQ(sub_cw_[2].ComputeFeasDistance(), 0);

	EXPECT_THROW(sub_l1_[0].ComputeFeasDistance(), GRBException); //no objective available yet
	EXPECT_DOUBLE_EQ(sub_l1_[1].ComputeFeasDistance(), 160);
	EXPECT_DOUBLE_EQ(sub_l1_[2].ComputeFeasDistance(), 0);
}

TEST_F(SubproblemTest, FeasSol) {
	EXPECT_THROW(sub_cw_[0].ComputeFeasSol(), GRBException);
	EXPECT_THROW(sub_cw_[0].ComputeFeasSol(0), GRBException);
	EXPECT_THROW(sub_cw_[0].ComputeFeasSol(1), GRBException);
	std::vector<double> feas_sol_cw(2, 100);
	std::vector<double> computed_feas_sol_cw = sub_cw_[1].ComputeFeasSol();
	for (unsigned i = 0; i < 2; ++i) {
		EXPECT_DOUBLE_EQ(computed_feas_sol_cw[i], feas_sol_cw[i]);
		EXPECT_DOUBLE_EQ(sub_cw_[1].ComputeFeasSol(i), feas_sol_cw[i]);
	}
	feas_sol_cw[0] = 105;
	feas_sol_cw[1] = 95;
	computed_feas_sol_cw = sub_cw_[2].ComputeFeasSol();
	for (unsigned i = 0; i < 2; ++i) {
		EXPECT_DOUBLE_EQ(computed_feas_sol_cw[i], feas_sol_cw[i]);
		EXPECT_DOUBLE_EQ(sub_cw_[2].ComputeFeasSol(i), feas_sol_cw[i]);
	}
	
	EXPECT_THROW(sub_l1_[0].ComputeFeasSol(), GRBException);
	EXPECT_THROW(sub_l1_[0].ComputeFeasSol(0), GRBException);
	EXPECT_THROW(sub_l1_[0].ComputeFeasSol(1), GRBException);
	std::vector<double> feas_sol_l1{110, 90};
	std::vector<double> computed_feas_sol_l1 = sub_l1_[1].ComputeFeasSol();
	for (unsigned i = 0; i < 2; ++i) {
		EXPECT_DOUBLE_EQ(computed_feas_sol_l1[i], feas_sol_l1[i]);
		EXPECT_DOUBLE_EQ(sub_l1_[1].ComputeFeasSol(i), feas_sol_l1[i]);
	}
	feas_sol_l1[0] = 105;
	feas_sol_l1[1] = 95;
	computed_feas_sol_l1 = sub_l1_[2].ComputeFeasSol();
	for (unsigned i = 0; i < 2; ++i) {
		EXPECT_DOUBLE_EQ(computed_feas_sol_l1[i], feas_sol_l1[i]);
		EXPECT_DOUBLE_EQ(sub_l1_[2].ComputeFeasSol(i), feas_sol_l1[i]);
	}
}

TEST_F(SubproblemTest, FeasDistance) {
	EXPECT_THROW((*sub_p_)[0].get_feas_distance(), GRBException); //no objective available yet
	EXPECT_DOUBLE_EQ((*sub_p_)[1].get_feas_distance(), 160);
	EXPECT_DOUBLE_EQ((*sub_p_)[2].get_feas_distance(), 0);
}

TEST_F(SubproblemTest, FeasSol) {
	EXPECT_THROW((*sub_p_)[0].get_feas_sol(), GRBException);
	EXPECT_THROW((*sub_p_)[0].get_feas_sol(0), GRBException);
	EXPECT_THROW((*sub_p_)[0].get_feas_sol(1), GRBException);
	std::vector<double> feas_sol{ 180, 20 };
	EXPECT_EQ((*sub_p_)[1].get_feas_sol(), feas_sol);
	EXPECT_DOUBLE_EQ((*sub_p_)[1].get_feas_sol(0), feas_sol[0]);
	EXPECT_DOUBLE_EQ((*sub_p_)[1].get_feas_sol(1), feas_sol[1]);
	feas_sol[0] = 135;
	feas_sol[1] = 65;
	EXPECT_EQ((*sub_p_)[2].get_feas_sol(), feas_sol);
	EXPECT_DOUBLE_EQ((*sub_p_)[2].get_feas_sol(0), feas_sol[0]);
	EXPECT_DOUBLE_EQ((*sub_p_)[2].get_feas_sol(1), feas_sol[1]);

}


TEST_F(SubproblemTest, CutCoeffs) {
	//should get a Gurobi exception when trying this before having solved the sp
	EXPECT_THROW(sub_cw_[0].GetNewCoeff(0), GRBException);
	EXPECT_THROW(sub_cw_[0].GetNewCoeff(1), GRBException);
	//coeffs returned are scaled by plant load, so multiply by that here
	std::vector<double> coeffs1_cw = sub_cw_[1].GetNewCoeffs();
	std::vector<double> coeffs2_cw = sub_cw_[2].GetNewCoeffs();
	EXPECT_DOUBLE_EQ(coeffs1_cw[0], sub_cw_[1].GetNewCoeff(0));
	EXPECT_DOUBLE_EQ(coeffs1_cw[1], sub_cw_[1].GetNewCoeff(1));
	EXPECT_DOUBLE_EQ(coeffs2_cw[0], sub_cw_[2].GetNewCoeff(0));
	EXPECT_DOUBLE_EQ(coeffs2_cw[1], sub_cw_[2].GetNewCoeff(1));
	EXPECT_NEAR(sub_cw_[1].GetNewCoeff(0)*sub_cw_[1].get_plant_load(), 5.0/6.0, 1e-10);
	EXPECT_NEAR(sub_cw_[1].GetNewCoeff(1)*sub_cw_[1].get_plant_load(), 5.0/12.0, 1e-10);
	EXPECT_DOUBLE_EQ(sub_cw_[2].GetNewCoeff(0)*sub_cw_[2].get_plant_load(), 0);
	EXPECT_DOUBLE_EQ(sub_cw_[2].GetNewCoeff(1)*sub_cw_[2].get_plant_load(), 0);

	//should get a Gurobi exception when trying this before having solved the sp
	EXPECT_THROW(sub_l1_[0].GetNewCoeff(0), GRBException);
	EXPECT_THROW(sub_l1_[0].GetNewCoeff(1), GRBException);
	//coeffs returned are scaled by plant load, so multiply by that here
	std::vector<double> coeffs1_l1 = sub_l1_[1].GetNewCoeffs();
	std::vector<double> coeffs2_l1 = sub_l1_[2].GetNewCoeffs();
	EXPECT_DOUBLE_EQ(coeffs1_l1[0], sub_l1_[1].GetNewCoeff(0));
	EXPECT_DOUBLE_EQ(coeffs1_l1[1], sub_l1_[1].GetNewCoeff(1));
	EXPECT_DOUBLE_EQ(coeffs2_l1[0], sub_l1_[2].GetNewCoeff(0));
	EXPECT_DOUBLE_EQ(coeffs2_l1[1], sub_l1_[2].GetNewCoeff(1));
	EXPECT_DOUBLE_EQ(sub_l1_[1].GetNewCoeff(0)*sub_l1_[1].get_plant_load(), 1.0);
	EXPECT_DOUBLE_EQ(sub_l1_[1].GetNewCoeff(1)*sub_l1_[1].get_plant_load(), 1.0);
	EXPECT_DOUBLE_EQ(sub_l1_[2].GetNewCoeff(0)*sub_l1_[2].get_plant_load(), 0);
	EXPECT_DOUBLE_EQ(sub_l1_[2].GetNewCoeff(1)*sub_l1_[2].get_plant_load(), 0);
}

TEST_F(SubproblemTest, CutRhs) {
	EXPECT_THROW(sub_cw_[0].GetNewRhs(), GRBException);
	EXPECT_DOUBLE_EQ(sub_cw_[1].GetNewRhs(), 1.25);
	EXPECT_DOUBLE_EQ(sub_cw_[2].GetNewRhs(), 0);

	EXPECT_THROW(sub_l1_[0].GetNewRhs(), GRBException);
	EXPECT_DOUBLE_EQ(sub_l1_[1].GetNewRhs(), 2.0);
	EXPECT_DOUBLE_EQ(sub_l1_[2].GetNewRhs(), 0);
}

TEST_F(SubproblemTest, CurrentCutLhs) {
	EXPECT_THROW(sub_cw_[0].GetCurrLhs(), GRBException);
	EXPECT_DOUBLE_EQ(sub_cw_[1].GetCurrLhs(), 0.25);
	EXPECT_DOUBLE_EQ(sub_cw_[2].GetCurrLhs(), 0);

	EXPECT_THROW(sub_l1_[0].GetCurrLhs(), GRBException);
	EXPECT_DOUBLE_EQ(sub_l1_[1].GetCurrLhs(), 0.4);
	EXPECT_DOUBLE_EQ(sub_l1_[2].GetCurrLhs(), 0);
}

TEST_F(SubproblemTest, GenerateAndVerifyCut) {
	EXPECT_THROW(sub_cw_[0].GenerateAndVerifyCut(), GRBException);
	EXPECT_DOUBLE_EQ(sub_cw_[1].GenerateAndVerifyCut(), 1.25);
	EXPECT_THROW(sub_cw_[2].GenerateAndVerifyCut(), IrrelevantCutException);

	EXPECT_THROW(sub_l1_[0].GenerateAndVerifyCut(), GRBException);
	EXPECT_DOUBLE_EQ(sub_l1_[1].GenerateAndVerifyCut(), 2.0);
	EXPECT_THROW(sub_l1_[2].GenerateAndVerifyCut(), IrrelevantCutException);
	//try to generate an invalid cut. done by changing the power suggestion without another solve
	//this is very constructed, but the goal is just to throw a BadCutException and this seems to be the only way to get it done to me
	double original = sub_l1_[1].get_pow_sugg()[0];
	sub_l1_[1].set_pow_sugg(0, 0);
	EXPECT_THROW(sub_l1_[1].GenerateAndVerifyCut(), BadCutException);
	//reset to original suggestion
	sub_l1_[1].set_pow_sugg(0, original);

}

TEST(PlantbusInfoTest, NoUpdate) {
	PlantbusInfo pb_info;
	EXPECT_FALSE(pb_info.get_no_update());
	pb_info.set_no_update(true);
	EXPECT_TRUE(pb_info.get_no_update());
	pb_info.set_no_update(false);
	EXPECT_FALSE(pb_info.get_no_update());
}

void CopySubproblemOptionFileToTestDirectories(const std::string& test_dir) {
	const auto sub_copy_options = std::filesystem::copy_options::overwrite_existing;
	std::filesystem::copy_file(test_dir + "Workspace/options_subproblem.txt", test_dir + "regular/only_cw/options_subproblem.txt", sub_copy_options);
	std::filesystem::copy_file(test_dir + "Workspace/options_subproblem.txt", test_dir + "regular/only_l1/options_subproblem.txt", sub_copy_options);
	std::filesystem::copy_file(test_dir + "Workspace/options_subproblem.txt", test_dir + "regular/both_cw/options_subproblem.txt", sub_copy_options);
	std::filesystem::copy_file(test_dir + "Workspace/options_subproblem.txt", test_dir + "regular/both_l1/options_subproblem.txt", sub_copy_options);
	std::filesystem::copy_file(test_dir + "Workspace/options_subproblem.txt", test_dir + "regular/tailored_strategy/options_subproblem.txt", sub_copy_options);
	std::filesystem::copy_file(test_dir + "Workspace/options_subproblem.txt", test_dir + "regular/tailored_strategy_multicut/options_subproblem.txt", sub_copy_options);
	std::filesystem::copy_file(test_dir + "Workspace/options_subproblem.txt", test_dir + "no_conv/options_subproblem.txt", sub_copy_options);
}

int main(int argc, char* argv[]) {
	testing::InitGoogleTest(&argc, argv);
	InputTest::test_dir = argv[1];
	BendersTest::test_dir = argv[1];
	SubproblemTest::test_dir = argv[1];
	CopySubproblemOptionFileToTestDirectories(argv[1]);
	return RUN_ALL_TESTS();
}
