/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

/**
* \file inputStructs.cpp
* \brief Implementation of constructors for %AlgorithmOptions, %MainOptions and %SubproblemOptions as well as methods to read into these structs from input files.
*/


#include "inputStructs.hpp"

namespace decoop {

	void ParseComments(std::fstream& input) {
		std::string tmp;
		std::string tmp2;
		tmp = static_cast<char>(input.peek());
		while (tmp == "#" || tmp == "\n") {
			std::getline(input, tmp2);
			tmp = static_cast<char>(input.peek());
		}
	}

	std::fstream& operator>> (std::fstream& in, SubproblemOptions& opt) {
		std::stringstream sstream;
		std::string token;
		std::string line;
		while (!in.eof()) {
			sstream.clear();
			ParseComments(in);
			std::getline(in, line);
			sstream.str(line);
			sstream >> token;
			if (token == "LP_FILE_PATH") {
				sstream >> opt.workspace_dir;
			}
			else if (token == "LP_FILE_NAME_L1") {
				sstream >> opt.lp_file_l1;
			}
			else if (token == "LP_FILE_NAME_CW") {
				sstream >> opt.lp_file_cw;
			}
			else {
				std::cout << "unkonwn keyword: " << token << std::endl;
			}
		}
		return in;
	}

	std::fstream& operator>> (std::fstream& in, MainOptions& opt) {
		std::stringstream sstream;
		std::string token;
		std::string line;

		while (!in.eof()) {
			sstream.clear();
			ParseComments(in);
			std::getline(in, line);
			sstream.str(line);
			sstream >> token;
			if (token=="LP_NOMINAL_OPERATION") {
				sstream >> opt.nominal_lp;
			}
			else if (token=="LP_PRICE_CALCULATION") {
				sstream >> opt.price_lp;
			}
			else if (token=="LP_INTEGRATED") {
				sstream >> opt.integrated_lp;
			}
			else if (token=="LP_SUB_DR") {
				sstream >> opt.sub_dr_lp;
			}
			else if (token=="NR_TIMESTEPS") {
				int tmp;
				sstream >> tmp;
				if (tmp < 0) {
					throw std::runtime_error("Number of timesteps must be a positive integer!");
				}
				opt.timesteps = static_cast<unsigned>(tmp);
			}
			else if (token == "MULTI_PURPOSE_STRING") {
				sstream >> opt.univ_str;
			}
			else if (token == "PLANTBUSSES") {
				int plantbus;
				double load_fraction;
				while (sstream >> plantbus >> load_fraction) {
					opt.plantbusses.emplace_back(std::make_pair(plantbus, load_fraction));
				}
				std::unordered_map<int, bool> plantbus_counter;
				for (auto& pb_rec : opt.plantbusses) {
					plantbus_counter[pb_rec.first] = true;
				}
				if (plantbus_counter.size() != opt.plantbusses.size()) {
					// If the sizes don't match, then opt.plantbusses has duplicates
					throw std::runtime_error("Every plantbus should only appear once in the list!");
				}
			}
			else if (token == "CONG_INDICATOR") {
				sstream >> opt.cong_indicator;
			}
			else if (token == "USE_INTEGRATED") {
				sstream >> opt.use_int_prob;
			}
			else if (token == "OUTPUT_FILE") {
				sstream >> opt.output_name;
			}
			else if (token == "CSV_FILE") {
				sstream >> opt.csv_name;
			}
			else if (token == "CASE_STUDY_CSV") {
				sstream >> opt.spec_name;
			}
			else if (token == "CASE_STUDY_INFO") {
				sstream >> opt.spec_func;
				if (opt.spec_func < 1 || opt.spec_func > 4) {
					throw std::runtime_error("Case study info should be 1, 2, 3 or 4!");
				}
			}
			else {
				std::cout << "unkonwn keyword: " << token << std::endl;
			}
		}
		return in;
	}

	std::fstream& operator>> (std::fstream& in, AlgorithmOptions& opt) {
		std::stringstream sstream;
		std::string token;
		std::string line;
		while (!in.eof()) {
			sstream.clear();
			ParseComments(in);
			std::getline(in, line);
			sstream.str(line);
			sstream >> token;
			if (token == "LP_MASTER_FIXED") {
				sstream >> opt.master_lp_file;
			}
			else if (token == "MAXIMUM_ITERATIONS") {
				int tmp;
				sstream >> tmp;
				if (tmp <= 0) {
					throw std::runtime_error("Max iters must be > 0!");
				}
				opt.max_iters = static_cast<unsigned>(tmp);
			}
			else if (token == "PRIMARY_SUB_TYPE") {
				sstream >> opt.primary_sp_type;
				if (opt.primary_sp_type > 4) {
					// this check will catch every bad input except for those in range [-UINT_MAX-1, -UINT_MAX + 3] but such an input is not to be ever expected
					throw std::runtime_error("Primary subproblem type must be an integer between 0 and 4!");
				}
			}
			else if (token == "SECONDARY_SUB_TYPE") {
				sstream >> opt.secondary_sp_type;
				if (opt.secondary_sp_type > 1) {
					// this check will catch every bad input except for those in range [-UINT_MAX-1, -UINT_MAX] but such an input is not to be ever expected
					throw std::runtime_error("Secondary subproblem type must be either 0 or 1!");
				}
			}
			else if (token == "MU") {
				sstream >> opt.mu;
				if (opt.mu < 0 || opt.mu > 1) {
					throw std::runtime_error("Mu must be in range [0, 1]!");
				}
				else if (opt.mu > 0.995) {
					std::cout << "Warning: mu > 0.995 may lead to numerical issues with secondary subproblem!" << std::endl;
				}
			}
			else if (token == "TAU_FACTOR") {
				sstream >> opt.initial_tau_factor;
			}
			else if (token == "ACCEPTABLE_GAP") {
				sstream >> opt.accept_gap;
			}
			else if (token == "SAFETY_FACTOR") {
				sstream >> opt.rho;
			}
			else if (token == "SUB_FEAS_VALUE") {
				sstream >> opt.eps;
			}
			else {
				std::cout << "unknown keyword: " << token << std::endl;
			}
		}
		return in;
	}

	SubproblemOptions::SubproblemOptions(const std::string &path, const bool path_includes_filename) {
		std::string path_and_name(path);
		if (!path_includes_filename) {
			path_and_name += "options_subproblem.txt";
		}
		std::fstream in(path_and_name);
		if (!in.is_open()) {
			throw std::runtime_error("failed to open SubproblemOption file. aborting");
		}
		in >> *this;
		in.close();
	}

	AlgorithmOptions::AlgorithmOptions(const std::string &path, const bool path_includes_filename) :
		max_iters{ 500 }, primary_sp_type{ 4 }, secondary_sp_type{ 0 },
		initial_tau_factor{ 1 }, accept_gap{ 1e-4 }, rho{ 1.19 }, eps{ 1e-7 }, mu{ 0.97 }
	{
		std::string path_and_name(path);
		if (!path_includes_filename) {
			path_and_name += "options_algorithm.txt";
		}
		std::fstream in(path_and_name);
		if (!in.is_open()) {
			throw std::runtime_error("failed to open AlgorithmOption file. aborting");
		}
		try {
			in >> *this;
		}
		catch (const std::runtime_error& e) {
			in.close();
			throw;
		}
		in.close();
		if (primary_sp_type == 1 && secondary_sp_type != 0) {
			secondary_sp_type = 0;
			std::cout << "Secondary cuts with OnlyL1 strategy are not allowed! Secondary cuts have been turned off." << std::endl;
		}
	}

	MainOptions::MainOptions(const std::string &path, const bool path_includes_filename) : use_int_prob{}, spec_func {} {
		std::string path_and_name(path);
		if (!path_includes_filename) {
			path_and_name += "options_main.txt";
		}
		std::fstream in(path_and_name);
		if (!in.is_open()) {
			throw std::runtime_error("failed to open MainOption file. aborting");
		}
		try {
			in >> *this;
		}
		catch (const std::runtime_error& e) {
			in.close();
			throw;
		}
		in.close();
	}
} //namespace decoop
