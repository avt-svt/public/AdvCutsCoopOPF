/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

/**
* \file coopBendersSubproblemCW.cpp
* \brief Implementation of %PlantbusSubproblemCW.
*/

#include "coopBendersSubproblemCW.hpp"

namespace decoop {

	unsigned long PlantbusSubproblemCW::InitObject() {
		for (int i = 0; i < variables_.size(); ++i) {
			std::string name = variables_[i].get(GRB_StringAttr_VarName);
			if (name.find("lambda") != std::string::npos) {
				lambda_index_ = i;
			}
			else {
				model_vars_.push_back(i);
			}
		}
		for (int i = 0; i < equations_.size(); ++i) {
			std::string name = equations_[i].get(GRB_StringAttr_ConstrName);
			if (name.find(coupling_equation_name_) != std::string::npos) {
				coupling_equation_.push_back(i);
			}
			else {
				constraints_.push_back(i);
			}
		}

		return constraints_.size() + model_vars_.size();
	}

	PlantbusSubproblemCW::PlantbusSubproblemCW(const GRBEnv& env, const std::string& option_path, const double plant_load, const unsigned num_timesteps) :
		options_{ option_path }, model_{ env, options_.workspace_dir + options_.lp_file_cw },
		equations_{ model_.getConstrs(), static_cast<size_t>(model_.get(GRB_IntAttr_NumConstrs)) },
		variables_{ model_.getVars(), static_cast<size_t>(model_.get(GRB_IntAttr_NumVars)) },
		coupling_equation_name_{ "masterSubCoupling" }, coupling_equation_{}, constraints_{}, model_vars_{},
		num_timesteps_{ num_timesteps }, pow_sugg_(num_timesteps_, 0.0),
		corepoint_(num_timesteps_, 1.0), plant_load_{ plant_load },	rhs_num_records_{ InitObject() }
	{
		model_.set(GRB_IntParam_Method, 1);
	}

	void PlantbusSubproblemCW::Solve() {
		//update coeff of lambda and RHS of the coupling constraints
		//multiply pow_sugg_ by -1 and set as RHS of constraints
		int i{ 0 };
		for (const int index : coupling_equation_) {
			model_.chgCoeff(equations_[index], variables_[lambda_index_], corepoint_[i] - pow_sugg_[i]); //old_coeff = corepoint + old_rhs
			equations_[index].set(GRB_DoubleAttr_RHS, -pow_sugg_[i]);
			++i;
		}

		try {
			model_.optimize();
		}
		catch (const GRBException& e) {
			std::cout << "GRBException: " << e.getMessage() << std::endl;
			throw;
		}
	}

	void PlantbusSubproblemCW::UpdateCorepoint(const unsigned key, const double new_corepoint) {
		corepoint_[key] = new_corepoint;
	}

	void PlantbusSubproblemCW::UpdateCorepoint(const std::vector<double>& new_corepoints) {
		corepoint_ = new_corepoints;
	}

	int PlantbusSubproblemCW::GetModelStatus() const {
		return model_.get(GRB_IntAttr_Status);
	}

	/**
	* \details Implementation currently does not check the model status here,
	*		   so calling this without ensuring the model has been solved may result in an exception thrown by Gurobi.
	*/
	double PlantbusSubproblemCW::GetNewRhs() const {
		//Should now have some positive number of records for computing rhs of the cut.
		//If that is not the case, throw an exception.
		if (rhs_num_records_ == 0) {
			throw std::runtime_error("Error: no records found to compute rhs of benders cut.");
		}

		std::vector<double> b(rhs_num_records_, 0.0);
		std::vector<double> lambda(rhs_num_records_, 0.0);
		int i = 0;

		//Box constraints on variables
		for (const int index : model_vars_) {
			const GRBVar& var = variables_[index];
			if (var.get(GRB_DoubleAttr_X) == var.get(GRB_DoubleAttr_LB)) {
				b[i] = var.get(GRB_DoubleAttr_X);
				lambda[i] = var.get(GRB_DoubleAttr_RC);
				++i;
			}
			else if (var.get(GRB_DoubleAttr_X) == var.get(GRB_DoubleAttr_UB)) {
				b[i] = var.get(GRB_DoubleAttr_X);
				lambda[i] = var.get(GRB_DoubleAttr_RC);
				++i;
			}
		}

		//Model equations
		for (const int index : constraints_) {
			const GRBConstr& constraint = equations_[index];
			b[i] = constraint.get(GRB_DoubleAttr_RHS);
			lambda[i] = constraint.get(GRB_DoubleAttr_Pi);
			++i;
		}

		return std::inner_product(b.begin(), b.end(), lambda.begin(), 0.0);
	}

	double PlantbusSubproblemCW::GetCurrLhs() const {
		std::vector<double> lambda(num_timesteps_);
		int i = 0;
		for (const int index : coupling_equation_) {
			lambda[i] = equations_[index].get(GRB_DoubleAttr_Pi);
			++i;
		}

		return std::inner_product(pow_sugg_.begin(), pow_sugg_.end(), lambda.begin(), 0.0);
	}

	double PlantbusSubproblemCW::GetNewCoeff(const unsigned key) const {
		const GRBConstr& eq = equations_[coupling_equation_.at(key)];
		return eq.get(GRB_DoubleAttr_Pi) / plant_load_;
	}

	std::vector<double> PlantbusSubproblemCW::GetNewCoeffs() const {
		std::vector<double> result(num_timesteps_);
		for (unsigned i = 0; i < num_timesteps_; ++i) {
			const GRBConstr& eq = equations_[coupling_equation_.at(i)];
			result[i] = eq.get(GRB_DoubleAttr_Pi) / plant_load_;
		}
		return result;
	}

	double PlantbusSubproblemCW::GetSubproblemObjective() const {
		return model_.get(GRB_DoubleAttr_ObjVal);
	}

	double PlantbusSubproblemCW::GenerateAndVerifyCut() const {
		const double curr_lhs = GetCurrLhs();
		const double rhs = GetNewRhs();
		//check if the current solution will be cut off
        	if (curr_lhs > rhs || fabs(rhs - curr_lhs) < 1e-14) { //floating point comparison for equality
			throw IrrelevantCutException("");
		}
		return rhs;
	}

	double PlantbusSubproblemCW::get_plant_load() const {
		return plant_load_;
	}

	double PlantbusSubproblemCW::ComputeFeasDistance() const {
		return this->GetSubproblemObjective()*plant_load_
			*std::accumulate(std::begin(pow_sugg_), std::end(pow_sugg_), double{},
				[](double acc, double elem) -> double {return acc + std::abs(1 - elem); });
	}

	std::vector<double> PlantbusSubproblemCW::ComputeFeasSol() const {
		std::vector<double> result(num_timesteps_);
		double lambda{ this->GetSubproblemObjective() };
		double load{ plant_load_ };
		std::transform(std::begin(pow_sugg_), std::end(pow_sugg_), std::begin(result),
			[lambda, load](double master_p_plant) {return load * ((1 - lambda)*master_p_plant + lambda); });
		return result;
	}
	
	double PlantbusSubproblemCW::ComputeFeasSol(const unsigned key) const {
		double lambda{ this->GetSubproblemObjective() };
		return plant_load_ * ((1 - lambda)*pow_sugg_[key] + lambda);
	}

	std::vector<double> PlantbusSubproblemCW::get_pow_sugg() const {
		return pow_sugg_;
	}

	double PlantbusSubproblemCW::get_pow_sugg(const unsigned i) const {
		return pow_sugg_[i];
	}

	void PlantbusSubproblemCW::set_pow_sugg(const unsigned key, const double val) {
		pow_sugg_[key] = val;
	}

	void PlantbusSubproblemCW::set_pow_sugg(const std::vector<double>& in) {
		pow_sugg_ = in;
	}

	GRBModel& PlantbusSubproblemCW::GetModel() {
		return model_;
	}
} //namespace decoop
