#!/bin/bash

module purge
module load DEVELOP MATH
module load gcc/8
module load intel/2021.3.0 cmake gurobi

rm -rf build
mkdir build
cd build
cmake ..
make
