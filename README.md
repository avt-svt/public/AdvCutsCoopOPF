# cooperatOPF

This software solves the cooperative OPF problem while maintaining confidentiality of plant models.
Several strategies to generate cutting planes can be examined by appropriate choice of options.

#Bug reports, feature requests

Please send an email to Tim.Varelmann@avt.rwth-aachen.de. 


#Installation

0. Ensure that Gurobi is installed on your machine. If you want to create your own *.lp-files from the gms-models in the ModelFiles folder, you also need a working GAMS environment (General Algebraic Modeling System (GAMS), GAMS Development Corporation, https://www.gams.com/download/ ). The GAMS C++ api documentation provides a plethora of test cases.
1. Clone this repository to a location of your convenience.
2. Adjust the gurobi version used in FindGUROBI.cmake to match the version installed on your machine, e.g., 'NAMES gurobi gurobi95' or 'NAMES gurobi gurobi91'.

---Windows with Visual Studio---

3. Open the CMake GUI and

a) select this repository as source location
b) choose a suitable build folder, e.g., build/
c) select suitable option x64 in "optional platform for generator" (e.g., x64).

4. Configure, generate and open the project.
5. Select Solution Configuration "Release" in Visual Studio and select CoopBendersDec as Startup project.
6. (optional, if you want to use the VS Debugger) Specify the full path to your working directory as argument with right-click on the CoopBendersDec project -> Properties -> Debugging -> Command Arguments. Please specify your working directory with a trailing slash.
7. Build the solution.
8. Copy the *.lp-files and options files from the ExampleWorkspace to your workspace and update all options to reflect your paths, desired parameters, and output options.
9. Specify the full path to your working directory as command line argument when executing CoopBendersDec.exe.

If you want to create your own *.lp-files from the GAMS Models provided in the folder ModelFiles:

1. build the Convert project (a CMakeLists.txt is available in Convert/src)
2. specify your Convert working directory, where the options_convert.txt is located, and
3. update the options_convert.txt to reflect your paths and other preferences(an example can be found in Convert/ExampleWorkspace).

---Unix-based systems---

3. In a suitable build folder, e.g., build/, execute cmake with a flag to provide the Gurobi library path e.g., -DCMAKE_PREFIX_PATH=/Library/gurobi912/mac64/, then execute make.
4. Copy the *.lp-files and options files from the ExampleWorkspace to your workspace and update all options to reflect your paths, desired parameters, and output options.
5. Specify the full path to your working directory as command line argument when executing ./CoopBendersDec.


#Unit Tests

If the cmake flag coopTesting is set to TRUE, you can build the additional target CoopBendersTest with your preferred build routine.
Note that CoopBendersTest requires the absolute path to the unittests directory as command-line argument. Within this directory, you will need a directory named "Workspace". Once again, you can make a copy of the ExampleWorkspace (in the unittests folder) and adjust its options.
In particular, make sure to specify the absolute path to unittests/lp-files correctly in your unittests/Workspace/options_subproblem.txt.
