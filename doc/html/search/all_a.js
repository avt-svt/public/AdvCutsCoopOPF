var searchData=
[
  ['n_5fslack_5f',['n_slack_',['../classdecoop_1_1_plantbus_subproblem_l1.html#ae2ec2c96547283c1752defa3e52633bd',1,'decoop::PlantbusSubproblemL1']]],
  ['negativeoptimalitygapexception',['NegativeOptimalityGapException',['../classdecoop_1_1_negative_optimality_gap_exception.html',1,'decoop::NegativeOptimalityGapException'],['../classdecoop_1_1_negative_optimality_gap_exception.html#ac7204e2f72d4b4fb7db381d9bf4ee012',1,'decoop::NegativeOptimalityGapException::NegativeOptimalityGapException()']]],
  ['no_5fupdate_5f',['no_update_',['../classdecoop_1_1_plantbus_info.html#a0d9569d0c9a097dcb9faf642019f58c9',1,'decoop::PlantbusInfo']]],
  ['nominal_5flp',['nominal_lp',['../structdecoop_1_1_main_options.html#a5477588b7543f9a5dc71c2a495090c75',1,'decoop::MainOptions']]],
  ['num_5ftimesteps_5f',['num_timesteps_',['../classdecoop_1_1_coop_benders_algorithm.html#a1dd0c07223c7153d4054edf730b3ff8b',1,'decoop::CoopBendersAlgorithm::num_timesteps_()'],['../classdecoop_1_1_plantbus_subproblem_c_w.html#ad247ed38e4efce149d022550e0478c36',1,'decoop::PlantbusSubproblemCW::num_timesteps_()'],['../classdecoop_1_1_plantbus_subproblem_l1.html#ab3b35f0a6fea154a107e94af1636a656',1,'decoop::PlantbusSubproblemL1::num_timesteps_()']]]
];
