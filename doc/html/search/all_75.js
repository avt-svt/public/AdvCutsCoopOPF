var searchData=
[
  ['univ_5fstr',['univ_str',['../structdecoop_1_1_main_options.html#a3554b6bc902318c26081501c6e19f559',1,'decoop::MainOptions']]],
  ['updatecorepoint',['UpdateCorepoint',['../classdecoop_1_1_plantbus_subproblem_c_w.html#ae8279974b7182bbd519f8385924fac33',1,'decoop::PlantbusSubproblemCW::UpdateCorepoint(const unsigned key, const double new_corepoint)'],['../classdecoop_1_1_plantbus_subproblem_c_w.html#a69065f486b0ac29be6db34f5c784f43b',1,'decoop::PlantbusSubproblemCW::UpdateCorepoint(const std::vector&lt; double &gt; &amp;new_corepoints)']]],
  ['updatepowsugg',['UpdatePowSugg',['../classdecoop_1_1_coop_benders_algorithm.html#a2fda073032c513522de1419fcad7411b',1,'decoop::CoopBendersAlgorithm']]],
  ['use_5fint_5fprob',['use_int_prob',['../structdecoop_1_1_main_options.html#a8cb4d9f2195307716f83789852ed0341',1,'decoop::MainOptions']]]
];
