
positive Variable
lambda
;

Parameter masterPplant(t);
masterPplant(t) =0;

Parameter corePt;
corePt=1;

Equation masterSubCoupling(t);
masterSubCoupling(t).. (1-lambda)*masterPplant(t) + lambda*corePt =e=  totPower(t)/100 ;

Variable dumLambda;
Equation dummyObjective;
dummyObjective.. dumLambda =e= lambda;

model subProblemCW /Plantdummy, masterSubCoupling, dummyObjective/;
*model subProblemCW /Plantlinear, masterSubCoupling, dummyObjective/;