$onText
 dummy "plant model" for unit tests
$offText

Variable
totPower(t);

Equation
totPowercalc;
totPowercalc.. sum(t,totPower(t)) =e= 100*card(t);

totPower.lo(t) = 20;
totPower.up(t) = 190;

Model Plantdummy / totPowercalc /;
