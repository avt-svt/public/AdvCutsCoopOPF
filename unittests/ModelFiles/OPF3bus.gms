$title Optimal power flow for a three-bus system

$onText
For more details please refer to Chapter 6 (Gcode6.2), of the following book:
Soroudi, Alireza. Power System Optimization Modeling in GAMS. Springer, 2017.
--------------------------------------------------------------------------------
Model type: LP
--------------------------------------------------------------------------------
Contributed by
Dr. Alireza Soroudi
IEEE Senior Member
email: alireza.soroudi@gmail.com
We do request that publications derived from the use of the developed GAMS code
explicitly acknowledge that fact by citing
Soroudi, Alireza. Power System Optimization Modeling in GAMS. Springer, 2017.
DOI: doi.org/10.1007/978-3-319-62350-4
$offText

Set
   bus        / 1*3   /
   slack(bus) / 3     /
   Gen        / g1*g2 /
   t          / t1*t2 /;

Scalar Sbase / 100 /;

Alias (bus,node);

Parameter plantbus(bus);

Table GenData(Gen,*) 'generating units characteristics'
       b   pmin  pmax    RU    RD
   g1  10  0     65      1     0
   g2  11  0     100     1200  1200;
* -----------------------------------------------------

Set GBconect(bus,Gen) 'connectivity index of each generating unit to each bus' / 1.g1, 3.g2 /;

Table BusData(bus,*) 'demands of each bus in MW'
       Pd
   2  100;

Set conex 'bus connectivity matrix' / 1.2, 2.3, 1.3 /;
conex(bus,node)$(conex(node,bus)) = 1;

Table branch(bus,node,*) 'network technical characteristics'
        x     Limit
   1.2  0.2   100
   2.3  0.25  100
   1.3  0.4   100 ;

branch(bus,node,'x')$(branch(bus,node,'x')=0)         =   branch(node,bus,'x');
branch(bus,node,'Limit')$(branch(bus,node,'Limit')=0) =   branch(node,bus,'Limit');
branch(bus,node,'bij')$conex(bus,node)                = 1/branch(bus,node,'x');
*****************************************************

Variable OF, Pij(bus,node,t), Pg(Gen,t), delta(bus,t), Pplant(bus,t);
Equation const1, const2, const3, const4, const5;

***********************************************************************
const1(bus,node,t)$(conex(bus,node))..
   Pij(bus,node,t) =e= branch(bus,node,'bij')*(delta(bus,t) - delta(node,t));

const2(bus,t)..
   sum(Gen$GBconect(bus,Gen), Pg(Gen,t)) - BusData(bus,'pd')$(not plantbus(bus))/Sbase - Pplant(bus,t)$(plantbus(bus))/Sbase  =e= sum(node$conex(node,bus), Pij(bus,node,t));
*   sum(Gen$GBconect(bus,Gen), Pg(Gen,t)) - BusData(bus,'pd')$(not plantbus(bus)) - Pplant(bus,t)$(plantbus(bus))  =e= sum(node$conex(node,bus), Pij(bus,node,t));

const3..
   OF =g= sum((Gen,t),Pg(Gen,t)*GenData(Gen,'b')*Sbase);
*   OF =g= sum((Gen,t),Pg(Gen,t)*GenData(Gen,'b'));

const4(Gen,t)..
   Pg(Gen,t+1) - Pg(Gen,t) =l= GenData(Gen,'RU')/Sbase;
*   Pg(Gen,t+1) - Pg(Gen,t) =l= GenData(Gen,'RU');

const5(Gen,t)..
   Pg(Gen,t-1) - Pg(Gen,t) =l= GenData(Gen,'RD')/Sbase;
*   Pg(Gen,t-1) - Pg(Gen,t) =l= GenData(Gen,'RD');


Pg.lo(Gen,t) = GenData(Gen,'Pmin')/Sbase;
Pg.up(Gen,t) = GenData(Gen,'Pmax')/Sbase;
*Pg.up(Gen,t)$(ord(t)=1 and ord(Gen)=1) = 0.99;
*Pg.lo(Gen,t) = GenData(Gen,'Pmin');
*Pg.up(Gen,t) = GenData(Gen,'Pmax');

delta.up(bus,t)   = pi;
delta.lo(bus,t)   =-pi;
delta.fx(slack,t) = 0;

Pij.up(bus,node,t)$((conex(bus,node))) = 1*branch(bus,node,'Limit')/Sbase;
Pij.lo(bus,node,t)$((conex(bus,node))) =-1*branch(bus,node,'Limit')/Sbase;
*Pij.up(bus,node,t)$((conex(bus,node))) = 1*branch(bus,node,'Limit');
*Pij.lo(bus,node,t)$((conex(bus,node))) =-1*branch(bus,node,'Limit');

*Model loadflow / const1, const2, const3, const4, const5 /;
*Model loadflow / const2, const3, const4, const5 /;
Model loadflow / const1, const2, const3 /;
