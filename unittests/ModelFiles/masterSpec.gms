
*Variable
*v_OFscaled;
*Equation OFscaled;
*OFscaled..  v_OFscaled =e= OF/p_OF;


*parameters RHS(iter, cut, bus), cutcoeff(iter, cut, bus, t);
*RHS(iter, cut, bus)=0;
*cutcoeff(iter, cut, bus, t)=1;
*Equation
*feasCut(iter, cut, bus);
*feasCut(iter, cut, bus)$plantbus(bus).. sum(t, cutcoeff(iter, cut, bus, t) * pPlant(bus,t) ) =g= RHS(iter,cut,bus);
* cf Birge, Louveaux, Introduction to stochastic programming, p. 183, Eq. 1.3
* slave constraints in form: LinearCombi of SlaveVars = constants - LinearCombi of couplingVars;

*model masterOPF /OFscaled, loadflow, feasCut/;
model masterOPF /loadflow/;
masterOPF.solvelink=5;
Pplant.lo(bus,t)$plantbus(bus) = 20;
Pplant.lo(bus,t)$(plantbus(bus) AND ord(t)=1) = 20;
Pplant.up(bus,t)$plantbus(bus) = 190;
Pplant.up(bus,t)$(plantbus(bus) AND ord(t)=1) = 190;
