#Do not edit keywords!
LP_MASTER_FIXED		../lp_files/master.lp

MAXIMUM_ITERATIONS	1
PRIMARY_SUB_TYPE	0
SECONDARY_SUB_TYPE	0
#tau_init = factor*TotalCooperatingLoad[MW]
TAU_FACTOR		1
ACCEPTABLE_GAP		0.1
#safety factor when adjusting the feasibilty threshold
SAFETY_FACTOR		1.19
#accepted numerical value for subproblem feasibility
SUB_FEAS_VALUE		1e-7