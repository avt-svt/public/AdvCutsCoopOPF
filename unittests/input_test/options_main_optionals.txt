# Do not edit keywords!
LP_NOMINAL_OPERATION	lpnominal
LP_PRICE_CALCULATION	lpprice
LP_INTEGRATED		lpint
LP_SUB_DR		lpsubdr
NR_TIMESTEPS		2

#Plantbusses in format <plantbus> <fraction>, all in one line
PLANTBUSSES		2 1

USE_INTEGRATED		1

#Congestion indicator: n(ormal) or c(ongested) fi final option is 1 or 2
#or bus count if final option is 3
CONG_INDICATOR		n

OUTPUT_FILE		output
CSV_FILE		resmanual

# Optional inputs: comment out if not in use

# File name for case study csv file - cumulated selected information from all runs in this case study
CASE_STUDY_CSV		casestudycsv

# Determine what to write to above file
# 1: accepted optimality gap, number of iterations, time measurements, exploited cooperation potential
# 2: congestion indicator, nominal cost, integrated cost savings, disaggregated cost savings
# 3: number of iterations, time measurements, congestion indicator, accepted optimality gap, exploited cooperation potential
CASE_STUDY_INFO		1

MULTI_PURPOSE_STRING	univstring