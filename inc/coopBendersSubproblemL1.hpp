/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

 /**
 * \file coopBendersSubproblemL1.hpp
 * \brief Header file for PlantbusSubproblemL1 class.
 */

#pragma once

#include "gurobi_c++.h"
#include <vector>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <functional>
#include "exceptions.hpp"
#include "inputStructs.hpp"
#include "dynArrayRaiiWrapper.hpp"

namespace decoop {

	/**
	* \brief Provides an interface for solving L1 subproblems and adding cuts from such problems.
	*/
	class PlantbusSubproblemL1 {
		const SubproblemOptions options_;			///< Holds options specified by user in option file.
		GRBModel model_;							///< L1 subproblem model.
		DynArrayRAIIWrapper<GRBConstr> equations_;	///< All equations part of the #model_.
		DynArrayRAIIWrapper<GRBVar> variables_;		///< All variables part of the #model_.
		const std::string coupling_equation_name_;	///< Name of the equation coupling master power suggestion and feasible profile computed in subproblem. 
		std::vector<int> coupling_equation_;		///< Indices of the equations in #equations_ that represent the coupling equation (1 entry per timestep).
		std::vector<int> constraints_;				///< Indices of all entries in #equations_ that are not a coupling equation.
		std::vector<int> model_vars_;				///< Indices of all entries in #variables_ that are part of the plant model.
		std::vector<int> n_slack_;					///< Indices of all entries in #variables_ that are negative slack variables.
		std::vector<int> p_slack_;					///< Indices of all entries in #variables_ that are positive slack variables.

		const unsigned num_timesteps_;				///< Number of timesteps in the model.
		std::vector<double> pow_sugg_;				///< Power profile suggestion. Unit is multiples of flexible load.
		const double plant_load_;					///< Flexible load of this subproblem in MW.

		const unsigned long rhs_num_records_;		///< Number of terms used to compute scalar product on right hand side of a cut.

		/**
		* \brief Fills #coupling_equation_, #constraints_, #model_vars_, #n_slack_ and #p_slack_.
		* \return Sum of sizes of #constraints_ and #model_vars_.
		*/
		unsigned long InitObject();

	public:
		/**
		* \brief Constructor.
		* \param env Reference to the Gurobi environment in which to construct the #model_.
		* \param option_path String containing the path to the option file directory.
		* \param plant_load Flexible load of this subproblem in MW.
		* \param num_timesteps Number of timesteps in the model.
		*/
		PlantbusSubproblemL1(const GRBEnv& env, const std::string& option_path, const double plant_load, const unsigned num_timesteps);

		/**
		* \brief Solves the subproblem with the currently held power profile suggestion.
		*/
		void Solve();

		/**
		* \brief Returns status code of the #model_. See Gurobi reference manual for meaning of codes.
		* \return Model status code.
		*/
		int GetModelStatus() const;

		/**
		* \brief Computes right hand side of the cut generated from current power profile suggestion.
		* \return Right hand side of the cut.
		*/
		double GetNewRhs() const;

		/**
		* \brief Computes value of the left hand side of the generated cut when current power profile suggestion is inserted.
		* \return Value of left hand side of the cut.
		*/
		double GetCurrLhs() const;

		/**
		* \brief Computes coefficient of the left hand side of the cut for a given timestep.
		* \param key Timestep for which the coefficient is to be calculated.
		* \return Coefficient at given timestep.
		*/
		double GetNewCoeff(const unsigned key) const;

		/**
		* \brief Computes all coefficients of the left hand side of the cut.
		* \return Vector holding all coefficients.
		*/
		std::vector<double> GetNewCoeffs() const;

		/**
		* \brief Returns objective value of the #model_.
		* \details The objective value sums up the slacks that are required to find the minimum feasible modification of the suggested power profile. Unit is multiples of flexible load.
		* \return Objective value.
		*/
		double GetSubproblemObjective() const;

		/**
		* \brief Computes right hand side of the cut, verifies its correctness and ensures that current power profile suggestion is cut off.
		* \return Right hand side of the cut.
		* \throw IrrelevantCutException is thrown if the current power profile suggestion is not cut off.
		* \throw BadCutException is thrown if the correctness check fails.
		*/
		double GenerateAndVerifyCut() const;

		/**
		* \brief Returns flexible load of this subproblem in MW.
		* \return Flexible load of this subproblem.
		*/
		double get_plant_load() const;

		/**
		* \brief Computes distance between suggested power profile and the feasible profile found by the subproblem. Unit is MW.
		* \return Distance to feasible power profile.
		*/
		double ComputeFeasDistance() const;
		
		/**
		* \brief Computes the feasible power profile found by the subproblem. Unit is MW.
		* \return Feasible power profile.
		*/
		std::vector<double> ComputeFeasSol() const;
		
		/**
		* \brief Computes entry of the feasible power profile at given timestep found by the subproblem. Unit is MW.
		* \param key Timestep of the feasible power entry.
		* \return Feasible power entry at given timestep.
		*/
		double ComputeFeasSol(const unsigned key) const;

		/**
		* \brief Returns values of negative slack variables.
		* \return Vector containing values of negative slack variables.
		*/
		std::vector<double> GetNSlackValues() const;

		/**
		* \brief Returns values of positive slack variables.
		* \return Vector containing values of positive slack variables.
		*/
		std::vector<double> GetPSlackValues() const;

		/**
		* \brief Returns power profile suggestion. Unit is multiples of flexible load.
		* \return Power profile suggestion.
		*/
		std::vector<double> get_pow_sugg() const;

		/**
		* \brief Returns entry of the power profile suggestion at a given timestep. Unit is multiples of flexible load.
		* \param i Timestep of power suggestion entry.
		* \return Power suggestion at given timestep.
		*/
		double get_pow_sugg(const unsigned i) const;

		/**
		* \brief Sets power profile suggestion.
		* \param in Reference to vector containing the new power profile suggestion.
		*/
		void set_pow_sugg(const std::vector<double>& in);
		
		/**
		* \brief Sets power suggestion at single timestep.
		* \param key Timestep to set power suggestion at.
		* \param val Power suggestion.
		*/
		void set_pow_sugg(const unsigned key, const double val);

		/**
		* \brief Returns reference to #model_ object.
		* \return Reference to model.
		*/
		GRBModel& GetModel();
	};
} //namespace decoop
