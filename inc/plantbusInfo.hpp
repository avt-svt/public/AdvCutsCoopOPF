/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

/**
* \file plantbusInfo.hpp
* \brief Header file for PlantbusInfo class.
*/

#pragma once

namespace decoop {

	/**
	* \brief Contains information on subproblem status that is relevant for CoopBendersAlgorithm class but not necessary to be known in the subproblem classes PlantbusSubproblemCW and PlantbusSubproblemL1.
	*/
	class PlantbusInfo {
		bool no_update_;	///< Indicates whether or not new power profiles should be suggested for a plantbus.

	public:
		/**
		* \brief Default constructor.
		*/
		PlantbusInfo();

		/**
		* \brief no_update_ indicates whether or not new power profiles should be suggested for a plantbus.
		* \return Value of no_update_.
		*/
		bool get_no_update() const;

		/**
		* \brief no_update_ indicates whether or not new power profiles should be suggested for a plantbus.
		* \param value Value to be set.
		*/
		void set_no_update(const bool value);
	};
} //namespace decoop