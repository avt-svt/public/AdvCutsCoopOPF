/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

/**
* \file coopBendersAlgorithm.hpp
* \brief Header file for CoopBendersAlgorithm and PriceProblem classes.
*/

#pragma once

#include "gurobi_c++.h"
#include <fstream>
#include <vector>
#include <chrono>
#include "coopBendersSubproblemCW.hpp"
#include "coopBendersSubproblemL1.hpp"
#include "inputStructs.hpp"
#include "plantbusInfo.hpp"
#include <unordered_map>
#include <variant>
#include <algorithm>

namespace decoop {

	/**
	* \brief Implements the decomposition algorithm.
	*/
	class CoopBendersAlgorithm {

		using SubVariant = std::variant<PlantbusSubproblemCW, PlantbusSubproblemL1>;				///< Variant that holds either subproblem type.
		using SubIterator = std::vector<std::variant<PlantbusSubproblemCW, PlantbusSubproblemL1>>::iterator;	///< Iterator over vector holding subproblem variants.
		using TimePoint = std::chrono::time_point<std::chrono::steady_clock>;                                   ///< Time point for measuring exection times.
	
		/**
		* \brief Used to determine what type of subproblem to create in CreateSubproblemObjects.
		*/
		enum class SubproblemType { kCWSubproblem, kL1Subproblem };
		
		std::vector<GRBEnv> environments_;					///< Vector holding all GRBEnv objects. One entry is needed per thread, so generally, this will have one entry per subproblem.
		const AlgorithmOptions options_;					///< Holds options specified by user in option file.
		GRBModel fixed_model_;							///< OPF model that fixes flexible load profiles to compute an upper bound on the cost.
		GRBModel master_model_;							///< OPF model that suggests power profiles for subproblems.

		const std::vector<std::pair<int, double>> plantbusses_;			///< Contains bus numbers of cooperating busses as well as flexible load fractions.
		std::vector<std::vector<GRBVar>> fx_p_plant_var_;			///< Power profile variables in the fixed OPF model. Outer vector is plantbusses, inner is timesteps.
		std::vector<std::vector<GRBVar>> master_p_plant_var_;			///< Power profile variables in the master OPF model. Outer vector is plantbusses, inner is timesteps.

		//sub problem
		std::vector<SubVariant> sub_prob_;					///< Vector holding the subproblem objects, each entry can hold either a CW or L1 problem. Holds all primary and secondary problems.
		std::vector<PlantbusInfo> pb_info_;					///< One entry per plantbus, since information in SubproblemInfo is applicable to all subproblems on a plantbus regardless of their type.
		std::vector<std::vector<double>> last_primary_feas_sol_;		///< Holds last iteration's feasible power profiles as determined by subproblems. Used for generating new corepoints for secondary cuts.
		
		SubIterator cw_begin_;							///< Iterator to beginning of primary CW problems in sub_prob_ vector.
		SubIterator cw_end_;							///< Iterator to end of primary CW problems in sub_prob_ vector.
		SubIterator l1_begin_;							///< Iterator to beginning of primary L1 problems in sub_prob_ vector.
		SubIterator l1_end_;							///< Iterator to end of primary L1 problems in sub_prob_ vector.
		SubIterator feas_begin_;						///< Iterator to beginning of problems used for computing feasibility distance in sub_prob_ vector.
		SubIterator feas_end_;							///< Iterator to end of problems used for computing feasibility distance in sub_prob_ vector.
		SubIterator secondary_begin_;						///< Iterator to beginning of secondary problems in sub_prob_ vector.
		SubIterator secondary_end_;						///< Iterator to end of secondary problems in sub_prob_ vector.

		//output
		std::ofstream& out_file_;						///< Reference to output txt file object, for writing iteration updates.
		const unsigned num_timesteps_;						///< Number of timesteps in the model.
		int final_iters_;							///< Final number of iterations performed.
		double tau_;								///< Current feasibility threshold.
		const double tot_flex_power_;						///< Total flexible load on all busses.
		double detectable_opt_gap_;						///< Detectable optimality gap.
		double accepted_cost_;							///< Accepted OPF cost. Cost either satisfies user-defined optimality gap, or, if not converged, cost of last suggestion, which is lower bound for optimal cost.
		double cumul_feas_dist_;						///< Cumulated distance to feasibility of all subproblems. Unit is MW.
		double coop_sol_cost_;							///< Cost of the cooperative (i.e. integrated) solution.

		std::chrono::duration<double> time_master_stage_;			///< Total master stage time.
		std::chrono::duration<double> time_feas_pt_stage_;			///< Total time finding feasible points.
		std::chrono::duration<double> time_sub_stage_;				///< Total subproblem stage time.
		TimePoint mas_t_;                               	                ///< Time at which a master stage is started.
		TimePoint fpt_t_;                                          		///< Time at which a feasible point finding stage is started.
		TimePoint sub_t_;                                       	        ///< Time at which a subproblem stage is started.

		bool converged_;							///< Indicates whether or not the algorithm has converged.
		const bool coop_sol_available_;						///< Indicates whether or not a cooperative (i.e. integrated) solution is available.

		/**
		* \brief Sets up members related to master and fixed OPF problems.
		*/
		void SetUpMasterAndFixed();

		/**
		* \brief Sets up members related to subproblems.
		* \param workspace_path Path to option file for subproblems.
		* \param bus_data Nominal power draw of all busses with loads, in MW.
		* \return Total flexible load of all cooperating busses.
		*/
		double SetUpSub(const std::string& workspace_path, const std::unordered_map<int, double>& bus_data);

		/**
		* \brief Solves all subproblems and generates cuts.
		* \param iteration Current iteration number.
		*/
		void SubStage(const int iteration);

		/**
		* \brief Computes an upper bound to OPF cost 
		* \details Solves a non-cooperative OPF problem with the most recent feasible solution. Called, if feasibility distance is below threshold #tau_.
		* \param iteration Current iteration number.
		* \throw NegativeOptimalityGapException is thrown if a computed optimality gap (either detectable or real, if applicable) is below a small negative threshold.
		* \throw WrongOptimalityGapException is thrown if the computed detectable gap is greater than the computed real gap. 
		*/
		void FeasPtStage(const int iteration);

		/**
		* \brief Solves master OPF problem and makes a new power profile suggestion.
		* \param iteration Current iteration number.
		*/
		void MasterStage(const int iteration);

		/**
		* \brief Solves specified subproblems in parallel.
		* \details Only the problems specified by ranges between each input iterator pair are solved.
		* \param iterators Pairs of iterators specifiying range of problems to solve.
		*/
		void SolveParallel(const std::vector<std::pair<SubIterator, SubIterator>>& iterators);

		/**
		* \brief Solves specified subproblems serially.
		* \details Only the problems specified by ranges between each input iterator pair are solved.
		* \param iterators Pairs of iterators specifiying range of problems to solve.
		*/
		void SolveSerial(const std::vector<std::pair<SubIterator, SubIterator>>& iterators);

		/**
		* \brief Generates cuts for specified subproblems serially.
		* \details Cuts are only generated for problems specified by ranges between each input iterator pair.
		* \param iteration Current iteration number.
		* \param iterators Pairs of iterators specifiying range of problems for which cuts will be generated.
		*/
		void GenerateCut(const int iteration, const std::vector<std::pair<SubIterator, SubIterator>>& iterators);

		/**
		* \brief Generates cuts for specified subproblems in parallel.
		* \details Cuts are only generated for problems specified by ranges between each input iterator pair.
		* \param iteration Current iteration number.
		* \param iterators Pairs of iterators specifiying range of problems for which cuts will be generated.
		*/
		void GenerateCutParallel(const int iteration, const std::vector<std::pair<SubIterator, SubIterator>>& iterators);
		
		/**
		* \brief Solves specified subproblems and generates a cut for each one in parallel.
		* \details Only the problems specified by ranges between each input iterator pair are solved and cuts are only generated for these problems.
		* \param iteration Current iteration number.
		* \param iterators Pairs of iterators specifiying range of problems to solve and for which cuts will be generated.
		*/
		void SolveAndGenerateCutParallel(const int iteration, const std::vector<std::pair<SubIterator, SubIterator>>& iterators);
		
		/**
		* \brief Updates power suggestion in subproblems based on last master solution.
		*/
		void UpdatePowSugg();

		/**
		* \brief Fills vectors #sub_prob_ and #pb_info_ and computes total flexible load.
		* \param workspace_path Path to subproblem option file.
		* \param bus_data Nominal power draw of all busses with loads, in MW.
		* \param sp_type Whether to add CW or L1 subprobles in this call.
		* \param add_environments Whether to add additional GRBEnv objects to vector #environments_.
		* \return Total flexible load of all cooperating busses.
		*/
		double CreateSubproblemObjects(const std::string& workspace_path, const std::unordered_map<int, double>& bus_data, const SubproblemType sp_type, const bool add_environments);

		/**
		* \brief Writes objective value and feasibility distance for each subproblem to output file.
		* \param iteration Current iteration number.
		*/
		void PrintSubproblemObjectivesAndDistances(const int iteration);

	public:
		/**
		* \brief Constructor.
		* \param out Reference to std::ofstream object for writing to output txt file.
		* \param workspace_path String containing the path to the option file directory.
		* \param timesteps Number of timesteps in the model.
		* \param plantbusses Contains bus numbers of cooperating busses as well as flexible load fractions.
		* \param bus_data Nominal power draw of all busses with loads, in MW.
		* \param coop_sol_available Indicates whether or not a cooperative (i.e. integrated model) solution is available.
		* \param coop_sol_cost Cost of the cooperative (integrated) solution.
		*/
		CoopBendersAlgorithm(
			std::ofstream& out, const std::string& workspace_path, unsigned timesteps,
			const std::vector<std::pair<int, double>>& plantbusses, const std::unordered_map<int, double>& bus_data,
			const bool coop_sol_available = false, const double coop_sol_cost = 0
		);

		/**
		* \brief Runs the algorithm.
		*/
		void Iterate();

		/**
		* \brief Returns power profiles for all cooperating busses suggested by the master problem.
		* \details Power profiles suggested by master problem are returned in a vector of vectors.
		*		   The outer vector contains an entry for each plantbus. The inner vector contains an entry for each timestep on that plantbus.
		*		   The order of plantbusses follows the order in which they are specified in the option file.
		*		   Each entry of the inner vectors is a GRBVar, so further information on the variables can be queried using the appropirate Gurobi methods.
		* \return Master power profile suggestion.
		*/
		std::vector<std::vector<GRBVar>> get_master_p_plant_var();

		/**
		* \brief Returns plantbusses and flexible load fractions.
		* \return Plantbusses and flexible load fraction on each plantbus. First entry of each pair is the plantbus number, second entry is its flexible load fraction.
		*/
		std::vector<std::pair<int, double>> GetPlantbus() const;

		/**
		* \brief Returns number of timesteps in the model.
		* \return Number of timesteps.
		*/
		unsigned get_num_timesteps() const;

		/**
		* \brief Returns reference to model with fixed cooperating loads.
		* \return Reference to fixed model.
		*/
		GRBModel& get_fixed_model();

		/**
		* \brief Returns reference to vector containing all subproblems.
		* \details The subproblems are stored in the following order, if applicable to selected cutting strategy:
		*		   1: primary CW problems. 2: primary L1 problems. 3: secondary CW problems.
		*		   The order of problems within a section always follows the order in which the plantbusses are specified in the option file.
		* \return Reference to vector containing all subproblems.
		*/
		std::vector<SubVariant>& ProvideSubproblemVec();

		/**
		* \brief Returns vector containing further information on each plantbus.
		* \details This vector always has one entry per plantbus, regardless of cutting strategy, since the information contained in a SubproblemInfo object is applicable to all subproblems on a plantbus, regardless of their type.
		*		   The order of objects follows the order in which the plantbusses are specified in the option file.
		* \return Vector containing plantbus information.
		*/
		std::vector<PlantbusInfo> get_pb_info();

		/**
		* \brief Returns the power profiles set in the fixed model.
		* \details Power profiles in the fixed model are returned in a vector of vectors.
		*		   These power profiles are certainly feasible.
		*		   The outer vector contains an entry for each plantbus. The inner vector contains an entry for each timestep on that plantbus.
		*		   The order of plantbusses follows the order in which they are specified in the option file.
		* \return Feasible fixed power profiles.
		*/
		std::vector<std::vector<double>> GetFixedPPlant() const;

		/**
		* \brief Returns whether or not the algorithm converged within the allocated maximum number of iterations.
		* \return Bool indicating whether convergence was achieved.
		*/
		bool get_converged() const;

		/**
		* \brief Returns total time spent in the master stage of the algorithm.
		* \details Unit is seconds.
		* \return Time spent in master stage.
		*/
		std::chrono::duration<double> get_time_master_stage() const;

		/**
		* \brief Returns total time spent in the subproblem stage of the algorithm.
		* \details Unit is seconds.
		* \return Time spent in subproblem stage.
		*/
		std::chrono::duration<double> get_time_sub_stage() const;

		/**
		* \brief Returns total time spent finding feasible points.
		* \details Unit is seconds.
		* \return Time spent finding feasible points.
		*/
		std::chrono::duration<double> get_time_feas_pt_stage() const;

		/**
		* \brief Returns total time spent solving subproblems within the subproblem stage.
		* \details Unit is seconds.
		* \return Time spent solving subproblems.
		*/
		std::chrono::duration<double> get_time_sub_solving() const;

		/**
		* \brief Returns total time spent generating cuts within the subproblem stage.
		* \details Unit is seconds.
		* \return Time spent generating cuts.
		*/
		std::chrono::duration<double> get_time_sub_cutgen() const;

		/**
		* \brief Returns total flexible cooperating load in MW.
		* \return Total flexible load.
		*/
		double get_tot_flex_power() const;

		/**
		* \brief Returns current tau threshold.
		* \return Tau threshold.
		*/
		double get_tau() const;

		/**
		* \brief Returns total number of iterations performed.
		* \return Total number of iterations.
		*/
		int get_final_iters() const;

		/**
		* \brief Returns optimality gap at solution.
		* \return Optimality gap.
		*/
		double get_detectable_opt_gap() const;

		/**
		* \brief Returns accepted cost of the OPF problem at found solution.
		* \details If the algorithm converged, the cost will be optimal up to the user-defined optimality gap.
		*          If not, then this is the cost of the last iteration's suggestion, which is a lower bound for the optimal cost.
		* \returns Accepted cost.
		*/
		double get_accepted_cost() const;

		/**
		* \brief Returns initial tau factor as specified in option file.
		* \details This tau factor is multiplied with the total flexible load to compute the initial tau threshold.
		* \return Initial tau factor.
		*/
		double GetInitialTau() const;

		/**
		* \brief Returns acceptable optimality gap as specified in option file.
		* \return Acceptable optimality gap.
		*/
		double GetAcceptableGap() const;

		/**
		* \brief Writes all cuts added by subproblems that are active in the current solution to output txt file.
		*/
		void PrintActiveCuts();
	};

	/**
	* \brief Class used to compute prices at each bus and congestion cost.
	*/
	class PriceProblem {
		GRBEnv price_env_;										///< Environment for model.
		GRBModel loadflow_price_;								///< Model used to compute prices.
		DynArrayRAIIWrapper<GRBVar> price_vars_;				///< All variables contained in the model #loadflow_price_.
		std::vector<std::pair<int, double>> plantbusses_;		///< Contains bus numbers of cooperating busses as well as flexible load fractions.
		unsigned timesteps_;									///< Number of timesteps in the model.
		double sbase_;											///< Per-unit base.

	public:
		/**
		* \brief Constructor.
		* \param lp_file Name of the LP file from which to build the #loadflow_price_ model.
		* \param plantbusses Contains bus numbers of cooperating busses as well as flexible load fractions.
		* \param timesteps Number of timesteps in the model.
		* \param sbase Per-unit base.
		*/
		PriceProblem(const std::string& lp_file, const std::vector<std::pair<int, double>>& plantbusses, const unsigned timesteps, const double sbase);

		/**
		* \brief Computes electricity prices as locational marginal cost at each bus.
		* \param bus_data Nominal power draw of all busses with loads, in MW.
		* \param wd power supply pattern of wind farms and non-cooperative load pattern, both over all time steps
		* \param loadflow_in Reference to solved OPF model whose power flow is to be used for computations.
		*/
		void ComputePrices(const std::unordered_map<int, double>& bus_data, const std::unordered_map<std::string, double>& wd, GRBModel& loadflow_in);

		/**
		* \brief Gets previously computed electricity prices at each bus.
		* \details The outer vector contains an entry for each plantbus. The inner vector contains an entry for each timestep on that plantbus.
		*		   The order of plantbusses follows the order in which they are specified in the option file.
		* \return Vector containing vectors of prices on each bus.
		*/
		std::vector<std::vector<double>> GetPrices();

		/**
		* \brief Computes congestion cost and writes congested lines to specified file.
		* \param out_file Reference to file object to write output to.
		* \param setup Describes what setup is being run (eg nominal, integrated, disaggregated).
		* \return Congestion cost.
		*/
		double ComputeAndPrintCongestionCost(std::ofstream& out_file, std::string&& setup);
	};

} //namespace decoop
