/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

 /**
 * \file exceptions.hpp
 * \brief Header file for custom exception classes.
 */

#include <exception>
#include <string>
#pragma once

namespace decoop {

	/**
	* \brief Exception class for incorrect cuts.
	*/
	class BadCutException : public std::runtime_error {
	public:
		/**
		* \brief Constructor.
		* \param msg Exception message.
		*/
		explicit BadCutException(const std::string& msg) : std::runtime_error("BadCutException: " + msg) {}
	};

	/**
	* \brief Execption class for irrelevant cuts.
	*/
	class IrrelevantCutException : public std::runtime_error {
	public:
		/**
		* \brief Constructor.
		* \param msg Exception message.
		*/
		explicit IrrelevantCutException(const std::string& msg) : std::runtime_error("IrrelevantCutException: " + msg) {}
	};

	/**
	* \brief Exception class for negative optimality gaps.
	*/
	class NegativeOptimalityGapException : public std::runtime_error {
	public:
		/**
		* \brief Constructor.
		* param msg Exception message.
		*/
		explicit NegativeOptimalityGapException(const std::string& msg) : std::runtime_error("NegativeOptimalityGapException: " + msg) {}
	};

	/**
	* \brief Exception class for non-plausible optimality gaps.
	*/
	class WrongOptimalityGapException : public std::runtime_error {
	public:
		/**
		* \brief Constructor.
		* \param msg Exception message.
		*/
		explicit WrongOptimalityGapException(const std::string& msg) : std::runtime_error("WrongOptimalityGapException: " + msg) {}
	};
} //namespace decoop