/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/
#pragma once

/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

 /**
 * \file inputStructs.hpp
 * \brief Header file for structs used to process user input from option files.
 */

#include <fstream>
#include <bitset>
#include <cstdlib>
#include <vector>
#include <sstream>
#include <iostream>
#include <unordered_map>

namespace decoop {

	/**
	* \brief Processes user input from file options_main.txt
	*/
	struct MainOptions {
		std::string nominal_lp;						///< LP file name for nominal operation.
		std::string price_lp;						///< LP file name for computing prices.
		std::string integrated_lp;					///< LP file name for the integrated problem.
		std::string sub_dr_lp;						///< LP file name for the demand response problem.
		unsigned timesteps;						///< Number of timesteps in the model.
		std::string univ_str;						///< Multi-purpose string for output.
		std::vector<std::pair<int, double>> plantbusses;		///< Contains bus numbers of cooperating busses as well as flexible load fractions.
		char cong_indicator;						///< Indicates whether the grid is in a congested state.
		bool use_int_prob;						///< Indicates whether the integrated problem should be solved.
		std::string output_name;					///< File name for output txt file.
		std::string csv_name;						///< File name for output csv file.
		//Optional
		std::string spec_name;						///< File name for case study csv file which contains cumulated selected information.
		int spec_func;							///< Determines which information to write to case study csv file.

		/**
		* \brief Overloaded operator to read in the option file.
		* \param in Reference to std::fstream object containing the option file.
		* \param opt Reference to MainOptions object to store input.
		*/
		friend std::fstream& operator>> (std::fstream& in, MainOptions& opt);

		/**
		* \brief Constructs a MainOptions object given a path to the option file. Allows to specify a different file name from the default options_main.txt.
		* \param path Path to the option file. Can, but does not have to, include the file name.
		* \param path_includes_name Indicates whether path includes name of the option file. Default value is false.
		*/
		explicit MainOptions(const std::string& path, const bool path_includes_name = false);
	};

	/**
	* \brief Processes user input from file options_algorithm.txt.
	*/
	struct AlgorithmOptions {
		std::string master_lp_file;					///< LP file name for master problem.
		unsigned max_iters;						///< Maximum number of iterations to perform.
		unsigned primary_sp_type;					///< Primary cut strategy.
		unsigned secondary_sp_type;					///< Secondary cut strategy.
		double initial_tau_factor;					///< Factor to multiply with cooperating load to determine initial threshoold tau.
		double accept_gap;						///< Acceptable optimality gap.
		double rho;							///< Safety factor when adjusting the feasibility threshold.
		double eps;							///< Numerical value for subproblem objective to consider the suggested power profile feasible.
		double mu;							///< Convex combination factor when generating new corepoints for secondary cuts.

		/**
		* \brief Overloaded operator to read in the option file.
		* \param in Reference to std::fstream object containing the option file.
		* \param opt Reference to AlgorithmOptions object to store input.
		*/
		friend std::fstream& operator>> (std::fstream& in, AlgorithmOptions& opt);

		/**
		* \brief Constructs an AlgorithmOptions object given a path to the option file. Allows to specify a different file name from the default options_algorithm.txt.
		* \param path Path to the option file. Can, but does not have to, include the file name.
		* \param path_includes_name Indicates whether path includes name of the option file. Default value is false.
		*/
		explicit AlgorithmOptions(const std::string& path, const bool path_includes_name = false);
	};

	/**
	* \brief Processes user input from file options_subproblem.txt.
	*/
	struct SubproblemOptions {
		std::string workspace_dir;					///< (Absolute) path to directory containing the LP files for subproblem models.
		std::string lp_file_cw;						///< LP file name for the CW-subproblem.
		std::string lp_file_l1;						///< LP file name for the L1-subproblem.

		/**
		* \brief Overloaded operator to read in the option file.
		* \param in Reference to std::fstream object containing the option file.
		* \param opt Reference to SubproblemOptions object to store input.
		*/
		friend std::fstream& operator>> (std::fstream& in, SubproblemOptions& opt);
		
		/**
		* \brief Constructs a SubproblemOptions object given a path to the option file. Allows to specify a different file name from the default options_subproblem.txt.
		* \param path Path to the option file. Can, but does not have to, include the file name.
		* \param path_includes_name Indicates whether path includes name of the option file. Default value is false.
		*/
		explicit SubproblemOptions(const std::string& path, const bool path_includes_name = false);
	};

	/**
	* \brief Skips lines containing comments when reading an option file.
	* \param input Reference to std::fstream object containing the option file.
	*/
	void ParseComments(std::fstream& input);

} //namespace decoop
