/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

/**
* \file coopBendersSubproblemCW.hpp
* \brief Header file for PlantbusSubproblemCW class.
*/

#pragma once

#include "gurobi_c++.h"
#include <vector>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <functional>
#include "exceptions.hpp"
#include "inputStructs.hpp"
#include "dynArrayRaiiWrapper.hpp"

namespace decoop {

	/**
	* \brief Provides an interface for solving CW subproblems and adding cuts from such problems.
	*/
	class PlantbusSubproblemCW {
		const SubproblemOptions options_;			///< Holds options specified by user in option file.
		GRBModel model_;							///< CW subproblem model.
		DynArrayRAIIWrapper<GRBConstr> equations_;	///< All equations part of the #model_.
		DynArrayRAIIWrapper<GRBVar> variables_;		///< All variables part of the #model_.
		const std::string coupling_equation_name_;	///< Name of the equation coupling master power suggestion and feasible profile computed in subproblem.
		std::vector<int> coupling_equation_; 		///< Indices of the equations in #equations_ that represent the coupling equation (1 entry per timestep).
		std::vector<int> constraints_; 				///< Indices of all entries in #equations_ that are not a coupling equation.
		std::vector<int> model_vars_; 				///< Indices of all entries in #variables_ that are part of the plant model.

		const unsigned num_timesteps_;				///< Number of timesteps in the model.
		std::vector<double> pow_sugg_;				///< Power profile suggestion. Unit is multiples of flexible load.
		std::vector<double> corepoint_;				///< Corepoint used to generate cuts. Unit is multiples of flexible load.
		const double plant_load_;					///< Flexible load of this subproblem in MW.

		const unsigned long rhs_num_records_;		///< Number of terms used to compute scalar product on right hand side of a cut.

		/**
		* \brief Fills #coupling_equation_, #constraints_, #model_vars_ and #lambda_index_.
		* \return Sum of sizes of #constraints_ and #model_vars_.
		*/
		unsigned long InitObject();

		int lambda_index_;							///< Index of the objective variable in #variables_.

	public:
		/**
		* \brief Constructor.
		* \param env Reference to the Gurobi environment in which to construct the #model_.
		* \param option_path String containing the path to the option file directory.
		* \param plant_load Flexible load of this subproblem in MW.
		* \param num_timesteps Number of timesteps in the model. 
		*/
		PlantbusSubproblemCW(const GRBEnv& env, const std::string& option_path, const double plant_load, const unsigned num_timesteps);

		/**
		* \brief Solves the subproblem with the currently held power profile suggestion and corepoint.
		*/
		void Solve();

		/**
		* \brief Update a single entry in the #corepoint_ vector.
		* \param key Index to update.
		* \param new_corepoint New value.
		*/
		void UpdateCorepoint(const unsigned key, const double new_corepoint);

		/**
		* \brief Update the #corepoint_ vector.
		* \param new_corepoints Reference to vector holding the new values.
		*/
		void UpdateCorepoint(const std::vector<double>& new_corepoints);

		/**
		* \brief Returns status code of the #model_. See Gurobi reference manual for meaning of codes.
		* \return Model status code.
		*/
		int GetModelStatus() const;

		/**
		* \brief Computes right hand side of the cut generated from current power profile suggestion.
		* \return Right hand side of the cut.
		*/
		double GetNewRhs() const;

		/**
		* \brief Computes value of the left hand side of the generated cut when current power profile suggestion is inserted.
		* \return Value of left hand side of the cut.
		*/
		double GetCurrLhs() const;

		/**
		* \brief Computes coefficient of the left hand side of the cut for a given timestep.
		* \param key Timestep for which the coefficient is to be calculated.
		* \return Coefficient at given timestep.
		*/
		double GetNewCoeff(const unsigned key) const;

		/**
		* \brief Computes all coefficients of the left hand side of the cut.
		* \return Vector holding all coefficients.
		*/
		std::vector<double> GetNewCoeffs() const;

		/**
		* \brief Returns objective value of the #model_.
		* \details Objective value should be between 0 (power suggestion is feasible) and 1.
		*		   The value indicates the factor of the corepoint in a feasible convex combination of corepoint and power suggestion.
		*		   p_feas = lambda * corepoint + (1-lambda) * p_sugg.
		* \return Objective value.
		*/
		double GetSubproblemObjective() const;

		/**
		* \brief Computes right hand side of the cut and ensures that current power profile suggestion is cut off.
		* \return Right hand side of the cut.
		* \throw IrrelevantCutException is thrown if the current power profile suggestion is not cut off.
		*/
		double GenerateAndVerifyCut() const;

		/**
		* \brief Returns flexible load of this subproblem in MW.
		* \return Flexible load of this subproblem.
		*/
		double get_plant_load() const;

		/**
		* \brief Computes distance between suggested power profile and the feasible profile found by the subproblem. Unit is MW.
		* \return Distance to feasible power profile.
		*/
		double ComputeFeasDistance() const;
		
		/**
		* \brief Computes the feasible power profile found by the subproblem. Unit is MW.
		* \return Feasible power profile.
		*/
		std::vector<double> ComputeFeasSol() const;
		
		/**
		* \brief Computes entry of the feasible power profile at given timestep found by the subproblem. Unit is MW.
		* \param key Timestep of the feasible power entry.
		* \return Feasible power entry at given timestep.
		*/
		double ComputeFeasSol(const unsigned key) const;

		/**
		* \brief Returns power profile suggestion. Unit is multiples of flexible load.
		* \return Power profile suggestion.
		*/
		std::vector<double> get_pow_sugg() const;

		/**
		* \brief Returns entry of the power profile suggestion at a given timestep. Unit is multiples of flexible load.
		* \param i Timestep of power suggestion entry.
		* \return Power suggestion at given timestep.
		*/
		double get_pow_sugg(const unsigned i) const;
		
		/**
		* \brief Sets power profile suggestion.
		* \param in Reference to vector containing the new power profile suggestion.
		*/
		void set_pow_sugg(const std::vector<double>& in);
		
		/**
		* \brief Sets power suggestion at single timestep.
		* \param key Timestep to set power suggestion at.
		* \param val Power suggestion.
		*/
		void set_pow_sugg(const unsigned key, const double val);

		/**
		* \brief Returns reference to #model_ object.
		* \return Reference to model.
		*/
		GRBModel& GetModel();
	};
} //namespace decoop
