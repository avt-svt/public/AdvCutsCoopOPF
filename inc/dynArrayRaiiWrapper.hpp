/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

 /**
 * \file dynArrayRaiiWrapper.hpp
 * \brief Header file for DynArrayRAIIWrapper class.
 */

#pragma once
#include <memory>

namespace decoop {

	/**
	* \brief Templated wrapper class to provide iterators and element access to a dynamic array, as well as simpler memory management.
	* \tparam T Type of the elements in the dynamic array to be wrapped.
	*/
	template<typename T>
	class DynArrayRAIIWrapper {
	public:
		using iterator = T * ;				///< Define iterator.
		using const_iterator = const T*;	///< Define const_iterator.

		/**
		* \brief Constructor.
		* \param dyn_arr The dynamic array to be wrapped.
		* \param size Number of elements in the dynamic array.
		*/
		DynArrayRAIIWrapper(T* dyn_arr, size_t size);

		/**
		* \brief Default destructor.
		*/
		~DynArrayRAIIWrapper() = default;

		/**
		* \brief Copy constructor is deleted.
		*/
		DynArrayRAIIWrapper(DynArrayRAIIWrapper const& other) = delete;

		/**
		* \brief Copy assignment operator is deleted.
		*/
		DynArrayRAIIWrapper& operator=(DynArrayRAIIWrapper const& other) = delete;
		
		/**
		* \brief Default move constructor.
		*/
		DynArrayRAIIWrapper(DynArrayRAIIWrapper&& other) = default;
		
		/**
		* \brief Default move assignment operator.
		*/
		DynArrayRAIIWrapper& operator=(DynArrayRAIIWrapper&& other) = default;
		
		/**
		* \brief Returns number of elements of the wrapped array.
		* \return Number of elements of the wrapped array.
		*/
		size_t size() const noexcept;

		/**
		* \brief Element access operator by index.
		* \param index Index to be accessed.
		* \return Reference to element at specified index.
		*/
		T& operator[](size_t index) noexcept;

		/**
		* \brief Constant element access operator by index.
		* \param index Index to be accessed.
		* \return Reference to constant element at specified index.
		*/
		const T& operator[](size_t index) const noexcept;

		/**
		* \brief Returns an #iterator to the beginning of the array.
		* \return Iterator to first element.
		*/
		iterator begin() noexcept;

		/**
		* \brief Returns a #const_iterator to the beginning of the array.
		* \return Iterator to constant first element.
		*/
		const_iterator begin() const noexcept;

		/**
		* \brief Returns a #const_iterator to the beginning of the array.
		* \return Iterator to constant first element.
		*/
		const_iterator cbegin() const noexcept;

		/**
		* \brief Returns an #iterator to the element following the last element of the array. Accessing it results in undefined behaviour.
		* \return Iterator to element following the last element.
		*/
		iterator end() noexcept;

		/**
		* \brief Returns a #const_iterator to the element following the last element of the array. Accessing it results in undefined behaviour.
		* \return Iterator to element following the last element.
		*/
		const_iterator end() const noexcept;

		/**
		* \brief Returns a #const_iterator to the element following the last element of the array. Accessing it results in undefined behaviour.
		* \return Iterator to element following the last element.
		*/
		const_iterator cend() const noexcept;
	
	private:
		std::unique_ptr<T[]> begin_;		///< Smart pointer to the beginning of the array. Handles memory management.
		T* end_;							///< Pointer to the element following the last element of the array.
	};
} //namespace decoop

#include "../src/dynArrayRaiiWrapper.cpp"