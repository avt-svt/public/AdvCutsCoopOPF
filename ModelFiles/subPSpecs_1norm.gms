
positive Variables
n_coup(t)
p_coup(t)
;

Parameter
ChlAl_nomPMW nominal power level of chloralkali plant in MW;
ChlAl_nomPMW = 5.308789133;

Parameter masterPplant(t);
masterPplant(t) =0;
Equation masterSubCoupling(t);
masterSubCoupling(t).. masterPplant(t) - n_coup(t) + p_coup(t) =e=  totPower(t)/(ChlAl_nomPMW*1000) ;

Variable dumO;
Equation dummyObjective;
dummyObjective.. dumO =e= sum(t, n_coup(t) + p_coup(t) );

model subProblem1 /Plantlinear, masterSubCoupling, dummyObjective/;
