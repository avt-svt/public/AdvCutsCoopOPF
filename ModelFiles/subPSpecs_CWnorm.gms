
positive Variable
lambda
;

Parameter
ChlAl_nomPMW nominal power level of chloralkali plant in MW;
ChlAl_nomPMW = 5.308789133;

Parameter masterPplant(t);
masterPplant(t) =0;

Parameter corePt;
corePt=1;

Equation masterSubCoupling(t);
masterSubCoupling(t).. (1-lambda)*masterPplant(t) + lambda*corePt =e=  totPower(t)/(ChlAl_nomPMW*1000) ;

Variable dumLambda;
Equation dummyObjective;
dummyObjective.. dumLambda =e= lambda;

model subProblemCW /Plantlinear, masterSubCoupling, dummyObjective/;

lambda.up = 1 + 1e-7;
lambda.lo = 0;