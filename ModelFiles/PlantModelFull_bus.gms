*---------------------------------------------------------------------------------------------------------------------------------------------------
*linear plant description
*---------------------------------------------------------------------------------------------------------------------------------------------------
Scalar
idenmean /0.40042776/
Ptmean  /5281.6006/
;
Set
iPt 7.5 minute i.e. 450s  /1*3/
;

Variable
p_Pt(bus,t,iPt)
p_iden(bus,t)
w3(bus,t)
;

*box constraints bounds
p_iden.lo(bus,t) = 0.30;
p_iden.up(bus,t) = 0.6;

*---------------------------------------------------------------------------------------------------------------------------------------------------
*linear models for Auxiliary units/computations
Scalars
Ae electrode area in m2 /2.7/
ncells /160/
nuF /1/
Farad /96485/
Sini /7.07188/
Dgas mol per day /80000/
Dliq mol per day /6.875734564159999e+05/
;

Variables
*Mass balances
Ndotcl2(bus,t)
LiqProd(bus,t)
*Inventory
St(bus,t)
*Compressor, power
Wc(bus,t)
totPower(bus,t)
;

Variables
*Inventory
Sin(bus,t)
Sout(bus,t)
;

Equations
*Mass balances
Ndotcl2calc(bus,t)
*Demand
LiqProd_eq(bus,t)
*Inventory
Stcalc(bus,t)
Ststart(bus)
Stend(bus,t)
Sin_eq(bus,t)
Sout_eq(bus,t)
Sbalance_eq(bus,t)
*Compressor, power
Wccalc(bus,t)
totPowercalc(bus,t)
;
Sin.up(bus,t) =4.5;
Sout.up(bus,t) = 4.5;
Sin.lo(bus,t) = -1e-9;
Sout.lo(bus,t) = -1e-9;
St.up(bus,t) = 13.435;
St.lo(bus,t) = 0;

parameter Wc_CONST;
Wc_CONST = 1.086*8.314*(273.15+13)*1.45/0.45 *(-1+ 3**(0.45/1.45));

*time reconciliation conversion are represented thus: (numbers)  Ndotcl2calc(mol/s). Sin and Sout /1 should be the gPROMS values
Ndotcl2calc(bus,t)$(plantbus(bus)).. Ndotcl2(bus,t) =e= ncells*0.5*nuF*Ae*1e4* p_iden(bus,t) / Farad;
LiqProd_eq(bus,t)$(plantbus(bus))..  LiqProd(bus,t) - 0.99*Ndotcl2(bus,t) =e= - Dliq/(60*1440) - 0.99*Dgas/(60*1440);
Stcalc(bus,t)$(plantbus(bus) and ord(t) >1 ).. St(bus,t) - St(bus,t-1) =e= 60*15*70.9*1.1023e-6* (Sin(bus,t) - Sout(bus,t));
Ststart(bus)$(plantbus(bus))..   St(bus,'t1') =e= Sini;
Stend(bus,t)$(plantbus(bus) and ord(t)=card(t)).. St(bus,t) -Sini =g= -1e-7;
Sin_eq(bus,t)$(plantbus(bus)).. Sin(bus,t) =g=  LiqProd(bus,t);
Sout_eq(bus,t)$(plantbus(bus)).. Sout(bus,t) =g= - LiqProd(bus,t);
Sbalance_eq(bus,t)$(plantbus(bus)).. Sin(bus,t) - Sout(bus,t) =e= LiqProd(bus,t);
Wccalc(bus,t)$(plantbus(bus)).. Wc(bus,t) - Wc_CONST * Ndotcl2(bus,t) =e= - Wc_CONST* Dgas/(60*1440) ;
totPowercalc(bus,t)$(plantbus(bus)).. totPower(bus,t) =e= Wc(bus,t)*1e-3 + sum(iPt$(ord(iPt)<card(iPt)), p_Pt(bus,t,iPt)/(card(iPt)-1) );

model LinearAuxiliaries /Ndotcl2calc, Stcalc, Ststart, Stend, Sin_eq, Sout_eq, Sbalance_eq, LiqProd_eq, Wccalc, totPowercalc/;

*-------------------------------------------------------------------------------
*  Process Model approximated with linear SS models
*-------------------------------------------------------------------------------
$title Linear SS models (chlor-alkali plant)

$onText
Linear Chlor alkali plant model for temperature and power. - identified SS
Temeperature model is generated from createLinearTemptmodel.m
Instantaneous power model is generated from createLinearPmodel.m - tf to SS
Model for auxiliaries is the same as- HWauxPartsmodel (I do not rewrite this portions
as they are exactly same as model LinearAuxiliaries
Date: 9/3/2019 12:38 pm

By Joannah Otashu
NB: discrete linear SS temperature and power models are sampled identically to
their HW model counterparts - 45 secs for Temp, 450 s for Power

*matlab excerpt
sys = ssest(data,nx) estimates a state-space model, sys, using time- or
frequency-domain data, data. sys is a state-space model of order nx and represents:
.
dot_x(t)=Ax(t)+Bu(t)+Ke(t)
y(t)=Cx(t)+Du(t)+e(t)
A, B, C, D, and K are state-space matrices. u(t) is the input, y(t) is the output,
e(t) is the disturbance and x(t) is the vector of nx states.
All the entries of A, B, C, and K are free estimable parameters by default. D is
fixed to zero by default, meaning that there is no feedthrough, except for static systems (nx=0).
* ENd excerpt
Now,
for the state space model, the inputs to the model is wt, and output is xt, and
states are representd with z's.
so the regular ssmodel, xdot = Ax +Bu + Ke; y = Cx + Du + e is given as zdot = Az + Bw +Ke;
x = Cz + Dw + e. In discrete form. .z(k+1)=A z(k) + B w(k) +Ke
$offText

*Power (linear discrete SS)-----------------------------------------------------------------------------------------------------------------------------------------------
Sets
Lzc /zc1/
Lw /w1/
;
Alias
(Lzc,Lzr)
;
*discrete ss matrices at sample time 450 secs
Table A3(Lzr,Lzc)

         zc1
zc1      5.569581787811398e-06
;

Table B3(lzc,Lw)
         w1
zc1      5.951270023029921e+02
;
Table C3(*,Lzc)
         zc1
   x1    25.225481409328860
;
Table D3(*,Lw)
       w1
   x1   0
;
Parameter z3ini_p(Lzc)
/
zc1   0
/
;
Variable
z3(bus,t,Lzr,iPt)
x3(bus,t,iPt)
;

Equation
L_zdotcalc(bus,t,Lzr,iPt)
L_x1calc(bus,t,iPt)
L_zcontinu(bus,t,Lzr,iPt)
L_Ptcalc(bus,t,iPt)
L_idencalc(bus,t)
L_zini(bus,t,Lzr,iPt)
;

L_zdotcalc(bus,t,Lzr,iPt)$(plantbus(bus) and ord(iPt)<card(iPt)).. z3(bus,t,Lzr,iPt+1) =e= sum(Lzc, A3(Lzr,Lzc)* z3(bus,t,Lzc,iPt)) + sum(Lw, + B3(Lzr,Lw)*w3(bus,t));
L_x1calc(bus,t,iPt)$(plantbus(bus)).. x3(bus,t,iPt) =e= sum(Lzc, C3('x1',Lzc)*z3(bus,t,Lzc,iPt)) + sum(Lw, D3('x1',Lw)*w3(bus,t));
L_zcontinu(bus,t,Lzr,iPt)$(plantbus(bus) and (ord(t)>1) and (ord(iPt)=card(iPt))).. z3(bus,t,Lzr,'1') =e= z3(bus,t-1,Lzr,iPt);
L_Ptcalc(bus,t,iPt)$(plantbus(bus)).. p_Pt(bus,t,iPt) =e= x3(bus,t,iPt) + Ptmean;
L_idencalc(bus,t)$(plantbus(bus)).. p_iden(bus,t) =e= w3(bus,t) + idenmean;
L_zini(bus,t,Lzr,iPt)$(plantbus(bus) and ord(t)=1 and ord(iPt)=1).. z3(bus,t,Lzr,iPt) =e= z3ini_p(Lzr);

*Temperature (linear discrete SS)--------------------------------------  ---------------------------------------------------------------------------------------------------
Scalar
Tinmean /78/
Tmean /87.77411/
;

Sets
Lztc /zc1*zc4/
Lwt /w1*w2/
Let /et1/
iT 45 secs  /1*21/
;
Alias
(Lztc,Lztr)
;
*discrete ss matrices at sample time 45 secs
Table A4(Lztr,Lztc)
              zc1         zc2          zc3         zc4
   zc1       0.962   -0.004271           0           0
   zc2    0.005388           1           0           0
   zc3           0           0       0.963  -1.304e-08
   zc4           0           0   2.056e-08           1
;


Table B4(Lztc,Lwt)
            w1            w2
   zc1      11.04          0
   zc2     0.0305          0
   zc3          0      176.7
   zc4          0  1.862e-06
;
Table C4(*,Lztc)
           zc1         zc2      zc3          zc4
x1       0.1194   0.008256  0.0001391     -4.699
;

Table D4(*,Lwt)
       w1   w2
   x1   0   0
;
Parameter z4ini_p(Lztc)
/
zc1  0
zc2  0
zc3  0
zc4  0
/
;
Variable
z4(bus,t,Lztr,iT)
x4(bus,t,iT)
w4(bus,t)
p_T(bus,t,iT)
p_Tin(bus,t)
;
*box constraints
p_Tin.lo(bus,t) = 75;
p_Tin.up(bus,t) = 80;
p_T.lo(bus,t,iT) = 85;
p_T.up(bus,t,iT) = 90;

Equation
L_z2dotcalc(bus,t,Lztr,iT)
L_x2calc(bus,t,iT)
L_z2continu(bus,t,Lztr,iT)
L_Tcalc(bus,t,iT)
L_z2ini(bus,t,Lztr,iT)
L_Tincalc(bus,t)
;

L_z2dotcalc(bus,t,Lztr,iT)$(plantbus(bus) and ord(iT)<card(iT)).. z4(bus,t,Lztr,iT+1) =e= sum(Lztc, A4(Lztr,Lztc)* z4(bus,t,Lztc,iT)) + ( B4(Lztr,'w1')*w3(bus,t) + B4(Lztr,'w2')*w4(bus,t) );
L_x2calc(bus,t,iT)$(plantbus(bus)).. x4(bus,t,iT) =e= sum(Lztc, C4('x1',Lztc)*z4(bus,t,Lztc,iT)) + D4('x1','w1')*w3(bus,t) + D4('x1','w2')*w4(bus,t);
L_z2continu(bus,t,Lztr,iT)$(plantbus(bus) and (ord(t)>1) and (ord(iT)=card(iT))).. z4(bus,t,Lztr,'1') =e= z4(bus,t-1,Lztr,iT);
L_Tcalc(bus,t,iT)$(plantbus(bus)).. p_T(bus,t,iT) =e= x4(bus,t,iT) + Tmean;
L_z2ini(bus,t,Lztr,iT)$(plantbus(bus) and ord(t)=1 and ord(iT)=1).. z4(bus,t,Lztr,iT) =e= z4ini_p(Lztr);
L_Tincalc(bus,t)$(plantbus(bus)).. p_Tin(bus,t) =e= w4(bus,t) + Tinmean;


model Plantlinear /L_zdotcalc, L_x1calc, L_zcontinu, L_Ptcalc, L_idencalc, L_zini, L_z2dotcalc, L_x2calc, L_z2continu, L_Tcalc, L_z2ini, L_Tincalc, LinearAuxiliaries/;
