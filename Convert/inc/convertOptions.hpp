/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

#pragma once
#include <sstream>
#include <vector>
#include <fstream>
#include <iostream>

namespace convert {

	void ParseComments(std::fstream& input);

	struct ConvertOptions {
		//general
		std::string model_dir, workspace_dir, lp_target_dir;
		//nominal and integrated loadflow, price calculation
		int num_timesteps;
		std::string loadflow, loadflow_model_name, price_loadflow, price_lp_file, integrated_specs, plant_model_int, integrated_lp_file;
		//master
		std::string master_specs, master_model_name, master_objective_name, master_lp_file;
		//subproblem
		std::string plant_model_sub, sub_specs_l1, sub_model_name_l1, sub_objective_name_l1, sub_lp_file_l1, sub_specs_cw, sub_model_name_cw, sub_objective_name_cw, sub_lp_file_cw;
		std::vector<std::pair<int, double>> plantbusses;
		friend std::fstream& operator>> (std::fstream& in, ConvertOptions& opt);
	};
}