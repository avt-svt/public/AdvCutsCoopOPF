/**********************************************************************************
 * Copyright (c) 2021 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

#include "convertOptions.hpp"

namespace convert {

	void ParseComments(std::fstream& input) {
		std::string tmp;
		std::string tmp2;
		tmp = static_cast<char>(input.peek());
		while (tmp == "#" || tmp == "\n") {
			std::getline(input, tmp2);
			tmp = static_cast<char>(input.peek());
		}
	}

	std::fstream& operator>> (std::fstream& in, ConvertOptions& opt) {
		std::stringstream sstream;
		std::string token;
		std::string line;
		ParseComments(in);
		while (!in.eof()) {
			sstream.clear();
			std::getline(in, line);
			sstream.str(line);
			sstream >> token;
			if (token == "MODEL_FILE_DIR") {
				sstream >> opt.model_dir;
			}
			else if (token == "WORKSPACE_DIR") {
				sstream >> opt.workspace_dir;
			}
			else if (token == "TARGET_LP_DIR") {
				sstream >> opt.lp_target_dir;
			}
			else if (token == "NR_TIMESTEPS") {
				int tmp;
				sstream >> tmp;
				if (tmp < 0) {
					throw std::runtime_error("Number of timesteps must be a positive integer!");
				}
				opt.num_timesteps = static_cast<unsigned>(tmp);
			}
			else if (token == "GMS_LOADFLOW") {
				sstream >> opt.loadflow;
				if (opt.loadflow.find(".gms") == std::string::npos) {
					throw std::runtime_error("File specified in GMS_LOADFLOW should end in '.gms'!");
				}
			}
			else if (token == "LOADFLOW_MODEL_NAME") {
				sstream >> opt.loadflow_model_name;
			}
			else if (token == "GMS_PRICE_ADJUSTMENT") {
				sstream >> opt.price_loadflow;
				if (opt.price_loadflow.find(".gms") == std::string::npos) {
					throw std::runtime_error("File specified in GMS_PRICE_ADJUSTMENT should end in '.gms'!");
				}
			}
			else if (token == "LP_PRICE_CALCULATION") {
				sstream >> opt.price_lp_file;
				if (opt.price_lp_file.find(".lp") == std::string::npos) {
					throw std::runtime_error("File specified in LP_PRICE_CALCULATION should end in '.lp'!");
				}
			}
			else if (token == "GMS_INTEGRATED_SPECS") {
				sstream >> opt.integrated_specs;
				if (opt.integrated_specs.find(".gms") == std::string::npos) {
					throw std::runtime_error("File specified in GMS_INTEGRATED_SPECS should end in '.gms'!");
				}
			}
			else if (token == "GMS_INTEGR_PLANT_MODEL") {
				sstream >> opt.plant_model_int;
				if (opt.plant_model_int.find(".gms") == std::string::npos) {
					throw std::runtime_error("File specified in GMS_INTEGR_PLANT_MODEL should end in '.gms'!");
				}
			}
			else if (token == "LP_INTEGRATED") {
				sstream >> opt.integrated_lp_file;
				if (opt.integrated_lp_file.find(".lp") == std::string::npos) {
					throw std::runtime_error("File specified in LP_INTEGRATED should end in '.lp'!");
				}
			}
			else if (token == "GMS_MASTER_SPECS") {
				sstream >> opt.master_specs;
				if (opt.master_specs.find(".gms") == std::string::npos) {
					throw std::runtime_error("File specified in GMS_MASTER_SPECS should end in '.gms'!");
				}
			}
			else if (token == "MASTER_MODEL_NAME") {
				sstream >> opt.master_model_name;
			}
			else if (token == "MASTER_OBJ_NAME") {
				sstream >> opt.master_objective_name;
			}
			else if (token == "LP_LOADFLOW") {
				sstream >> opt.master_lp_file;
				if (opt.master_lp_file.find(".lp") == std::string::npos) {
					throw std::runtime_error("File specified in LP_LOADFLOW should end in '.lp'!");
				}
			}
			else if (token == "GMS_SUB_PLANT_MODEL") {
				sstream >> opt.plant_model_sub;
				if (opt.plant_model_sub.find(".gms") == std::string::npos) {
					throw std::runtime_error("File specified in GMS_SUB_PLANT_MODEL should end in '.gms'!");
				}
			}
			else if (token == "GMS_SUB_SPECS_L1") {
				sstream >> opt.sub_specs_l1;
				if (opt.sub_specs_l1.find(".gms") == std::string::npos) {
					throw std::runtime_error("File specified in GMS_SUB_SPECS_L1 should end in '.gms'!");
				}
			}
			else if (token == "SUB_MODEL_NAME_L1") {
				sstream >> opt.sub_model_name_l1;
			}
			else if (token == "SUB_OBJ_NAME_L1") {
				sstream >> opt.sub_objective_name_l1;
			}
			else if (token == "LP_SUB_L1") {
				sstream >> opt.sub_lp_file_l1;
				if (opt.sub_lp_file_l1.find(".lp") == std::string::npos) {
					throw std::runtime_error("File specified in LP_SUB_L1 should end in '.lp'!");
				}
			}
			else if (token == "GMS_SUB_SPECS_CW") {
				sstream >> opt.sub_specs_cw;
				if (opt.sub_specs_cw.find(".gms") == std::string::npos) {
					throw std::runtime_error("File specified in GMS_SUB_SPECS_CW should end in '.gms'!");
				}
			}
			else if (token == "SUB_MODEL_NAME_CW") {
				sstream >> opt.sub_model_name_cw;
			}
			else if (token == "SUB_OBJ_NAME_CW") {
				sstream >> opt.sub_objective_name_cw;
			}
			else if (token == "LP_SUB_CW") {
				sstream >> opt.sub_lp_file_cw;
				if (opt.sub_lp_file_cw.find(".lp") == std::string::npos) {
					throw std::runtime_error("File specified in LP_SUB_CW should end in '.lp'!");
				}
			}
			else if (token == "PLANTBUSSES") {
				int plantbus;
				double load_fraction;
				while (sstream >> plantbus >> load_fraction) {
					opt.plantbusses.emplace_back(std::make_pair(plantbus, load_fraction));
				}
			}
			else {
				std::cout << "unkonwn keyword: " << token << std::endl;
			}
			ParseComments(in);
		}
		return in;
	}
}