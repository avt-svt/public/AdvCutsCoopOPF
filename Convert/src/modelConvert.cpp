#include <fstream>
#include <iostream>
#include "convertOptions.hpp"
#include "convertUtils.hpp"

int main(int argc, char* argv[]) {
	try {
		if (argc < 2) throw std::runtime_error("no command line Arguments given");

		convert::ConvertOptions options;

		//read options from input file, command arg is directory where the option file is
		std::string option_file_dir(argv[1]);
		std::fstream input_options(option_file_dir + "options_convert.txt");
		if (!input_options.is_open()) {
			throw std::runtime_error("failed to open option file. aborting");
		}
		try {
			input_options >> options;
		}
		catch (const std::runtime_error& e) {
			std::cout << "Error reading option file: ";
			input_options.close();
			throw;
		}
		input_options.close();

		try {
			convert::ConvertToGeneric(options);
		}
		catch (const std::exception& e) {
			std::cout << e.what() << std::endl;
			std::exit(-1);
		}
		//Replace generic names from dictionary ----------------------------------------------
		convert::AddNamesToGeneric(options.workspace_dir + "dict_loadflow_nominal_mod.txt", options.workspace_dir + "loadflow_nominal_mod_generic.lp", options.lp_target_dir + options.price_lp_file);
		convert::AddNamesToGeneric(options.workspace_dir + "dict_loadflow_int.txt", options.workspace_dir + "loadflow_int_generic.lp", options.lp_target_dir + options.integrated_lp_file);
		convert::AddNamesToGeneric(options.workspace_dir + "dict_loadflow.txt", options.workspace_dir + "loadflow_generic.lp", options.lp_target_dir + options.master_lp_file);
		convert::AddNamesToGeneric(options.workspace_dir + "dict_sub_l1.txt", options.workspace_dir + "sub_generic_l1.lp", options.lp_target_dir + options.sub_lp_file_l1);
		convert::AddNamesToGeneric(options.workspace_dir + "dict_sub_cw.txt", options.workspace_dir + "sub_generic_cw.lp", options.lp_target_dir + options.sub_lp_file_cw);
	}
	catch (const std::runtime_error& e) {
		std::cout << e.what() << std::endl;
	}
	return 0;
}