#Do not edit keywords!
MODEL_FILE_DIR		U:/cooperatopf/ModelFiles/
WORKSPACE_DIR		U:/cooperatopf/Convert/Workspace/
TARGET_LP_DIR		U:/cooperatopf/Workspace/
NR_TIMESTEPS		96

GMS_LOADFLOW		loadflow.gms
#GMS_LOADFLOW		cong_loadflow.gms
LOADFLOW_MODEL_NAME	loadflow

GMS_PRICE_ADJUSTMENT	nomPriceLoadflow.gms
LP_PRICE_CALCULATION	loadflow_price.lp

GMS_INTEGRATED_SPECS	integratedSpecs.gms
GMS_INTEGR_PLANT_MODEL	PlantModelFull_bus.gms
LP_INTEGRATED		loadflow_integrated.lp

GMS_MASTER_SPECS	masterSpec.gms
MASTER_MODEL_NAME	masterOPF
MASTER_OBJ_NAME		OF
LP_LOADFLOW		loadflow.lp

GMS_SUB_PLANT_MODEL	PlantModelFull.gms
GMS_SUB_SPECS_L1	subPSpecs_1norm.gms
SUB_MODEL_NAME_L1	subProblem1
SUB_OBJ_NAME_L1		dumO
LP_SUB_L1		sub_l1.lp
GMS_SUB_SPECS_CW	subPSpecs_CWnorm.gms
SUB_MODEL_NAME_CW	subProblemCW
SUB_OBJ_NAME_CW		dumLambda
LP_SUB_CW		sub_cw.lp

#Plantbusses in format <plantbus> <fraction>, all in one line

PLANTBUSSES		5 1
# PLANTBUSSES		5 0.462 7 0.2536 8 0.038
# PLANTBUSSES		7 0.568
# PLANTBUSSES		5 0.462 1 0.0602 7 0.2536
# PLANTBUSSES		5 0.5535 7 0.2536
